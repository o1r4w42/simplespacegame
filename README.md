Dagger2:

    Preparations in root build.gradle:
        - add 'classpath "net.ltgt.gradle:gradle-apt-plugin:0.18"' to
            'dependencies' under 'buildscript'
        - add 'daggerVersion = "2.16"' to 'ext' under 'allprojects'
        - add 'apply plugin: "net.ltgt.apt"' under 'project(":core")'
        - add 'compile "com.google.dagger:dagger:$daggerVersion"' and
            'apt "com.google.dagger:dagger-compiler:$daggerVersion"' to 'dependencies' under 'project(":core")'
    Usage:
        - create an injection module class with the '@Module' annotation like SSInjectionModule;
            this class has a function for every injectable class
        - create an injection component interface with the '@Singleton' and '@Component' annotation
            like SSInjectionComponent; the '@Component' annotation must point to at least one module class;
            this class has a function for every class that has injections
        - instantiate a class auto generated from the component interface in the main game class
            like in SimpleSpaceGame; the inject function of this injector must be called by every class
            with injections prior to any operation on the injected fields