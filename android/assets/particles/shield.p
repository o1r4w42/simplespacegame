Untitled
- Delay -
active: false
- Duration - 
lowMin: 100.0
lowMax: 100.0
- Count - 
min: 100
max: 400
- Emission - 
lowMin: 400.0
lowMax: 400.0
highMin: 400.0
highMax: 400.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 200.0
lowMax: 200.0
highMin: 200.0
highMax: 200.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 1.0
scaling2: 0.11764706
timelineCount: 3
timeline0: 0.0
timeline1: 0.999
timeline2: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 32.0
highMax: 32.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 200.0
lowMax: 200.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.23529412
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.5068493
timeline2: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 360.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.999
timeline2: 1.0
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 6
colors0: 1.0
colors1: 0.38039216
colors2: 0.047058824
colors3: 1.0
colors4: 0.047058824
colors5: 0.047058824
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.9649123
scaling2: 0.75
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.5890411
timeline2: 0.8
timeline3: 1.0
- Options - 
attached: true
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
particle.png

