<?xml version="1.0" encoding="UTF-8"?>
<tileset name="effects" tilewidth="8" tileheight="8" tilecount="6" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="8" height="8" source="../../rawAssets/effects/laser_start.png"/>
 </tile>
 <tile id="1">
  <image width="8" height="8" source="../../rawAssets/effects/laser_mid.png"/>
 </tile>
 <tile id="2">
  <image width="8" height="8" source="../../rawAssets/effects/laser_end.png"/>
 </tile>
 <tile id="3">
  <image width="8" height="8" source="../../rawAssets/effects/gun_start.png"/>
 </tile>
 <tile id="4">
  <image width="8" height="8" source="../../rawAssets/effects/gun_mid.png"/>
 </tile>
 <tile id="5">
  <image width="8" height="8" source="../../rawAssets/effects/gun_end.png"/>
 </tile>
</tileset>
