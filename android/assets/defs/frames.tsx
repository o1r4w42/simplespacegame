<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="frames" tilewidth="120" tileheight="120" tilecount="1" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="120" height="120" source="../../rawAssets/frames/s_0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="module" type="mid" x="20" y="40" width="40" height="40">
    <properties>
     <property name="index" type="int" value="0"/>
     <property name="points">0,-2,-1
1,-1,-1
2,1,-1
3,2,-1
4,2,1
5,1,1
6,-1,1
7,-2,1</property>
     <property name="size" value="80,40"/>
     <property name="subIndex" type="int" value="0"/>
    </properties>
   </object>
   <object id="2" name="module" type="mid" x="60" y="40" width="40" height="40">
    <properties>
     <property name="index" type="int" value="1"/>
     <property name="points">0,-2,-1
1,-1,-1
2,1,-1
3,2,-1
4,2,1
5,1,1
6,-1,1
7,-2,1</property>
     <property name="size" value="80,40"/>
     <property name="subIndex" type="int" value="0"/>
    </properties>
   </object>
   <object id="3" name="module" type="mid" x="-20" y="40" width="40" height="40">
    <properties>
     <property name="index" type="int" value="2"/>
     <property name="points">0,-4,-1
7,-4,1</property>
     <property name="size" value="160,40"/>
     <property name="subIndex" type="int" value="0"/>
    </properties>
   </object>
   <object id="4" name="module" type="back" x="20" y="0" width="40" height="40">
    <properties>
     <property name="index" type="int" value="3"/>
     <property name="points">1,-2,-2
6,-2,2</property>
     <property name="size" value="80,120"/>
     <property name="subIndex" type="int" value="0"/>
    </properties>
   </object>
   <object id="5" name="module" type="front" x="60" y="0" width="40" height="40">
    <properties>
     <property name="index" type="int" value="4"/>
     <property name="points">2,2,-2
5,2,2</property>
     <property name="size" value="80,120"/>
     <property name="subIndex" type="int" value="0"/>
    </properties>
   </object>
   <object id="6" name="module" type="mid" x="100" y="40" width="40" height="40">
    <properties>
     <property name="index" type="int" value="5"/>
     <property name="points">3,4,-1
4,4,1</property>
     <property name="size" value="160,40"/>
     <property name="subIndex" type="int" value="0"/>
    </properties>
   </object>
   <object id="10" name="body" x="0" y="80">
    <polyline points="0,0 20,20 100,20 120,0 120,-40 100,-60 20,-60 0,-40 0,0"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
