<?xml version="1.0" encoding="UTF-8"?>
<tileset name="items" tilewidth="32" tileheight="32" tilecount="4" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <properties>
  <property name="sensor" type="bool" value="true"/>
 </properties>
 <tile id="1">
  <properties>
   <property name="part" value="armor"/>
  </properties>
  <image width="32" height="32" source="../../rawAssets/items/armor_0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="body" x="0" y="0" width="32" height="32">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="2">
  <properties>
   <property name="part" value="engine"/>
  </properties>
  <image width="32" height="32" source="../../rawAssets/items/engine_0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="body" x="0" y="0" width="32" height="32">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="3">
  <properties>
   <property name="part" value="power"/>
  </properties>
  <image width="32" height="32" source="../../rawAssets/items/power_0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="body" x="0" y="0" width="32" height="32">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="4">
  <properties>
   <property name="part" value="weapon"/>
  </properties>
  <image width="32" height="32" source="../../rawAssets/items/weapon_0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="body" x="0" y="0" width="32" height="32">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
</tileset>
