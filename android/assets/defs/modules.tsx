<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="modules" tilewidth="40" tileheight="40" tilecount="4" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="1">
  <properties>
   <property name="density" type="float" value="1"/>
   <property name="hp" type="float" value="20"/>
   <property name="hp:max" type="float" value="20"/>
   <property name="size" type="int" value="1"/>
  </properties>
  <image width="40" height="40" source="../../rawAssets/modules/s_0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="slot" type="back" x="2" y="25" width="2" height="2"/>
   <object id="2" name="slot" type="side" x="14" y="16" width="2" height="2"/>
   <object id="3" name="slot" type="front" x="33" y="22" width="2" height="2"/>
  </objectgroup>
 </tile>
 <tile id="2">
  <properties>
   <property name="density" type="float" value="1"/>
   <property name="hp" type="float" value="20"/>
   <property name="hp:max" type="float" value="20"/>
   <property name="size" type="int" value="1"/>
  </properties>
  <image width="40" height="40" source="../../rawAssets/modules/s_1.png"/>
  <objectgroup draworder="index">
   <object id="4" name="slot" type="back" x="2" y="25" width="2" height="2"/>
   <object id="5" name="slot" type="side" x="14" y="16" width="2" height="2"/>
   <object id="6" name="slot" type="front" x="33" y="22" width="2" height="2"/>
  </objectgroup>
 </tile>
 <tile id="3">
  <properties>
   <property name="density" type="float" value="1"/>
   <property name="hp" type="float" value="20"/>
   <property name="hp:max" type="float" value="20"/>
   <property name="size" type="int" value="1"/>
  </properties>
  <image width="40" height="40" source="../../rawAssets/modules/mStart_0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="slot" type="back" x="4" y="7" width="2" height="2">
    <properties>
     <property name="add" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="4">
  <properties>
   <property name="density" type="float" value="1"/>
   <property name="hp" type="float" value="20"/>
   <property name="hp:max" type="float" value="20"/>
   <property name="size" type="int" value="1"/>
  </properties>
  <image width="40" height="40" source="../../rawAssets/modules/mEnd_0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="slot" type="front" x="31" y="13" width="2" height="2">
    <properties>
     <property name="add" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
</tileset>
