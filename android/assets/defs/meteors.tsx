<?xml version="1.0" encoding="UTF-8"?>
<tileset name="meteors" tilewidth="100" tileheight="100" tilecount="4" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <properties>
   <property name="hp" type="float" value="10"/>
   <property name="size" type="int" value="1"/>
  </properties>
  <image width="18" height="18" source="../../rawAssets/meteors/s_0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="body" x="0" y="0" width="18" height="18">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1">
  <properties>
   <property name="hp" type="float" value="20"/>
   <property name="size" type="int" value="2"/>
  </properties>
  <image width="28" height="28" source="../../rawAssets/meteors/m_0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="body" x="0" y="0" width="28" height="28">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="2">
  <properties>
   <property name="hp" type="float" value="40"/>
   <property name="size" type="int" value="3"/>
  </properties>
  <image width="44" height="44" source="../../rawAssets/meteors/l_0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="body" x="0" y="0" width="44" height="44">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="3">
  <properties>
   <property name="hp" type="float" value="100"/>
   <property name="size" type="int" value="4"/>
  </properties>
  <image width="100" height="100" source="../../rawAssets/meteors/xl_0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="body" x="0" y="0" width="98" height="96">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
</tileset>
