<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.0" name="parts" tilewidth="32" tileheight="32" tilecount="6" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <properties>
   <property name="acceleration:max" type="float" value="4.5"/>
   <property name="acceleration:mod" type="float" value="0.5"/>
   <property name="effect" value="default"/>
   <property name="icon" type="file" value="../../rawAssets/icons/part_drive.png"/>
   <property name="shipPartType" value="drive"/>
  </properties>
  <image width="21" height="13" source="../../rawAssets/parts/drive_0.png"/>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="effect" value="default"/>
   <property name="icon" type="file" value="../../rawAssets/icons/part_thrust.png"/>
   <property name="rotate:max" type="float" value="135"/>
   <property name="rotate:mod" type="float" value="0.5"/>
   <property name="shipPartType" value="thrust"/>
  </properties>
  <image width="7" height="16" source="../../rawAssets/parts/thrust_0.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="dmg" type="float" value="5"/>
   <property name="dmg:armor" type="float" value="1"/>
   <property name="dmg:shield" type="float" value="0.5"/>
   <property name="icon" type="file" value="../../rawAssets/icons/part_gun.png"/>
   <property name="shipPartType" value="gun"/>
  </properties>
  <image width="32" height="32" source="../../rawAssets/parts/gun_0.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="dmg" type="float" value="5"/>
   <property name="dmg:armor" type="float" value="0.5"/>
   <property name="dmg:shield" type="float" value="1"/>
   <property name="icon" type="file" value="../../rawAssets/icons/part_laser.png"/>
   <property name="shipPartType" value="laser"/>
  </properties>
  <image width="32" height="32" source="../../rawAssets/parts/laser_0.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="icon" type="file" value="../../rawAssets/icons/part_armor.png"/>
   <property name="shipPartType" value="armor"/>
  </properties>
  <image width="32" height="32" source="../../rawAssets/parts/armor_0.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="icon" type="file" value="../../rawAssets/icons/part_shield.png"/>
   <property name="shipPartType" value="shield"/>
   <property name="sp" type="float" value="20"/>
   <property name="sp:max" type="float" value="20"/>
   <property name="sp:reg" type="float" value="1"/>
  </properties>
  <image width="32" height="32" source="../../rawAssets/parts/shield_0.png"/>
 </tile>
</tileset>
