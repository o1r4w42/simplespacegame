<?xml version="1.0" encoding="UTF-8"?>
<tileset name="ships" tilewidth="92" tileheight="92" tilecount="3" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <properties>
  <property name="sensor" type="bool" value="false"/>
 </properties>
 <tile id="2">
  <properties>
   <property name="hp" type="float" value="100"/>
   <property name="hp:reg" type="float" value="0.1"/>
   <property name="size" type="int" value="3"/>
   <property name="sp" type="float" value="10"/>
   <property name="sp:reg" type="float" value="1"/>
  </properties>
  <image width="92" height="92" source="../../rawAssets/ships/s_0.png"/>
  <objectgroup draworder="index">
   <object id="12" name="body" x="7" y="74">
    <polyline points="0,-7 28,-7 79,-23 79,-33 28,-49 0,-49 0,-7"/>
   </object>
   <object id="5" name="slot" type="back" x="5" y="29" width="2" height="2">
    <properties>
     <property name="index" type="int" value="0"/>
    </properties>
   </object>
   <object id="6" name="slot" type="back" x="18" y="23" width="2" height="2">
    <properties>
     <property name="index" type="int" value="1"/>
    </properties>
   </object>
   <object id="8" name="slot" type="mid" x="35" y="27" width="2" height="2">
    <properties>
     <property name="index" type="int" value="2"/>
    </properties>
   </object>
   <object id="10" name="slot" type="front" x="77" y="36" width="2" height="2">
    <properties>
     <property name="index" type="int" value="3"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="3">
  <properties>
   <property name="hp" type="float" value="100"/>
   <property name="hp:reg" type="float" value="0.1"/>
   <property name="size" type="int" value="3"/>
   <property name="sp" type="float" value="10"/>
   <property name="sp:reg" type="float" value="1"/>
  </properties>
  <image width="92" height="92" source="../../rawAssets/ships/s_1.png"/>
  <objectgroup draworder="index">
   <object id="12" name="body" x="4" y="74">
    <polyline points="0,0 35,0 83,-22 83,-34 35,-56 0,-56 0,0"/>
   </object>
   <object id="4" name="slot" type="back" x="2" y="23" width="2" height="2">
    <properties>
     <property name="index" type="int" value="0"/>
    </properties>
   </object>
   <object id="5" name="slot" type="back" x="16" y="17" width="2" height="2">
    <properties>
     <property name="index" type="int" value="1"/>
    </properties>
   </object>
   <object id="7" name="slot" type="mid" x="40" y="25" width="2" height="2">
    <properties>
     <property name="index" type="int" value="2"/>
    </properties>
   </object>
   <object id="9" name="slot" type="front" x="79" y="36" width="2" height="2">
    <properties>
     <property name="index" type="int" value="3"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="4">
  <properties>
   <property name="hp" type="float" value="100"/>
   <property name="hp:reg" type="float" value="0.1"/>
   <property name="size" type="int" value="3"/>
   <property name="sp" type="float" value="10"/>
   <property name="sp:reg" type="float" value="1"/>
  </properties>
  <image width="80" height="80" source="../../rawAssets/ships/s_2.png"/>
  <objectgroup draworder="index">
   <object id="1" name="body" x="1" y="68">
    <polyline points="0,-7 28,-7 79,-23 79,-33 28,-49 0,-49 0,-7"/>
   </object>
   <object id="3" name="slot" type="back" x="-1" y="23" width="2" height="2">
    <properties>
     <property name="index" type="int" value="0"/>
    </properties>
   </object>
   <object id="4" name="slot" type="back" x="12" y="17" width="2" height="2">
    <properties>
     <property name="index" type="int" value="1"/>
    </properties>
   </object>
   <object id="6" name="slot" type="mid" x="29" y="21" width="2" height="2">
    <properties>
     <property name="index" type="int" value="2"/>
    </properties>
   </object>
   <object id="8" name="slot" type="front" x="71" y="30" width="2" height="2">
    <properties>
     <property name="index" type="int" value="3"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
</tileset>
