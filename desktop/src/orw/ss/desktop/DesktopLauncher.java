package orw.ss.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.SimpleSpaceGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = SSUtil.SSIntegers.SCREEN_WIDTH;
		config.height = SSUtil.SSIntegers.SCREEN_HEIGHT;
		new LwjglApplication(new SimpleSpaceGame(), config);
	}
}
