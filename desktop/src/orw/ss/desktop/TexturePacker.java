package orw.ss.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Texture;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.SimpleSpaceGame;

public class TexturePacker {
	public static void main (String[] arg) {
		ORWLogger.log("TexturePacker is starting...");
		com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings settings = new com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings();
		settings.filterMin = Texture.TextureFilter.Nearest;
		settings.filterMag = Texture.TextureFilter.Nearest;
		settings.maxWidth = 2048;
		settings.maxHeight = 2048;
		settings.combineSubdirectories = true;
		settings.duplicatePadding = true;
		settings.stripWhitespaceX = true;
		settings.stripWhitespaceY = true;
		settings.pot = true;
		settings.scale = new float[] { 1f };
		com.badlogic.gdx.tools.texturepacker.TexturePacker.processIfModified(settings,
				"../rawAssets/", "../assets/",
				"textures");
		ORWLogger.log("TexturePacker finished!");
	}
}
