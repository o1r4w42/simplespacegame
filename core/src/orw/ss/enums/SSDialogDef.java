package orw.ss.enums;

import orw.ss.SSUtil;

public enum SSDialogDef {
    UPGRADE, PAUSE;

    private final int index;

    SSDialogDef() {
        index = SSUtil.SSIntegers.SSDialogDefInd++;
    }

    public int getInd() {
        return index;
    }

}
