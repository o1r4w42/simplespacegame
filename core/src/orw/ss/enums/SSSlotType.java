package orw.ss.enums;

import orw.ORWLogger;
import orw.ss.SSUtil;

public enum SSSlotType {
    FRONT(SSUtil.SSStrings.FRONT),
    SIDE(SSUtil.SSStrings.SIDE),
    BACK(SSUtil.SSStrings.BACK),
    ;
    private final String name;

    SSSlotType(String name) {
        this.name = name;
    }

    public static SSSlotType get(String name) {
        for (SSSlotType type : SSSlotType.values())
            if (type.name.equals(name)) return type;
        ORWLogger.log("SSSlotType '" + name + "' not found!");
        return null;
    }

}
