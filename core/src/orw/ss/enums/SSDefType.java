package orw.ss.enums;

import orw.ORWLogger;
import orw.ss.SSUtil;

public enum SSDefType {
    SHIP_S_0(SSUtil.SSStrings.SHIP_S_0),
    SHIP_S_1(SSUtil.SSStrings.SHIP_S_1),
    SHIP_S_2(SSUtil.SSStrings.SHIP_S_2),

    FRAME_XS_0(SSUtil.SSStrings.FRAME_XS_0),
    FRAME_S_0(SSUtil.SSStrings.FRAME_S_0),
    FRAME_M_0(SSUtil.SSStrings.FRAME_M_0),

    MODULE_BASE_S_0(SSUtil.SSStrings.MODULE_BASE_S_0),
    MODULE_S_0(SSUtil.SSStrings.MODULE_S_0),
    MODULE_S_1(SSUtil.SSStrings.MODULE_S_1),
    MODULE_M_START_0(SSUtil.SSStrings.MODULE_M_START_0),
    MODULE_M_END_0(SSUtil.SSStrings.MODULE_M_END_0),
    //        STAR_0(SSUtil.SSStrings.STAR_0),
    ITEM_ARMOR(SSUtil.SSStrings.ITEM_ARMOR),
    ITEM_ENGINE(SSUtil.SSStrings.ITEM_ENGINE),
    ITEM_POWER(SSUtil.SSStrings.ITEM_POWER),
    ITEM_WEAPON(SSUtil.SSStrings.ITEM_WEAPON),

    PART_DRIVE_0(SSUtil.SSStrings.PART_DRIVE_0),
    PART_THRUST_0(SSUtil.SSStrings.PART_THRUST_0),
    PART_GUN_0(SSUtil.SSStrings.PART_GUN_0),
    PART_LASER_0(SSUtil.SSStrings.PART_LASER_0),
    PART_ARMOR_0(SSUtil.SSStrings.PART_ARMOR_0),
    PART_SHIELD_0(SSUtil.SSStrings.PART_SHIELD_0),

    BG_0(SSUtil.SSStrings.BG_0),

    MET_S_0(SSUtil.SSStrings.MET_S_0),
    MET_M_0(SSUtil.SSStrings.MET_M_0),
    MET_L_0(SSUtil.SSStrings.MET_L_0),
    MET_XL_0(SSUtil.SSStrings.MET_XL_0),;

    private final String name;

    SSDefType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static SSDefType get(String name) {
        for (SSDefType type : SSDefType.values())
            if (type.name.equals(name)) return type;
        ORWLogger.log("SSDefType '" + name + "' not found!");
        return null;
    }

    public static SSDefType get(SSShipPartType type,
                                int size) {
        switch (type) {
            case ARMOR:
                return PART_ARMOR_0;
            case DRIVE:
                return PART_DRIVE_0;
            case GUN:
                return PART_GUN_0;
            case LASER:
                return PART_LASER_0;
            case SHIELD:
                return PART_SHIELD_0;
            case THRUST:
                return PART_THRUST_0;
        }
        return null;
    }

}
