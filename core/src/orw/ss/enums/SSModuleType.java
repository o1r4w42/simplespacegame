package orw.ss.enums;

import orw.ORWLogger;
import orw.ss.SSUtil;

public enum SSModuleType {
    FRONT(SSUtil.SSStrings.FRONT, SSSlotType.BACK),
    MID(SSUtil.SSStrings.MID),
    BACK(SSUtil.SSStrings.BACK, SSSlotType.FRONT),
    ;

    private final String name;
    private final SSSlotType[] types;

    SSModuleType(String name, SSSlotType... types) {
        this.name = name;
        this.types = types;
    }

    public static SSModuleType get(String name) {
        for (SSModuleType type : SSModuleType.values())
            if (type.name.equals(name)) return type;
        ORWLogger.log("SSModuleType '" + name + "' not found!");
        return null;
    }

    public boolean checkType(SSSlotType type) {
        if (types == null) return true;
        for (SSSlotType t : types)
            if (t == type) return false;
        return true;
    }
}
