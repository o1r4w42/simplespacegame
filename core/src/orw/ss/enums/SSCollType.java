package orw.ss.enums;

public enum SSCollType {
    ITEM((short) 1),
    PLAYER((short) 2),
    ENEMY((short) 8),
    METEOR((short) 16),
    WEAPON((short) 32),

    BODY((short) (2 + 8 + 16));

    private final short id;

     SSCollType(short id) {
        this.id = id;
    }

    public short getId() {
        return id;
    }

    public boolean equalsBit(short bit) {
        return id == bit;
    }

}
