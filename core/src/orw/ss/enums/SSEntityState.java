package orw.ss.enums;

public enum SSEntityState {
    FLY, ROTATE, ATTACK, DESPAWN, IDLE, UPGRADE, DRIFT;
}
