package orw.ss.enums;

import com.badlogic.gdx.graphics.Color;

import orw.ORWLogger;
import orw.ss.SSUtil;

public enum SSUpgradePartType {
    ARMOR(SSUtil.SSStrings.ARMOR, "icons/ico_armor", Color.ORANGE),
    ENGINE(SSUtil.SSStrings.ENGINE, "icons/ico_engine", Color.GREEN),
    POWER(SSUtil.SSStrings.POWER, "icons/ico_power", Color.BLUE),
    WEAPON(SSUtil.SSStrings.WEAPON, "icons/ico_weapon", Color.RED),
    ;

    private final String name;
    private final int index;
    private final String source;
    private final Color color;

    SSUpgradePartType(String name, String source, Color color) {
        this.name = name;
        index = SSUtil.SSIntegers.SSUpgradePartTypeInd++;
        this.source = source;
        this.color = color;
    }

    public int getIndex() {
        return index;
    }

    public static SSUpgradePartType get(String name) {
        for (SSUpgradePartType type : SSUpgradePartType.values())
            if (type.name.equals(name)) return type;
        ORWLogger.log("SSUpgradePartType '" + name + "' not found!");
        return null;
    }

    public String getSource() {
        return source;
    }

    public Color getColor() {
        return color;
    }

}
