package orw.ss.enums;

import com.badlogic.gdx.utils.XmlReader;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.assets.SSAssetManager;
import orw.ss.assets.SSEntityParser;
import orw.ss.assets.SSParser;
import orw.ss.entity.SSEntityDef;

public enum SSEntityProp implements SSParser {

    SENSOR(SSUtil.SSStrings.SENSOR, SSAssetManager.boolParser),
    EFFECT(SSUtil.SSStrings.EFFECT, SSAssetManager.effectParser),
    SHIP_PART_TYPE(SSUtil.SSStrings.SHIP_PART_TYPE,
            SSAssetManager.shipPartTypeParser),
    VIEW_SRC(SSUtil.SSStrings.VIEW_PATH, null),
    VIEW_NAME(SSUtil.SSStrings.VIEW_NAME, null),
    VIEW_IND(SSUtil.SSStrings.VIEW_IND, null),
    SLOTS(SSUtil.SSStrings.SLOTS, null),
    PART(SSUtil.SSStrings.PART, SSAssetManager.partParser),
    SLOT(SSUtil.SSStrings.SLOT, SSAssetManager.slotParser),
    MODULE(SSUtil.SSStrings.MODULE, SSAssetManager.moduleParser),
    BODY(SSUtil.SSStrings.BODY, SSAssetManager.bodyParser),
    ICON(SSUtil.SSStrings.ICON, SSAssetManager.fileParser),
    SIZE(SSUtil.SSStrings.SIZE, SSAssetManager.statParser),
    DENSITY(SSUtil.SSStrings.DENSITY, SSAssetManager.statParser),
    HP(SSUtil.SSStrings.HP, SSAssetManager.statParser),
    HP_MAX(SSUtil.SSStrings.HP_MAX, SSAssetManager.statParser),
    HP_REG(SSUtil.SSStrings.HP_REG, SSAssetManager.statParser),
    SP(SSUtil.SSStrings.SP, SSAssetManager.statParser),
    SP_MAX(SSUtil.SSStrings.SP_MAX, SSAssetManager.statParser),
    SP_REG(SSUtil.SSStrings.SP_REG, SSAssetManager.statParser),
    DMG(SSUtil.SSStrings.DMG, SSAssetManager.statParser),
    DMG_ARMOR(SSUtil.SSStrings.DMG_ARMOR, SSAssetManager.statParser),
    DMG_SHIELD(SSUtil.SSStrings.DMG_SHIELD, SSAssetManager.statParser),
    ROTATE_MOD(SSUtil.SSStrings.ROTATE_MOD, SSAssetManager.statParser),
    ROTATE_MAX(SSUtil.SSStrings.ROTATE_MAX, SSAssetManager.angleParser),
    ACCELERATION_MOD(SSUtil.SSStrings.ACCELERATION_MOD, SSAssetManager.statParser),
    ACCELERATION_MAX(SSUtil.SSStrings.ACCELERATION_MAX, SSAssetManager.statParser),;

    private final String name;
    private final SSEntityParser parser;

    SSEntityProp(String name, SSEntityParser parser) {
        this.name = name;
        this.parser = parser;
    }

    public static SSEntityProp get(String name) {
        for (SSEntityProp type : SSEntityProp.values())
            if (type.name.equals(name)) return type;
        ORWLogger.log("SSEntityProp '" + name + "' not found!");
        return null;
    }

    @Override
    public void parse(SSEntityDef def, XmlReader.Element element, XmlReader.Element parent) {
        parser.parse(def, element, parent, name, this);
    }

    public String getName() {
        return name;
    }

}
