package orw.ss.enums;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.assets.SSAssetManager;

public enum SSEntityStat {
    ACCELERATION_SPEED(SSUtil.SSStrings.ACCELERATION_SPEED),
    ACCELERATION_STEP(SSUtil.SSStrings.ACCELERATION_STEP),
    ACCELERATION_MOD(SSUtil.SSStrings.ACCELERATION_MOD),
    ACCELERATION_MAX(SSUtil.SSStrings.ACCELERATION_MAX),
    ROTATE_SPEED(SSUtil.SSStrings.ROTATE_SPEED),
    ROTATE_STEP(SSUtil.SSStrings.ROTATE_STEP),
    ROTATE_MOD(SSUtil.SSStrings.ROTATE_MOD),
    ROTATE_MAX(SSUtil.SSStrings.ROTATE_MAX),
    VELOCITY_MAX(SSUtil.SSStrings.VELOCITY_MAX),
    HP(SSUtil.SSStrings.HP),
    HP_MAX(SSUtil.SSStrings.HP_MAX),
    HP_REG(SSUtil.SSStrings.HP_REG),
    SP(SSUtil.SSStrings.SP),
    SP_MAX(SSUtil.SSStrings.SP_MAX),
    SP_REG(SSUtil.SSStrings.SP_REG),
    SIZE(SSUtil.SSStrings.SIZE, false),
    DENSITY(SSUtil.SSStrings.DENSITY, false),
    MASS(SSUtil.SSStrings.MASS),
    DMG(SSUtil.SSStrings.DMG, false),
    DMG_ARMOR(SSUtil.SSStrings.DMG_ARMOR, false),
    DMG_SHIELD(SSUtil.SSStrings.DMG_SHIELD, false),
    ;

    private final int index;
    private final String name;
    private final boolean additive;
    private final boolean multiplicative;

    SSEntityStat(String name) {
        this(name, true, false);
    }

    SSEntityStat(String name, boolean additive) {
        this(name, additive, false);
    }

    SSEntityStat(String name, boolean additive, boolean multiplicative) {
        index = SSUtil.SSIntegers.SSEntityStatInd++;
        this.name = name;
        this.additive = additive;
        this.multiplicative = multiplicative;
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }

    public boolean isAdditive() {
        return additive;
    }

    public boolean isMultiplicative() {
        return multiplicative;
    }

    public static SSEntityStat get(String name) {
        for (SSEntityStat type : SSEntityStat.values())
            if (type.name.equals(name)) return type;
        ORWLogger.log("SSEntityStat '" + name + "' not found!");
        return null;
    }

}
