package orw.ss.enums;

import orw.ss.SSUtil;

public enum SSEntityType {
    PLAYER(SSUtil.SSStrings.PLAYER),
    SHIP(SSUtil.SSStrings.ENEMY),
    ITEM(SSUtil.SSStrings.ITEM),
    OBJECT(SSUtil.SSStrings.OBJECT),
    METEOR(SSUtil.SSStrings.METEOR),
    ;

    private final String name;

    SSEntityType(String n) {
        name = n;
    }

    public String getName() {
        return name;
    }

}
