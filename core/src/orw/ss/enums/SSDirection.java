package orw.ss.enums;

public enum SSDirection {
    LEFT(1), RIGHT(-1);

    private final int mod;

    SSDirection(int mod) {
        this.mod = mod;
    }

    public int getMod() {
        return mod;
    }

}
