package orw.ss.enums;

import orw.ORWLogger;
import orw.ss.SSUtil;

public enum SSShipPartType {
    ARMOR(SSUtil.SSStrings.ARMOR, SSUpgradePartType.ARMOR),
    DRIVE(SSUtil.SSStrings.DRIVE,  SSUpgradePartType.ENGINE, SSSlotType.BACK),
    GUN(SSUtil.SSStrings.GUN, SSUpgradePartType.WEAPON),
    LASER(SSUtil.SSStrings.LASER, SSUpgradePartType.WEAPON),
    SHIELD(SSUtil.SSStrings.SHIELD, SSUpgradePartType.POWER),
    THRUST(SSUtil.SSStrings.THRUST, SSUpgradePartType.ENGINE, SSSlotType.FRONT,
            SSSlotType.BACK),;

    private final String name;
    private final SSUpgradePartType cost;
    private final SSSlotType[] types;

    SSShipPartType(String name,
                           SSUpgradePartType cost, SSSlotType... types) {
        this.name = name;
        this.cost = cost;
        this.types = types;
    }


    public static SSShipPartType get(String name) {
        for (SSShipPartType type : SSShipPartType.values())
            if (type.name.equals(name)) return type;
        ORWLogger.log("SSShipPartType '" + name + "' not found!");
        return null;
    }

    public SSUpgradePartType getCost() {
        return cost;
    }

    public int getCost(SSUpgradePartType upt) {
        if (upt == cost) return 1;
        return 0;
    }

    public boolean canFit(SSSlotType slotType) {
        if (types == null || types.length == 0) return true;
        for (SSSlotType type : types)
            if (type == slotType) return true;
        return false;
    }

}
