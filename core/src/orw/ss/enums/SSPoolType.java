package orw.ss.enums;

import orw.ORWLogger;
import orw.ss.SSUtil;

public enum SSPoolType {
    DEFAULT(SSUtil.SSStrings.DEFAULT, SSUtil.SSStrings.EFFECT_DEFAULT),
    DAMAGE(SSUtil.SSStrings.DAMAGE, SSUtil.SSStrings.EFFECT_DAMAGE),
    EXPLOSION(SSUtil.SSStrings.EXPLOSION, SSUtil.SSStrings.EFFECT_EXPLOSION),
    SHIELD(SSUtil.SSStrings.SHIELD, SSUtil.SSStrings.EFFECT_SHIELD),
    ;

    private final String name;
    private final String source;

    SSPoolType(String name, String source) {
        this.name = name;
        this.source = source;
    }

    public static SSPoolType get(String name) {
        for (SSPoolType type : SSPoolType.values())
            if (type.name.equals(name)) return type;
        ORWLogger.log("SSPoolType '" + name + "' not found!");
        return null;
    }

    public String getSource() {
        return source;
    }

    public String getName() {
        return name;
    }
}
