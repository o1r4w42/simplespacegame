package orw.ss.enums;

import orw.ss.SSUtil;

public enum SSGroupType {
    BACKGROUND, ITEM, PLAYER, ENEMY;

    private final int index;

    SSGroupType() {
        index = SSUtil.SSIntegers.SSGroupTypeInd++;
    }

    public int getInd() {
        return index;
    }
}
