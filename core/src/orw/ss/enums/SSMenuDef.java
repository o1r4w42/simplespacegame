package orw.ss.enums;

import orw.ss.SSUtil;

public enum SSMenuDef {
    GAME;

    private final int index;

    SSMenuDef() {
        index = SSUtil.SSIntegers.SSMenuDefInd++;
    }

    public int getInd() {
        return index;
    }

}
