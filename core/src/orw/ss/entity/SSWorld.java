package orw.ss.entity;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;

import javax.inject.Inject;

import orw.ss.SimpleSpaceGame;
import orw.ss.entity.SSEntityFactory;
import orw.ss.entity.actors.SSBgObject;
import orw.ss.entity.actors.SSItem;
import orw.ss.entity.actors.SSMeteor;
import orw.ss.entity.actors.SSShip;
import orw.ss.entity.view.SSEffectView;
import orw.ss.enums.SSDialogDef;
import orw.ss.enums.SSGroupType;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.input.SSPlayerController;
import orw.ss.physics.SSContactListener;
import orw.ss.screen.SSCamera;

public class SSWorld implements Disposable {

    private final World world;
    private final Box2DDebugRenderer b2dRenderer;
    private final Group[] entityGroups;
//    private final ArrayList<SSEffectView> effectViews;

    @Inject
    SSCamera camera;
    @Inject
    SSEntityFactory entityFactory;
    @Inject
    SSPlayerController playerController;

    private boolean updateWorld;
    private Stage stage;
    private SSShip player;

    public SSWorld(SSInjectionComponent injector) {
        injector.injectWorld(this);

        world = new World(new Vector2(0, 0), true);
        world.setContactListener(new SSContactListener());

        b2dRenderer = new Box2DDebugRenderer();

        entityGroups = new Group[SSGroupType.values().length];
        for (int i = 0; i < entityGroups.length; i++) {
            entityGroups[i] = new Group();
        }
    }

    public void init(Stage stage) {
        this.stage = stage;
        for (Group entityGroup : entityGroups) {
            stage.addActor(entityGroup);
        }

        player = entityFactory.createPlayer();
        playerController.init(player);

        entityFactory.initObjects(getGroup(SSGroupType.BACKGROUND), camera, this);
    }

    public void start() {
        getGroup(SSGroupType.PLAYER).addActor(player);
        camera.setPlayer(player);
        playerController.start();

        entityFactory.initItem(getGroup(SSGroupType.ITEM), camera, this);
        entityFactory.initMeteors(getGroup(SSGroupType.ENEMY), camera, this);

        updateWorld = true;
    }

    public void restart() {
        playerController.stop();
        entityFactory.removeEntities(false);
        start();
    }


    public void render(boolean debug) {
        if (debug) {
            b2dRenderer.render(world, camera.combined);
        }
    }

    public void update(float delta) {
        if (updateWorld) {
//            if (checkSpawn) updateSpawn(delta);

            world.step(delta, 6, 2);

            stage.act(delta);

            playerController.update(delta);

            camera.update(delta);
        }

    }

    private Group getGroup(SSGroupType groupType) {
        return entityGroups[groupType.getInd()];
    }

    public Body createBody(BodyDef bodyDef) {
        return world.createBody(bodyDef);
    }

    public void destroyBody(Body body) {
        world.destroyBody(body);
    }

//    public void addEffectView(SSEffectView view) {
//        effectViews.add(view);
//    }
//
//    public void removeEffectView(SSEffectView view) {
//        effectViews.remove(view);
//    }

    @Override
    public void dispose() {

    }

    public void remove(SSMeteor meteor) {
        entityFactory.remove(meteor, getGroup(SSGroupType.ENEMY), camera, this);
//        checkSpawn = factory.shouldCheckSpawn();
    }

    public void remove(SSShip ship) {
        entityFactory.remove(ship, getGroup(SSGroupType.ENEMY), camera, this);
//        checkSpawn = factory.shouldCheckSpawn();
    }

    public void remove(SSItem item) {
        entityFactory.remove(item, getGroup(SSGroupType.ITEM), camera, this);
//        checkSpawn = factory.shouldCheckSpawn();
    }

    public void remove(SSBgObject object) {
        entityFactory.removeEntitiy(object);
    }


    public void showDialog(SSDialogDef upgrade) {

    }

    public void pauseWorld(boolean pause) {
        updateWorld = pause;
    }
}
