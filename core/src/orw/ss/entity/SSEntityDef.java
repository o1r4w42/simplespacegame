package orw.ss.entity;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.maps.MapProperties;

import java.util.Iterator;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.entity.equip.modules.SSModuleSlots;
import orw.ss.entity.equip.parts.SSPartSlots;
import orw.ss.enums.SSDefType;
import orw.ss.enums.SSEntityProp;
import orw.ss.enums.SSModuleType;
import orw.ss.enums.SSSlotType;
import orw.ss.physics.SSCircleForm;
import orw.ss.physics.SSCollForm;
import orw.ss.physics.SSPolyForm;
import orw.ss.physics.SSRectForm;

public class SSEntityDef extends SSStats {

    private MapProperties properties;
    private SSDefType defType;

    public SSEntityDef(SSDefType type) {
        super(null);
        properties = new MapProperties();
        defType = type;
    }

    @Override
    public String toString() {
        return "SSEntityDef(" + defType + ")";
    }

    public <T> void addProperty(SSEntityProp partType, T value) {
        properties.put(partType.getName(), value);
    }

    public <T> T get(SSEntityProp entityProp, Class<T> clazz, T def) {
        if (properties.containsKey(entityProp.getName()))
            return properties.get(entityProp.getName(), clazz);
        return def;
    }

    public void addSlot(SSSlotType type, float x, float y, float width,
                        float height, boolean add) {
        SSPartSlots slots = null;
        if (!properties.containsKey(SSUtil.SSStrings.SLOTS)) {
            slots = new SSPartSlots(Color.RED);
            properties.put(SSUtil.SSStrings.SLOTS, slots);
        } else slots = properties.get(SSUtil.SSStrings.SLOTS, SSPartSlots.class);
        slots.addSlot(type, x, y, width, height, add);
    }

    public SSDefType getDefType() {
        return defType;
    }

    public void print() {
        ORWLogger.log("SSEntityDef:" + defType);
        Iterator<String> keys = properties.getKeys();
        while (keys.hasNext()) {
            String key = keys.next();
            ORWLogger.log(key + ":" + properties.get(key));
        }
        ORWLogger.log("");
    }

    public void addCircleBody(float x, float y, float radius) {
        addBody(new SSCircleForm(x, y, radius));
    }

    public void addRectBody(float x, float y, float w, float h) {
        addBody(new SSRectForm(x, y, w, h));
    }

    public void addPolyBody(float x, float y, String points) {
        addBody(new SSPolyForm(x, y, points));
    }

    private void addBody(SSCollForm body) {
        properties.put(SSUtil.SSStrings.BODY, body);
    }


}
