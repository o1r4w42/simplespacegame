package orw.ss.entity;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Group;

import java.util.ArrayList;

import javax.inject.Inject;

import orw.ss.SSUtil;
import orw.ss.SSVector;
import orw.ss.SimpleSpaceGame;
import orw.ss.assets.SSAssetManager;
import orw.ss.entity.actors.SSBgObject;
import orw.ss.entity.actors.SSItem;
import orw.ss.entity.actors.SSMeteor;
import orw.ss.entity.actors.SSShip;
import orw.ss.entity.equip.SSEquipFactory;
import orw.ss.enums.SSCollType;
import orw.ss.enums.SSDefType;
import orw.ss.enums.SSEntityStat;
import orw.ss.enums.SSEntityType;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.screen.SSCamera;

public class SSEntityFactory {

    private final TextureAtlas atlas;
    private final ArrayList<SSItem> items;
    private final ArrayList<SSMeteor> meteors;
    private final ArrayList<SSShip> ships;
    private final ArrayList<SSBgObject> objects;
    private final SSInjectionComponent injector;

    @Inject
    SSAssetManager assetManager;

    private SSShip player;
    private int meteorMax = 4;
    private int meteorLvlMin = 1;
    private int meteorLvlMax = 2;
    private int shipMax = 1;

    public SSEntityFactory(SSInjectionComponent injector) {
        injector.injectEntityFactory(this);
        this.injector = injector;
        atlas = assetManager.get(SSUtil.SSStrings.FILE_SPRITE_ATLAS, TextureAtlas.class);
        items = new ArrayList<>();
        meteors = new ArrayList<>();
        ships = new ArrayList<>();
        objects = new ArrayList<>();
    }

    public void initObjects(Group group, SSCamera camera, SSWorld world) {
        int count = 6;
        float step = SSUtil.SSIntegers.SCREEN_HEIGHT;
        float offset = -step * count / 2f;
        for (int i = 0; i < count; i++) {
            for (int j = 0; j < count; j++) {
                float x = step * i + offset;
                float y = step * j + offset;
                add(createObject(x, y), group, camera, world);
            }
        }
    }

    public SSShip createPlayer() {
        SSEntityDef definition = assetManager.getDefinition(SSDefType.FRAME_S_0);
        SSShip ship = new SSShip(definition, SSEntityType.PLAYER, injector);
        initEntity(ship, SSCollType.PLAYER.getId(), SSUtil.SSShorts.collMaskPlayer, 0, 0,
                90f);
        ship.initSpeed(0f, 0f, 0f);
        player = ship;
        return ship;
    }

    public void initItem(Group group, SSCamera camera, SSWorld world) {
        //        makeItem(group, camera, world, true);
        //        add(createItem(0, dist, SSDefType.ITEM_WEAPON), group, camera, world);
        //        add(createItem(0, 220, SSDefType.ITEM_WEAPON), group, camera, world);
        //        add(createItem(200, 0, SSDefType.ITEM_ENGINE), group, camera, world);
        //        add(createItem(0, -200, SSDefType.ITEM_POWER), group, camera, world);
        //        add(createItem(-200, 0, SSDefType.ITEM_WEAPON), group, camera, world);
    }

    public void initMeteors(Group group, SSCamera camera, SSWorld world) {
//        float dist = camera.getSpawnRange(0f) * SSUtil.SSFloats.BODY_TO_WORLD;
//        SSVector tmpVec = SSVector.getVector(dist, 0f);
//        for (int i = 0; i < meteorMax; i++) {
//            tmpVec.setAngle(MathUtils.random(360f));
//            float angle = SSUtil.modAngle(
//                    player.getAngle(tmpVec.getX(), tmpVec.getY()),
//                    180f);
//            angle += 90f - MathUtils.random(180f);
//            float vel = 1f + MathUtils.random();
//            add(createMeteor(tmpVec.getX(), tmpVec.getY(), angle, 0, 0, vel,
//                    MathUtils.random(1, 2)), group, camera, world);
//        }
//        tmpVec.free();

//                add(createMeteor(0, 200, 270, 0, 0, 1, 3), group, camera, world);
        //        add(createMeteor(100, 100, 270, 0, 0, 0, 1), group, camera, world);
        //        add(createMeteor(-100, 100, 0, 0, 0, 0, 1), group, camera, world);
        //        add(createMeteor(100, -100, 270, 0, 0, 0, 2), group, camera, world);
        //        add(createMeteor(-100, -100, 270, 0, 0, 0, 2), group, camera, world);
    }

    private SSBgObject createObject(float x, float y) {
        SSEntityDef definition = assetManager.getDefinition(SSDefType.BG_0);
        SSBgObject object = new SSBgObject(definition, SSEntityType.OBJECT,
                player, injector);
        initEntity(object, (short) 0, (short) 0, x, y, 0);
        return object;
    }

    private void makeItem(Group group, SSCamera camera, SSWorld world,
                          boolean start) {
        float dist = camera.getSpawnRange(0f) * SSUtil.SSFloats.BODY_TO_WORLD;
        SSVector tmpVec = SSVector.getVector(dist, 0f);
        float mod = MathUtils.PI / 4f - MathUtils.random(MathUtils.PI / 2f);
        float tva = player.getVelAngleRad();
        float angle = SSUtil.modAngleRad(tva, mod);
        tmpVec.setAngleRad(angle);
        tmpVec.add(camera.position.x, camera.position.y);
        add(createItem(tmpVec.getX(), tmpVec.getY(), SSItem.randomItem(start)),
                group, camera, world);
        tmpVec.free();
    }

    private SSItem createItem(float x, float y, SSDefType defType) {
        SSEntityDef definition = assetManager.getDefinition(defType);
        SSItem item = new SSItem(definition, SSEntityType.ITEM, player, injector);
        initEntity(item, SSCollType.ITEM.getId(), SSUtil.SSShorts.collMaskItem, x, y, 0);
        return item;
    }


    private void makeMeteor(Group group, SSCamera camera, SSWorld world) {
        float dist = camera.getSpawnRange(0f) * SSUtil.SSFloats.BODY_TO_WORLD;
        SSVector tmpVec = SSVector.getVector(dist, 0f);
        float mod = MathUtils.PI / 2f - MathUtils.random(MathUtils.PI);
        float tva = player.getVelAngleRad();
        float angle = SSUtil.modAngleRad(tva, mod);
        tmpVec.setAngleRad(angle);
        tmpVec.add(camera.position.x, camera.position.y);
        float x = tmpVec.getX();
        float y = tmpVec.getY();
        float vel = 1f + MathUtils.random();
        mod = MathUtils.PI / 4f - MathUtils.random(MathUtils.PI / 2f);
        tmpVec.sub(camera.position.x, camera.position.y);
        angle = SSUtil.modAngleRad(tmpVec.angleRad(), mod);
        add(createMeteor(x, y, angle, 0, 0, vel,
                MathUtils.random(meteorLvlMin, meteorLvlMax)), group,
                camera, world);
        tmpVec.free();
    }

    private SSMeteor createMeteor(float x, float y, float angle, float acceleration,
                                  float rotate, float velocity, int size) {
        SSEntityDef definition;
        switch (size) {
            case 1:
                definition = assetManager.getDefinition(SSDefType.MET_S_0);
                break;
            case 2:
                definition = assetManager.getDefinition(SSDefType.MET_M_0);
                break;
            case 3:
                definition = assetManager.getDefinition(SSDefType.MET_L_0);
                break;
            case 4:
                definition = assetManager.getDefinition(SSDefType.MET_XL_0);
                break;
            default:
                definition = assetManager.getDefinition(SSDefType.MET_S_0);
                break;
        }
        SSMeteor meteor = new SSMeteor(definition, SSEntityType.METEOR, player, injector);
        initEntity(meteor, SSCollType.METEOR.getId(), SSUtil.SSShorts.collMaskMeteor, x,
                y, angle);
        meteor.initSpeed(acceleration, rotate, velocity);
        return meteor;
    }

    private void makeShip(Group group, SSCamera camera, SSWorld world) {
        float dist = camera.getSpawnRange(0f) * SSUtil.SSFloats.BODY_TO_WORLD;
        SSVector tmpVec = SSVector.getVector(dist, 0f);
        float mod = MathUtils.PI / 2f - MathUtils.random(MathUtils.PI);
        float tva = player.getVelAngleRad();
        float angle = SSUtil.modAngleRad(tva, mod);
        tmpVec.setAngleRad(angle);
        tmpVec.add(camera.position.x, camera.position.y);
        float x = tmpVec.getX();
        float y = tmpVec.getY();
        float vel = 1f + MathUtils.random();
        mod = MathUtils.PI / 4f - MathUtils.random(MathUtils.PI / 2f);
        tmpVec.sub(camera.position.x, camera.position.y);
        angle = SSUtil.modAngleRad(tmpVec.angleRad(), mod);
        add(createShip(x, y, angle, 0, 0, vel), group, camera, world);
        tmpVec.free();
    }

    private SSShip createShip(float x, float y, float angle, float acceleration,
                              float rotate, float velocity) {
        SSEntityDef definition = assetManager.getDefinition(SSDefType.SHIP_S_1);
        SSShip ship = new SSShip(definition, SSEntityType.SHIP, player, injector);
        initEntity(ship, SSCollType.ENEMY.getId(), SSUtil.SSShorts.collMaskEnemy, x, y,
                angle);
        ship.initSpeed(acceleration, rotate, velocity);
        return ship;
    }

    private void initEntity(SSEntity entity, short collId, short collMask,
                            float x, float y, float angle) {
        entity.initView(atlas);
        entity.initBody(x * SSUtil.SSFloats.WORLD_TO_BODY, y * SSUtil.SSFloats.WORLD_TO_BODY,
                collId, collMask, angle);
    }

    private void add(SSBgObject object, Group group, SSCamera camera,
                     SSWorld world) {
        objects.add(object);
        addEntity(object, group, camera, world);
    }

    private void add(SSMeteor meteor, Group group, SSCamera camera,
                     SSWorld world) {
        meteors.add(meteor);
        addEntity(meteor, group, camera, world);
    }

    private void add(SSShip ship, Group group, SSCamera camera, SSWorld world) {
        ships.add(ship);
        addEntity(ship, group, camera, world);
    }

    private void add(SSItem item, Group group, SSCamera camera, SSWorld world) {
        items.add(item);
        addEntity(item, group, camera, world);
    }

    private void addEntity(SSEntity entity, Group group, SSCamera camera,
                           SSWorld world) {
        entity.spawn();
        group.addActor(entity);
    }

    public void remove(SSMeteor meteor, Group group, SSCamera camera,
                       SSWorld world) {
        int size = (int) meteor.getStat(SSEntityStat.SIZE);
        if (size > 1) {
            add(createMeteor(meteor.getCXS(), meteor.getCYS(),
                    meteor.getVelAngle(), 0, 0, meteor.getVelocity(),
                    size - 1), group, camera, world);
        } else if (meteors.size() - 1 < meteorMax)
            makeMeteor(group, camera, world);

        meteors.remove(meteor);
        removeEntitiy(meteor);
    }

    public void remove(SSShip ship, Group group, SSCamera camera,
                       SSWorld world) {
        add(createItem(ship.getCXS(), ship.getCYS(), SSDefType.ITEM_ARMOR),
                group, camera, world);
        ships.remove(ship);
        removeEntitiy(ship);
    }

    public void remove(SSItem item, Group group, SSCamera camera,
                       SSWorld world) {
        items.remove(item);
        removeEntitiy(item);
    }


    public void removeEntitiy(SSEntity entity) {
        entity.free();
    }

    public void removeEntities(boolean all) {
        //        playerController.stop();
        for (SSItem item : items)
            item.destroy();
        items.clear();
        for (SSMeteor meteor : meteors)
            meteor.destroy();
        meteors.clear();
        for (SSShip ship : ships)
            ship.destroy();
        ships.clear();
        if (all) {
            for (SSBgObject object : objects)
                object.destroy();
            objects.clear();
        }
    }

    public void checkSpawn(Group itemGroup, Group enemyGroup, SSCamera camera,
                           SSWorld world) {
        //        SSLogger.log("checkSpawn");
        if (items.size() == 0) makeItem(itemGroup, camera, world, false);
        if (meteors.size() < meteorMax)
            for (int i = 0; i < meteorMax - meteors.size(); i++)
                makeMeteor(enemyGroup, camera, world);
        if (ships.size() < shipMax)
            for (int i = 0; i < shipMax - ships.size(); i++)
                makeShip(enemyGroup, camera, world);
    }

    public boolean shouldCheckSpawn() {
        if (items.size() == 0) return true;
        if (meteors.size() < meteorMax) return true;
        if (ships.size() < shipMax) return true;
        return false;
    }


}
