package orw.ss.entity;

import orw.ss.enums.SSEntityStat;

public class SSStats {

    private float[] stats;

    //    public SSStats(boolean def) {
    public SSStats(SSStats other) {
        stats = new float[SSEntityStat.values().length];
        //        for (SSEntityStat stat : SSEntityStat.values())
        //            if (def == null) stats[stat.getIndex()] = stat.getDefault();
        //            else setStat(stat, def.getStat(stat));
        if (other == null) setDefault();
        else setStats(other);
        //        if (other != null) setStats(other);
    }

    private void setDefault() {
        for (SSEntityStat stat : SSEntityStat.values())
            //            stats[stat.getIndex()] = stat.getDefault();
            stats[stat.getIndex()] = 0f;
    }

    public void setStat(SSEntityStat stat, float value) {
        //        SSLogger.log("setStat:" + stat + ", " + value);
        stats[stat.getIndex()] = value;
    }

    public void addStat(SSEntityStat stat, float value) {
        //        SSLogger.log(this + ":modStat:" + stat + ", " + value);
        stats[stat.getIndex()] += value;
    }

    public void multiplyStat(SSEntityStat stat, float value) {
        //        SSLogger.log(this + ":modStat:" + stat + ", " + value);
        stats[stat.getIndex()] *= value;
    }

    public void divideStat(SSEntityStat stat, float value) {
        //        SSLogger.log(this + ":modStat:" + stat + ", " + value);
        stats[stat.getIndex()] /= value;
    }

    public float getStat(SSEntityStat stat) {
        return stats[stat.getIndex()];
    }

    public void modStats(SSStats other, int mod) {
        //        SSLogger.log(this + ":modStats:" + other);
        for (SSEntityStat stat : SSEntityStat.values()){
            if (other.getStat(stat) != 0f){
                if (stat.isAdditive()){
                    addStat(stat, other.getStat(stat) * mod);
                }else if (stat.isMultiplicative()){
                    if(mod>0){
                        multiplyStat(stat, other.getStat(stat));
                    }else{
                        divideStat(stat, other.getStat(stat));
                    }
                }
            }
        }


    }

    public void setStats(SSStats other) {
        //        SSLogger.log(this + ":setStats:" + other);
        for (SSEntityStat stat : SSEntityStat.values())
            //            if (stat.isAdditive())
                setStat(stat, other.getStat(stat));
    }
}
