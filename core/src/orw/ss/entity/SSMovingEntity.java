package orw.ss.entity;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import orw.ss.entity.actors.SSShip;
import orw.ss.entity.equip.parts.SSWeaponLogic;
import orw.ss.entity.logic.SSMovingLogic;
import orw.ss.entity.view.SSMovingView;
import orw.ss.enums.SSEntityType;
import orw.ss.injection.SSInjectionComponent;

public abstract class SSMovingEntity extends SSEntity {

    private SSMovingLogic movingLogic;
    private SSMovingView movingView;

    public SSMovingEntity(SSEntityDef definition, SSEntityType type, SSShip target, SSInjectionComponent injector) {
        super(definition, type, target, injector);
        movingLogic = (SSMovingLogic) logic;
        movingView = (SSMovingView) view;
    }


    @Override
    public void act(float delta) {
        if (isActive()) body.setPositionToBody(this);
        super.act(delta);
    }

    @Override
    public void spawn() {
        super.spawn();
        view.updateAngleRad(body.getBodyAngleRad());
    }

    @Override
    public void initView(TextureAtlas atlas) {
        movingLogic.setViewScales();
        super.initView(atlas);
        movingView.setListener(movingLogic);
    }

    public void initSpeed(float acceleration, float rotate, float velocity) {
        //        movingLogic.init(accell, rotate);
        //        Vector2 vel = body.getLinearVelocity();
        //        vel.set(velocity, 0f);
        //        vel.setAngleRad(body.getAngle());
        //        body.setLinearVelocity(vel);
        body.initVelocity(velocity);
    }

    public void setBodyAngleVel(float rad) {
        body.setVelocityAngleRad(rad);
        setBodyAngle(rad);
    }

    public void setBodyAngle(float rad) {
        //        SSLogger.log("setBodyRotation:" + this);
        body.setAngleRad(rad);
        view.updateAngleRad(rad);
    }

    public float getVelocity() {
        return body.getVelocity();
    }

    /**
     * Get the angle in radians.
     *
     * @return the current world rotation angle of the body in radians.
     */
    public float getBodyAngleRad() {
        return body.getBodyAngleRad();
    }

    public float getVelAngleRad() {
        return body.getVelAngleRad();
    }

    public float getVelAngle() {
        return body.getVelAngle();
    }

    public float getBodyAngle() {
        return body.getBodyAngle();
    }

    public void updateDamage(SSWeaponLogic logic, boolean start) {
        movingLogic.updateDamage(logic, start);
    }

}
