package orw.ss.entity.equip;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import javax.inject.Inject;

import orw.ss.SSUtil;
import orw.ss.assets.SSAssetManager;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.equip.modules.SSShipModule;
import orw.ss.entity.equip.parts.SSShipPart;
import orw.ss.enums.SSDefType;
import orw.ss.injection.SSInjectionComponent;

public class SSEquipFactory {

    private final TextureAtlas atlas;

    @Inject SSAssetManager assetManager;

    public SSEquipFactory(SSInjectionComponent injector) {
        injector.injectEquipFactory(this);
        atlas = assetManager.get(SSUtil.SSStrings.FILE_SPRITE_ATLAS, TextureAtlas.class);
    }

    public SSShipPart createShipPart(SSEntityDef def, Color color,
                                     float degree) {
        SSShipPart part = new SSShipPart(atlas, def, assetManager, color,
                        degree);
        return part;
    }


    public SSShipModule createShipModule(SSEntityDef def) {
        SSShipModule part = new SSShipModule(atlas, def);
        return part;
    }

    public SSEntityDef getDef(SSDefType type) {
        return assetManager.getDefinition(type);
    }
}
