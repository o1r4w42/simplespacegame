package orw.ss.entity.equip;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import java.util.ArrayList;
import java.util.List;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.SSVector;
import orw.ss.entity.SSEntity;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.SSMovingEntity;
import orw.ss.entity.equip.modules.SSModuleSlots;
import orw.ss.entity.equip.modules.SSShipModule;
import orw.ss.entity.equip.parts.SSShipPart;
import orw.ss.entity.logic.SSShipLogic;
import orw.ss.entity.view.SSShipView;
import orw.ss.enums.SSDefType;
import orw.ss.enums.SSDirection;
import orw.ss.enums.SSEntityStat;
import orw.ss.enums.SSEntityState;
import orw.ss.enums.SSModuleType;
import orw.ss.physics.SSBody;
import orw.ss.physics.SSPolyForm;

public class SSShipEquip {

    private final List<SSShipModule> activeModules;
    private final SSEquipFactory equipFactory;

    private PolygonShape shape;
    private Vector2[] vertices;
    private SSModuleSlots moduleSlots;

    public SSShipEquip(SSEquipFactory equipFactory) {
        this.equipFactory = equipFactory;
        this.moduleSlots = new SSModuleSlots();
        activeModules = new ArrayList<>();
    }

    public void init(SSBody body) {
        Fixture fixture = body.getBodyFixture();
        shape = (PolygonShape) fixture.getShape();
        SSPolyForm form = (SSPolyForm) body.getCollForm();
        vertices = form.getVertices();
    }

    public void update(float delta, float x, float y, float angle,
                       float accelerationMod, float rotateMod, SSDirection rotateDir,
                       SSEntityState state) {
        for (SSShipModule module : activeModules)
            module.update(delta, x, y, angle, accelerationMod, rotateMod, rotateDir,
                    state);
    }

    public void renderDebug(ShapeRenderer renderer, float x, float y,
                            float rad) {
        moduleSlots.render(renderer, x, y, rad);
        for (SSShipModule module : activeModules)
            module.renderDebug(renderer, x, y, rad);
    }

    public void clear() {

    }

    public void setModule(int index, SSEntityDef def, SSMovingEntity movingEntity, SSBody body,
                          SSShipLogic logic, SSShipView shipView, float xO, float yO, Color color,
                          boolean force) {
        SSModuleType slotType = moduleSlots.getType(index);
        for (int i = 0; i < moduleSlots.getSize(index); i++) {
            SSShipModule module = equipFactory.createShipModule(def);

            SSShipModule lastModule = moduleSlots.getModule(index, i);
            if (lastModule != null) {
                activeModules.remove(lastModule);
                shipView.removeModule(lastModule);
                logic.modStats(lastModule, -1);
            }

            moduleSlots.setModule(index, i, module);
            activeModules.add(module);
            shipView.addModule(module);
            logic.modStats(module, 1);

            SSVector position = moduleSlots.getPosition(index, i);
            module.init(movingEntity, body, logic, shipView, slotType, position, index, xO, yO, color);
        }
        updateShape(index, movingEntity, force);
        if (!force) {
            moduleSlots.updateSlots(index);
        }
    }

    private void updateShape(int index, SSEntity movingEntity, boolean force) {
        int[][] points = moduleSlots.getPoints(index);
        if (points == null) return;
        for (int[] array : points) {
            float newX = array[1] * .2f;
            float newY = array[2] * .2f;
            Vector2 pos = vertices[array[0]];
            if (force || (pos.x < 0 && newX < pos.x) || (pos.x > 0 && newX > pos.x)) {
                pos.x = newX;
            }
            if (force || (pos.y < 0 && newY < pos.y) || (pos.y > 0 && newY > pos.y)) {
                pos.y = newY;
            }
        }
        shape.set(vertices);

        movingEntity.updateBounds(moduleSlots.getShapeSize());
    }

    public void setPart(int moduleIndex, int slotIndex, SSEntityDef def, float angle, Color color,
                        SSMovingEntity movingEntity, SSBody body, SSShipLogic logic, SSShipView shipView) {
        for (int j = 0; j < moduleSlots.getSize(moduleIndex); j++) {
            SSShipModule module = moduleSlots.getModule(moduleIndex, j);
            if (module == null) return;
            for (int i = 0; i < module.getSize(slotIndex); i++) {
                SSShipPart part = equipFactory.createShipPart(def, color, angle);
                module.setPart(slotIndex, i, part, shipView, logic, movingEntity, body);
            }
        }
    }

    public SSShipModule getModule(int index) {
        return moduleSlots.getModule(index, 0);
    }

    public SSModuleSlots getModuleSlots() {
        return moduleSlots;
    }

    public SSShipPart getModulePart(int moduleIndex, int slotIndex) {
        return moduleSlots.getModule(moduleIndex, 0).getPart(slotIndex, 0);
    }

    public SSEntityDef getDef(SSDefType type) {
        return equipFactory.getDef(type);
    }
}
