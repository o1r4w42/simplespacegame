package orw.ss.entity.equip.modules;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.ArrayList;
import java.util.List;

import orw.ss.SSUtil;
import orw.ss.SSVector;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.SSMovingEntity;
import orw.ss.entity.SSStats;
import orw.ss.entity.equip.parts.SSPartSlots;
import orw.ss.entity.equip.parts.SSShipPart;
import orw.ss.entity.logic.SSShipLogic;
import orw.ss.entity.view.SSShipView;
import orw.ss.entity.view.SSSprite;
import orw.ss.enums.SSDirection;
import orw.ss.enums.SSEntityProp;
import orw.ss.enums.SSEntityStat;
import orw.ss.enums.SSEntityState;
import orw.ss.enums.SSModuleType;
import orw.ss.enums.SSSlotType;
import orw.ss.physics.SSBody;

public class SSShipModule extends SSStats {

    protected final SSPartSlots slots;
    private final SSSprite sprite;
    private final SSSprite frameSprite;
    private final String viewName;
    private final int viewIndex;
    private final String source;
    private final List<SSShipPart[]> parts;
    private final List<SSShipPart> activeParts;
    private final int frameIndex;
    private final String frameName;
    private final String frameSource;
    private final SSSprite colorSprite;

    public SSVector position;
    private SSModuleType moduleType;

    public SSShipModule(TextureAtlas atlas, SSEntityDef definition) {
        super(definition);
        setStat(SSEntityStat.MASS, getStat(SSEntityStat.SIZE) * getStat(SSEntityStat.DENSITY));
        slots = definition.get(SSEntityProp.SLOTS, SSPartSlots.class,
                SSPartSlots.EMPTY);
        parts = new ArrayList<>();
        for (int i = 0; i < slots.getSize(); i++)
            parts.add(new SSShipPart[slots.getSize(i)]);

        activeParts = new ArrayList<>();

        viewName = definition.get(SSEntityProp.VIEW_SRC, String.class,
                SSUtil.SSStrings.EMPTY)
                + definition.get(SSEntityProp.VIEW_NAME, String.class,
                SSUtil.SSStrings.EMPTY);
        viewIndex = definition.get(SSEntityProp.VIEW_IND, int.class, 0);
        source = viewName + SSUtil.SSStrings.UNDER_SCORE + viewIndex;
        sprite = SSSprite.create(atlas, viewName, viewIndex);

        colorSprite = SSSprite.create(atlas, viewName + SSUtil.SSStrings.ENDING_COLOR, viewIndex);

        frameName = SSUtil.SSStrings.DEF_FRAME;
        frameIndex = 0;
        frameSource = frameName + SSUtil.SSStrings.UNDER_SCORE + frameIndex;

        frameSprite = SSSprite.create(atlas, frameName, 0);
    }


    public SSModuleType getModuleType() {
        return moduleType;
    }

    public void init(SSMovingEntity owner, SSBody body, SSShipLogic shipLogic,
                     SSShipView view, SSModuleType moduleType, SSVector position,
                     int index, float xO, float yO, Color color) {
        this.moduleType = moduleType;
        this.position = position;
        sprite.setFlip(false, position.getY() < 0);
        colorSprite.setFlip(false, position.getY() < 0);
        colorSprite.setColor(color);
    }

    public void render(Batch batch) {
        frameSprite.render(batch);
        sprite.render(batch);
        colorSprite.render(batch);
    }

    public void update(float delta, float x, float y, float rad,
                       float accelerationMod, float rotateMod, SSDirection rotateDir,
                       SSEntityState state) {
        SSVector tmpVec = getSpritePosition(x, y, rad);
        updateSprite(tmpVec.getX(), tmpVec.getY(), rad);
        updateParts(delta, tmpVec.getX(), tmpVec.getY(), rad, accelerationMod, rotateMod, rotateDir, state);
        tmpVec.free();
    }

    private SSVector getSpritePosition(float x, float y, float rad) {
        SSVector tmpVec = SSVector.getVector();
        tmpVec.set(1, 0);
        tmpVec.setAngleRad(SSUtil.modAngleRad(position.angleRad(), rad));
        tmpVec.scl(position.len());
        tmpVec.add(x, y);
        return tmpVec;
    }

    private void updateSprite(float x, float y, float rad) {
        frameSprite.setPosition(x, y);
        frameSprite.setAngleRad(rad);
        sprite.setPosition(x, y);
        sprite.setAngleRad(rad);
        colorSprite.setPosition(x, y);
        colorSprite.setAngleRad(rad);
    }

    private void updateParts(float delta, float x, float y, float rad,
                             float accelMod, float rotateMod, SSDirection rotateDir,
                             SSEntityState state) {
        for (SSShipPart part : activeParts) {
            part.update(delta, x, y, rad, accelMod, rotateMod, rotateDir,
                    state, position.getY() < 0);
        }
    }

    public void renderDebug(ShapeRenderer renderer, float x, float y,
                            float rad) {
        SSVector tmpVec = getSpritePosition(x, y, rad);
        slots.render(renderer, tmpVec.getX(), tmpVec.getY(), rad, false,
                position.getY() < 0, moduleType);
        tmpVec.free();
    }

    public String getSpriteSource() {
        return source;
    }

    public String getFrameSource() {
        return frameSource;
    }

    public int getSize() {
        return slots.getSize();
    }

    public int getSize(int index) {
        return slots.getSize(index);
    }

    public SSSlotType getSlotType(int index) {
        return slots.getSlotType(index);
    }

    public void setPart(int slotIndex, int subIndex, SSShipPart part,
                        SSShipView shipView, SSShipLogic logic,
                        SSMovingEntity movingEntity, SSBody body) {
        activeParts.add(part);

        SSShipPart lastPart = getPart(slotIndex, subIndex);
        if (lastPart != null) {
            shipView.removePart(lastPart);
            logic.modStats(lastPart, -1);
        }

        part.init(movingEntity, body, logic, shipView, getSlotType(slotIndex),
                getPosition(slotIndex, subIndex), this);
        shipView.addPart(part);
        logic.modStats(part, 1);
        logic.updateSlotStats(part, getSlotType(slotIndex));

        parts.get(slotIndex)[subIndex] = part;
    }

    public SSShipPart getPart(int slotIndex, int subIndex) {
        return parts.get(slotIndex)[subIndex];
    }

    public SSVector getPosition(int slotIndex, int index) {
        return slots.getPosition(slotIndex, index);
    }

    public SSPartSlots getSlots() {
        return slots;
    }

    public void free(SSBody body) {

    }


    public float getX() {
        return position.getX();
    }

    public float getY() {
        return position.getY();
    }


    public void setVisible(boolean visible) {
        sprite.setVisible(visible);
    }
}
