package orw.ss.entity.equip.modules;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import java.util.HashMap;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.SSVector;
import orw.ss.enums.SSModuleType;

public class SSModuleSlots {

    private static final float BASE_SIZE = .1f;

    private HashMap<Integer, SSModuleSlot> slots;
    private float[][] viewBounds;
    private float[][] sizeBounds;

    public SSModuleSlots() {
        viewBounds = new float[2][2];
        sizeBounds = new float[2][2];
        slots = new HashMap<>();
        addModuleSlot(SSModuleType.MID, 0, -20, 0,
                new int[][]{{0, -2, -1}, {1, -1, -1}, {2, 1, -1}, {3, 2, -1}, {4, 2, 1}, {5, 1, 1}, {6, -1, 1}, {7, -2, 1}});
        addModuleSlot(SSModuleType.MID, 1, 20, 0,
                new int[][]{{0, -2, -1}, {1, -1, -1}, {2, 1, -1}, {3, 2, -1}, {4, 2, 1}, {5, 1, 1}, {6, -1, 1}, {7, -2, 1}});
        addModuleSlot(SSModuleType.MID, 2, -60, 0, new int[][]{{0, -4, -1}, {7, -4, 1}});
        addModuleSlot(SSModuleType.BACK, 3, -20, -40, new int[][]{{0, -2, -1}, {1, -2, -2}, {6, -2, 2}, {7, -2, 1}});
        addModuleSlot(SSModuleType.FRONT, 4, 20, -40, new int[][]{{3, 2, -1}, {2, 2, -2}, {5, 2, 2}, {4, 2, 1}});
        addModuleSlot(SSModuleType.MID, 5, 60, 0, new int[][]{{3, 4, -1}, {4, 4, 1}});
    }

    public void addModuleSlot(SSModuleType type, int index, float x, float y,
                              int[][] points) {
        SSModuleSlot slot = slots.get(index);
        if (slot != null) {
            ORWLogger.log(ORWLogger.LogCategory.ERROR, "Module slot for index " + index + " already set!");
            return;
        }
        slot = new SSModuleSlot(type, x, y, points);
        slots.put(index, slot);
    }

    public void render(ShapeRenderer renderer, float x, float y,
                       float rad) {
        for (SSModuleSlot slot : slots.values())
            slot.render(renderer, x, y, rad);
    }

    public int getSize() {
        return slots.size();
    }

    public int[][] getPoints(int index) {
        return slots.get(index).points;
    }

    public int getSize(int index) {
        return slots.get(index).positions.length;
    }

    public SSVector getPosition(int slotIndex, int index) {
        return slots.get(slotIndex).positions[index];
    }

    public SSModuleType getType(int index) {
        return slots.get(index).type;
    }

    public void updateSlots(int index) {
        SSVector position = getPosition(index, 0);
        float x = position.getX() * SSUtil.SSFloats.BODY_TO_WORLD;
        float y = position.getY() * SSUtil.SSFloats.BODY_TO_WORLD;
        if (getType(index) == SSModuleType.MID) {
            int nextIndex = getSize();
            updateSlot(nextIndex, x, y - 40f);

            float nextX = x - 40f;
            if (x > 0) {
                nextX = x + 40f;
            }
            nextIndex = getSize();
            updateSlot(nextIndex, nextX, y);
        } else {
            int nextIndex = getSize();
            updateSlot(nextIndex, x, y - 40f);

            nextIndex = getSize();
            updateSlot(nextIndex, x - 40f, y);

            nextIndex = getSize();
            updateSlot(nextIndex, x + 40f, y);

            nextIndex = getSize();
            updateSlot(nextIndex, x, y + 40f);
        }
    }

    private void updateSlot(int index, float x, float y) {
        if (!hasSlotAtPosition(x, y)) {
            SSModuleType type = SSModuleType.MID;
            if (y != 0f) {
                type = x > 0 ? SSModuleType.FRONT : SSModuleType.BACK;
            }
            addModuleSlot(type, index, x, y, SSUtil.createShapePoints(x, y, BASE_SIZE));
        } else {
            ORWLogger.log("Position already set for x " + x + ", y " + y);
        }
    }

    private boolean hasSlotAtPosition(float x, float y) {
        x /= SSUtil.SSFloats.BODY_TO_WORLD;
        y /= SSUtil.SSFloats.BODY_TO_WORLD;
        for (SSModuleSlot slot : slots.values()) {
            if (slot.positions[0].equals(x, y)) {
                return true;
            }
        }
        return false;
    }

    public float getViewWidth() {
        return Math.max(-viewBounds[0][0], viewBounds[0][1]) * 2f + .4f;
    }

    public float getViewHeight() {
        return Math.max(-viewBounds[1][0], viewBounds[1][1]) * 2f + .4f;
    }

    public float getShapeSize() {
        return Math.max(getShapeWidth(), getShapeHeight());
    }

    public float getShapeWidth() {
        return Math.max(-sizeBounds[0][0], sizeBounds[0][1]) * 2f + .4f;
    }

    public float getShapeHeight() {
        return Math.max(-sizeBounds[1][0], sizeBounds[1][1]) * 2f + .4f;
    }

    public SSShipModule getModule(int slotIndex, int moduleIndex) {
        return slots.get(slotIndex).modules[moduleIndex];
    }

    public void setModule(int slotIndex, int moduleIndex, SSShipModule module) {
        slots.get(slotIndex).setModule(moduleIndex, module);
    }

    private class SSModuleSlot {

        private SSModuleType type;
        private int[][] points;
        private SSVector[] positions;
        private SSShipModule[] modules;

        SSModuleSlot(SSModuleType type, float x, float y, int[][] points) {
            this.type = type;
            this.points = points;

            x /= SSUtil.SSFloats.BODY_TO_WORLD;
            y /= SSUtil.SSFloats.BODY_TO_WORLD;

            if (type == SSModuleType.MID) {
                positions = new SSVector[1];
                modules = new SSShipModule[1];
                setPosition(0, x, y);
            } else {
                positions = new SSVector[2];
                modules = new SSShipModule[2];
                setPosition(0, x, y);
                setPosition(1, x, -y);
            }
        }

        private void setPosition(int index, float x, float y) {
            viewBounds[0][0] = Math.min(viewBounds[0][0], x);
            viewBounds[0][1] = Math.max(viewBounds[0][1], x);
            viewBounds[1][0] = Math.min(viewBounds[1][0], y);
            viewBounds[1][1] = Math.max(viewBounds[1][1], y);
            positions[index] = SSVector.getVector(x, y);
        }

        public void setModule(int index, SSShipModule module) {
            for (SSVector vector : positions) {
                sizeBounds[0][0] = Math.min(sizeBounds[0][0], vector.getX());
                sizeBounds[0][1] = Math.max(sizeBounds[0][1], vector.getX());
                sizeBounds[1][0] = Math.min(sizeBounds[1][0], vector.getY());
                sizeBounds[1][1] = Math.max(sizeBounds[1][1], vector.getY());
            }
//            ORWLogger.log("x["+sizeBounds[0][0]+"|"+sizeBounds[0][1]+"], y["+sizeBounds[1][0]+"|"+sizeBounds[1][1]+"]");
            modules[index] = module;
        }

        public void render(ShapeRenderer renderer, float x, float y,
                           float rad) {
            SSVector tmpVec = SSVector.getVector();
            ShapeType lastType = renderer.getCurrentType();
            Color lastColor = renderer.getColor();
            renderer.set(ShapeType.Line);
            renderer.setColor(Color.RED);
            for (SSVector pos : positions) {
                tmpVec.set(pos);
                tmpVec.setAngleRad(
                        SSUtil.modAngleRad(tmpVec.angleRad(), rad));
                tmpVec.add(x, y);

                renderer.rect(tmpVec.getX() - BASE_SIZE, tmpVec.getY() - BASE_SIZE,
                        BASE_SIZE * 2f, BASE_SIZE * 2f);
            }
            renderer.set(lastType);
            renderer.setColor(lastColor);
            tmpVec.free();
        }
    }


}
