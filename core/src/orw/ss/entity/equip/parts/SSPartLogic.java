package orw.ss.entity.equip.parts;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import orw.ss.SSUtil;
import orw.ss.SSVector;
import orw.ss.entity.SSMovingEntity;
import orw.ss.entity.logic.SSShipLogic;
import orw.ss.enums.SSDirection;
import orw.ss.enums.SSEntityStat;
import orw.ss.enums.SSEntityState;
import orw.ss.physics.SSBody;

public abstract class SSPartLogic {

    protected SSShipPart part;
    protected SSVector effectOffset;
    private SSMovingEntity owner;


    public SSPartLogic(SSShipPart part) {
        this.part = part;
    }

    public void iniView(TextureAtlas atlas) {

    }

    public void init(SSBody body, SSMovingEntity owner, SSShipLogic logic) {
        this.owner = owner;
        if (part.hasEffect())
            initEffectOffset();
    }

    protected void initEffectOffset() {
        initEffectOffset(0f, 0f);
    }

    protected void initEffectOffset(float xo, float yo) {
        initEffectOffset(part.position.getX(), part.position.getY(), xo, yo);
    }

    protected void initEffectOffset(float x, float y, float xo, float yo) {
        effectOffset = SSVector.getVector(xo, yo);
        effectOffset.setAngleRad(SSUtil.modAngleRad(effectOffset.angleRad(),
                        part.radians));
        effectOffset.add(x, y);
    }

    public void update(float delta, float x, float y, float rad, float accelMod,
                       float rotateMod, SSDirection rotateDir,
                       SSEntityState state, boolean flipY) {
        SSVector tmpVec = getSpritePosition(x, y, rad, flipY);
        updateSprite(tmpVec.getX(), tmpVec.getY(), rad);
        tmpVec.free();
        if (part.hasEffect()) {
            tmpVec = getEffectPosition(x, y, rad, flipY);
            updateEffect(delta, tmpVec.getX(), tmpVec.getY(), rad);
            tmpVec.free();
        }
    }

    protected SSVector getSpritePosition(boolean flipY) {
        return getSpritePosition(owner.getCX(), owner.getCY(),
                        owner.getBodyAngleRad(), flipY);
    }

    protected SSVector getSpritePosition(float x, float y, float rad,
                    boolean flipY) {
        SSVector tmpVec = SSVector.getVector();

        tmpVec.set(part.position);
        if (flipY) tmpVec.setY(tmpVec.getY() * -1);
        tmpVec.setAngleRad(SSUtil.modAngleRad(tmpVec.angleRad(), rad));
        tmpVec.add(x, y);

        //        tmpVec.set(1, 0);
        //        tmpVec.setAngleRad(SSUtil.modAngleRad(part.position.angleRad(), rad));
        //        tmpVec.scl(part.position.len());
        //        tmpVec.add(x, y);
        return tmpVec;
    }

    protected SSVector getEffectPosition(boolean flipY) {
        return getEffectPosition(owner.getCX(), owner.getCY(),
                        owner.getBodyAngleRad(), flipY);
    }

    protected SSVector getEffectPosition(float rad, boolean flipY) {
        return getEffectPosition(owner.getCX(), owner.getCY(), rad, flipY);
    }

    protected SSVector getEffectPosition(float x, float y, float rad,
                    boolean flipY) {
        SSVector tmpVec = SSVector.getVector();

        tmpVec.set(effectOffset);
        if (flipY) tmpVec.setY(tmpVec.getY() * -1);
        tmpVec.setAngleRad(SSUtil.modAngleRad(tmpVec.angleRad(), rad));
        tmpVec.add(x, y);

        //        tmpVec.set(1, 0);
        //        tmpVec.setAngleRad(SSUtil.modAngleRad(effectOffset.angleRad(), rad));
        //        //        tmpVec.setAngleRad(SSUtil.modAngleRad(effectOffset.angleRad(),
        //        //                        SSUtil.modAngleRad(rad, part.radians)));
        //        tmpVec.scl(effectOffset.len());
        //        tmpVec.add(x, y);
        return tmpVec;
    }

    protected void updateSprite(float x, float y, float rad) {
        part.sprite.setPosition(x, y);
        part.sprite.setAngleRad(SSUtil.modAngleRad(rad, part.radians));
    }

    protected void updateEffect(float delta, float x, float y, float rad) {
        part.effect.setPosition(x, y);
    }

    public void render(Batch batch) {

    }

    public float getStat(SSEntityStat stat) {
        return part.getStat(stat);
    }
    //    public void renderDebug(ShapeRenderer renderer, float x, float y,
    //                    float angle) {
    //    }

    public void free(SSBody body) {
        if (part.hasEffect()) effectOffset.free();
    }

    public void setVisible(boolean visible) {

    }

    //    public abstract void removeEffect();

}
