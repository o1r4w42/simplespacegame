package orw.ss.entity.equip.parts;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import orw.ss.SSUtil;
import orw.ss.SSVector;

public class SSLaserLogic extends SSWeaponLogic {

    private Vector2[] vertices;

    public SSLaserLogic(SSShipPart part) {
        super(part, SSUtil.SSStrings.LASER, Color.RED, .1f, 2.56f,
                        .02f);
    }

    @Override
    protected void initShape(PolygonShape shape, float x, float y) {
        vertices = new Vector2[4];
        vertices[0] = new Vector2(0f, -range.getY() / 2f);
        vertices[0].setAngleRad(SSUtil.modAngleRad(vertices[0].angleRad(),
                        part.radians));
        vertices[0].add(x, y);
        vertices[1] = new Vector2(range.getX(), -range.getY() / 2f);
        vertices[1].setAngleRad(SSUtil.modAngleRad(vertices[1].angleRad(),
                        part.radians));
        vertices[1].add(x, y);
        vertices[2] = new Vector2(range.getX(), range.getY() / 2f);
        vertices[2].setAngleRad(SSUtil.modAngleRad(vertices[2].angleRad(),
                        part.radians));
        vertices[2].add(x, y);
        vertices[3] = new Vector2(0f, range.getY() / 2f);
        vertices[3].setAngleRad(SSUtil.modAngleRad(vertices[3].angleRad(),
                        part.radians));
        vertices[3].add(x, y);
        shape.set(vertices);
    }

    @Override
    protected void updateShape(PolygonShape shape, float x, float y) {
        vertices[1].set(range.getX(), -range.getY() / 2f);
        vertices[1].setAngleRad(SSUtil.modAngleRad(vertices[1].angleRad(),
                        part.radians));
        vertices[1].add(x, y);
        vertices[2].set(range.getX(), range.getY() / 2f);
        vertices[2].setAngleRad(SSUtil.modAngleRad(vertices[2].angleRad(),
                        part.radians));
        vertices[2].add(x, y);
        shape.set(vertices);
    }

    @Override
    protected void updateFire(float delta, float x, float y, float rad) {
        rad = SSUtil.modAngleRad(rad, part.radians);
        float scaleY = (.5f + MathUtils.random(1f));
        updateStart(x, y, rad, 1f, scaleY);
        //        float range = getRange();
        float range = getRange() + MathUtils.random(getRange() / 10f)
                        - getRange() / 20f;

        SSVector tmpVec = SSVector.getVector(range / 2f - getMidOffset(), 0f);
        //        tmpVec.set(range / 2f - getMidOffset(), 0f);
        tmpVec.setAngleRad(rad);
        tmpVec.add(x, y);
        updateMid(tmpVec.getX(), tmpVec.getY(), rad, getRangeScale(range),
                        scaleY);

        tmpVec.set(range - getEndOffset() - getMidOffset(), 0f);
        tmpVec.setAngleRad(rad);
        tmpVec.add(x, y);
        updateEnd(tmpVec.getX(), tmpVec.getY(), rad, 1f, scaleY);
        tmpVec.free();
    }



}
