package orw.ss.entity.equip.parts;

import com.badlogic.gdx.graphics.g2d.ParticleEmitter.ScaledNumericValue;
import com.badlogic.gdx.math.MathUtils;

import orw.ss.SSUtil;
import orw.ss.entity.SSMovingEntity;
import orw.ss.entity.logic.SSShipLogic;
import orw.ss.enums.SSDirection;
import orw.ss.enums.SSEntityState;
import orw.ss.physics.SSBody;

public class SSDriveLogic extends SSPartLogic {

    private float[] lifeValues = { 40f, 80f };
    private float[] scaleXValues = { 16f, 24f };

    public SSDriveLogic(SSShipPart part) {
        super(part);
    }

    @Override
    protected void initEffectOffset(float x, float y, float xo, float yo) {
        super.initEffectOffset(x, y, -.16f, 0);
    }

    @Override
    public void init(SSBody body, SSMovingEntity owner, SSShipLogic logic) {
        super.init(body, owner, logic);

        ScaledNumericValue life = part.emitter.getLife();
        life.setLow(lifeValues[0]);
        life.setHigh(lifeValues[1]);

        ScaledNumericValue scaleX = part.emitter.getXScale();
        scaleX.setHigh(0f);
        scaleX.setLow(0f);

        part.view.addEffect(part.effect);
    }

    @Override
    public void update(float delta, float x, float y, float rad, float accelMod,
                       float rotateMod, SSDirection rotateDir,
                       SSEntityState state, boolean flipY) {
        super.update(delta, x, y, rad, accelMod, rotateMod, rotateDir, state,
                        flipY);
        ScaledNumericValue eAngle = part.emitter.getAngle();
        float a = SSUtil.modAngleRad(rad, -MathUtils.PI)
                        * MathUtils.radiansToDegrees;
        eAngle.setHigh(a);
        eAngle.setLow(a);

        ScaledNumericValue scaleX = part.emitter.getXScale();
        scaleX.setHigh(scaleXValues[0] * accelMod * SSUtil.SSFloats.WORLD_TO_BODY);
        scaleX.setLow(scaleXValues[1] * accelMod * SSUtil.SSFloats.WORLD_TO_BODY);
    }


}
