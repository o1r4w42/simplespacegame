package orw.ss.entity.equip.parts;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import orw.ss.SSUtil;
import orw.ss.SSVector;

public class SSGunLogic extends SSWeaponLogic {

    private float firePos;
    private float fireRad;
    private float fireSpeed;
    private Vector2[] vertices;

    public SSGunLogic(SSShipPart part) {
        super(part, SSUtil.SSStrings.GUN, Color.YELLOW, .2f, 2.56f,
                        .04f);
    }

    @Override
    protected void initShape(PolygonShape shape, float x, float y) {
        vertices = new Vector2[4];
        vertices[0] = new Vector2(0f, -range.getY() / 2f);
        vertices[0].setAngleRad(SSUtil.modAngleRad(vertices[0].angleRad(),
                        part.radians));
        vertices[0].add(x, y);
        vertices[1] = new Vector2(range.getX(), -range.getY());
        vertices[1].setAngleRad(SSUtil.modAngleRad(vertices[1].angleRad(),
                        part.radians));
        vertices[1].add(x, y);
        vertices[2] = new Vector2(range.getX(), range.getY());
        vertices[2].setAngleRad(SSUtil.modAngleRad(vertices[2].angleRad(),
                        part.radians));
        vertices[2].add(x, y);
        vertices[3] = new Vector2(0f, range.getY() / 2f);
        vertices[3].setAngleRad(SSUtil.modAngleRad(vertices[3].angleRad(),
                        part.radians));
        vertices[3].add(x, y);
        shape.set(vertices);
    }

    @Override
    protected void updateShape(PolygonShape shape, float x, float y) {
        vertices[1].set(range.getX(), -range.getY());
        vertices[1].setAngleRad(SSUtil.modAngleRad(vertices[1].angleRad(),
                        part.radians));
        vertices[1].add(x, y);
        vertices[2].set(range.getX(), range.getY());
        vertices[2].setAngleRad(SSUtil.modAngleRad(vertices[2].angleRad(),
                        part.radians));
        vertices[2].add(x, y);
        shape.set(vertices);
    }

    @Override
    protected void updateFire(float delta, float x, float y, float rad) {
        rad = SSUtil.modAngleRad(rad, part.radians);
        //        rad = SSUtil.modAngleRad(rad, fireRad);
        //        float scaleY = MathUtils.random(1.5f);
        //        float scaleY = 1f;
        float range = getRange();
        if (firePos >= range) {
            firePos = 0f;
            updateAngle();
        } else {
            //            firePos += delta * 12.8f;
            //            firePos += delta * 25.6f;
            //            firePos += delta * 2.56f;
            //            fireSpeed = 1f;
            firePos += delta * fireSpeed;
            if (firePos > range)
                firePos = range;
        }
        float mod = 0f;
        if (firePos > 0f) mod = range / firePos;
        float scaleY = Math.abs(MathUtils.lerp(.6f, .2f, mod) + .1f);
        if (scaleY > 2f) scaleY = 2f;
        //        scaleY = 1f;
        //        SSLogger.log("scaleY:" + scaleY);

        updateStart(x, y, rad, 1f, scaleY);
        //        if (firePos > 0f) scaleY = range / firePos;

        float scaleX = getRangeScale(firePos);
        float xR = firePos / 2f;
        if (firePos > range / 2f) {
            scaleX = getRangeScale(range - firePos);
            xR += firePos - range / 2f;
            //            scaleY = 1.1f - firePos / range;
            //            scaleY = MathUtils.lerp(1f, .1f,
            //                            range / 2f / (firePos - range / 2f));
        }
        //        float scaleYMid = scaleY*1.5f; 
        SSVector tmpVec = SSVector.getVector(xR - getMidOffset(), 0f);
        tmpVec.setAngleRad(rad);
        tmpVec.add(x, y);
        updateMid(tmpVec.getX(), tmpVec.getY(), rad, scaleX, scaleY * 1.5f);

        //        float scaleYEnd = scaleY*1.5f;
        tmpVec.set(firePos - getMidOffset() - getEndOffset(), 0f);
        tmpVec.setAngleRad(rad);
        tmpVec.add(x, y);
        updateEnd(tmpVec.getX(), tmpVec.getY(), rad, 1f, scaleY * 2f);

        tmpVec.free();
    }

    @Override
    protected void updateFire(boolean fire) {
        super.updateFire(fire);
        firePos = 0f;
        updateAngle();
    }

    private void updateAngle() {
        fireRad = MathUtils.PI / 128f - MathUtils.random(MathUtils.PI / 64f);
        fireSpeed = 12.8f + MathUtils.random(25.6f);
    }


}
