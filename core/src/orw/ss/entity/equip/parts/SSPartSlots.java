package orw.ss.entity.equip.parts;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import java.util.ArrayList;
import java.util.List;

import orw.ss.SSUtil;
import orw.ss.SSVector;
import orw.ss.enums.SSModuleType;
import orw.ss.enums.SSSlotType;

public class SSPartSlots {

    public static final SSPartSlots EMPTY = new SSPartSlots(Color.RED);
    private List<SSPartSlot> slots;
    private Color debugColor;

    public SSPartSlots(Color debugColor) {
        this.debugColor = debugColor;
        slots = new ArrayList<>();
    }

    public void addSlot(SSSlotType type, float x, float y, float width,
                        float height, boolean add) {
        SSPartSlot slot = new SSPartSlot(type, x, -y, width, height);
        if (add) slot.addSlot(x, y);
        slots.add(slot);
    }

    public SSSlotType getSlotType(int ind) {
        return slots.get(ind).type;
    }

    public int getSize() {
        return slots.size();
    }

    public int getSize(int ind) {
        return slots.get(ind).positions.size();
    }

    public int getSlotSize(int ind) {
        return (int) (slots.get(ind).size * SSUtil.SSFloats.BODY_TO_WORLD);
    }

    public SSVector getPosition(int index, int sloiIndex) {
        return slots.get(index).positions.get(sloiIndex);
    }

    public void render(ShapeRenderer renderer, float x, float y, float angle,
                       boolean flippedX, boolean flippedY, SSModuleType moduleType) {
        for (SSPartSlot slot : slots)
            slot.render(renderer, x, y, angle, flippedX, flippedY, moduleType);
    }

    public class SSPartSlot {

        private List<SSVector> positions;
        private SSSlotType type;
        private float size;

        public SSPartSlot(SSSlotType type, float x, float y, float width,
                            float height) {
            positions = new ArrayList<>();
            this.type = type;
            positions.add(SSVector.getVector(x * SSUtil.SSFloats.WORLD_TO_BODY,
                    y * SSUtil.SSFloats.WORLD_TO_BODY));
            size = Math.max(width * SSUtil.SSFloats.WORLD_TO_BODY,
                    height * SSUtil.SSFloats.WORLD_TO_BODY);
        }

        public void addSlot(float x, float y) {
            positions.add(SSVector.getVector(x * SSUtil.SSFloats.WORLD_TO_BODY,
                    y * SSUtil.SSFloats.WORLD_TO_BODY));
        }

        public void render(ShapeRenderer renderer, float x, float y,
                           float angle, boolean flippedX, boolean flippedY,
                           SSModuleType moduleType) {
            if (!moduleType.checkType(type)) return;

            SSVector tmpVec = SSVector.getVector();
            ShapeType lastType = renderer.getCurrentType();
            Color lastColor = renderer.getColor();
            renderer.set(ShapeType.Filled);
            renderer.setColor(debugColor);
            for (SSVector pos : positions) {
                tmpVec.set(pos);
                if (flippedY) tmpVec.setY(tmpVec.getY() * -1);
                tmpVec.setAngleRad(
                        SSUtil.modAngleRad(tmpVec.angleRad(), angle));
                tmpVec.add(x, y);

                renderer.rect(tmpVec.getX() - size, tmpVec.getY() - size,
                        size * 2f, size * 2f);
            }
            renderer.set(lastType);
            renderer.setColor(lastColor);
            tmpVec.free();
        }

    }
}
