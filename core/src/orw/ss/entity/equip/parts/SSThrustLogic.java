package orw.ss.entity.equip.parts;

import com.badlogic.gdx.graphics.g2d.ParticleEmitter.ScaledNumericValue;
import com.badlogic.gdx.math.MathUtils;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.entity.SSMovingEntity;
import orw.ss.entity.logic.SSShipLogic;
import orw.ss.enums.SSDirection;
import orw.ss.enums.SSEntityState;
import orw.ss.physics.SSBody;

public class SSThrustLogic extends SSPartLogic {

    private float[] lifeValues = {20f, 40f};
    private float[] scaleXValues = {4f, 8f};
    private boolean thrustChange;
    private SSDirection direction;
    private SSDirection thrustDir;
    private SSEntityState lastState;
    private SSDirection lastRotateDir;
    private boolean wasThrusting;
    private int[] countValues = {20, 200};
    private boolean thrusting;

    public SSThrustLogic(SSShipPart part) {
        super(part);
    }

    @Override
    protected void initEffectOffset(float x, float y, float xo, float yo) {
        float posY = part.parent.getY() + part.position.getY();
        float yOffset = .04f;
        if (posY < 0 && part.parent.getY() == 0f) {
            yOffset = -.04f;
        }
        super.initEffectOffset(x, y, 0f, yOffset);
    }

    @Override
    public void init(SSBody body, SSMovingEntity owner, SSShipLogic logic) {
        super.init(body, owner, logic);
        thrusting = false;
        thrustChange = false;
        lastRotateDir = SSDirection.LEFT;
        wasThrusting = false;
        lastState = SSEntityState.IDLE;

        ScaledNumericValue life = part.emitter.getLife();
        life.setLow(lifeValues[0]);
        life.setHigh(lifeValues[1]);

        ScaledNumericValue scaleX = part.emitter.getXScale();
        scaleX.setHigh(scaleXValues[0]);
        scaleX.setLow(scaleXValues[1]);

        part.emitter.setMinParticleCount(countValues[0]);
        part.emitter.setMaxParticleCount(countValues[1]);

        direction = SSDirection.RIGHT;
        thrustDir = SSDirection.RIGHT;
        float posX = part.parent.getX() + part.position.getX();
        float posY = part.parent.getY() + part.position.getY();
        if (posX > 0) {
            if (posY < 0) {
                direction = SSDirection.LEFT;
            } else {
                thrustDir = SSDirection.LEFT;
            }
        } else {
            if (posY > 0) {
                direction = SSDirection.LEFT;
                thrustDir = SSDirection.LEFT;
            }
        }

        part.emitter.setMaxParticleCount(0);
        part.view.addEffect(part.effect);
    }

    @Override
    public void update(float delta, float x, float y, float rad, float accelerationMod,
                       float rotateMod, SSDirection rotateDir, SSEntityState state, boolean flipY) {
        super.update(delta, x, y, rad, accelerationMod, rotateMod, rotateDir, state, flipY);

        thrustChange = false;
        thrustChange |= (rotateMod != 0) != wasThrusting;
        thrustChange |= lastRotateDir != rotateDir;
        thrustChange |= lastState != SSEntityState.ROTATE
                && state == SSEntityState.ROTATE;
        thrustChange |= lastState == SSEntityState.ROTATE
                && state != SSEntityState.ROTATE;
        thrustChange |= lastState != SSEntityState.DRIFT
                && state == SSEntityState.DRIFT;
        thrustChange |= lastState == SSEntityState.DRIFT
                && state != SSEntityState.DRIFT;


        if (thrustChange) {
            thrusting = activateThrusting(rotateDir, rotateMod != 0, state);
            if (thrusting) {
                part.emitter.setMinParticleCount(countValues[0]);
                part.emitter.setMaxParticleCount(countValues[1]);
            } else {
                part.emitter.setMaxParticleCount(0);
                part.emitter.setMinParticleCount(0);
            }
        }
        if (thrusting) {
            ScaledNumericValue eAngle = part.emitter.getAngle();

            float a = SSUtil.modAngleRad(rad, -MathUtils.PI / 2) * MathUtils.radiansToDegrees;
            if (thrustDir == SSDirection.LEFT) {
                a = SSUtil.modAngleRad(rad, MathUtils.PI / 2) * MathUtils.radiansToDegrees;
            }

            eAngle.setHigh(a);
            eAngle.setLow(a);
            ScaledNumericValue scaleX = part.emitter.getXScale();
            if (state == SSEntityState.DRIFT) {
                scaleX.setHigh(scaleXValues[0] * SSUtil.SSFloats.WORLD_TO_BODY * 2f);
                scaleX.setLow(scaleXValues[1] * SSUtil.SSFloats.WORLD_TO_BODY * 2f);
            } else {
                scaleX.setHigh(scaleXValues[0] * rotateMod
                        * SSUtil.SSFloats.WORLD_TO_BODY);
                scaleX.setLow(scaleXValues[1] * rotateMod * SSUtil.SSFloats.WORLD_TO_BODY);
            }
        }
        wasThrusting = rotateMod != 0;
        lastRotateDir = rotateDir;
        lastState = state;
    }


    private boolean activateThrusting(SSDirection rotateDir, boolean thrusting, SSEntityState state) {
        if (state == SSEntityState.DRIFT) return thrustDir != rotateDir;
        return thrusting && (
                (state == SSEntityState.ROTATE && direction == rotateDir) ||
                        (state != SSEntityState.ROTATE && direction != rotateDir));
    }

}
