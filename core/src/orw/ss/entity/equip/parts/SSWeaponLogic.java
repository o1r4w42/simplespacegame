package orw.ss.entity.equip.parts;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import java.util.ArrayList;
import java.util.List;

import orw.ss.SSUtil;
import orw.ss.SSVector;
import orw.ss.entity.SSMovingEntity;
import orw.ss.entity.logic.SSMovingLogic;
import orw.ss.entity.logic.SSShipLogic;
import orw.ss.entity.view.SSSprite;
import orw.ss.enums.SSCollType;
import orw.ss.enums.SSDirection;
import orw.ss.enums.SSEntityState;
import orw.ss.physics.SSBody;
import orw.ss.physics.SSWeaponFixData;

public abstract class SSWeaponLogic extends SSPartLogic {


    private SSEntityState lastState;
    private boolean fireChange;
    protected SSVector range;
    private Fixture fixture;
    private SSShipLogic shipLogic;
    private List<SSMovingEntity> targets;
    private float baseRange;
    private float rangeOffset;
    private SSMovingEntity currentTarget;
    private SSEntityState currentState;
    private SSSprite startSprite;
    private SSSprite midSprite;
    private SSSprite endSprite;
    private float scale;
    private String spriteName;
    private Color spriteColor;

    public SSWeaponLogic(SSShipPart part,
                    String spriteName, Color spriteColor, float scale,
                    float rangeX, float rangeY) {
        super(part);
        this.spriteName = spriteName;
        this.spriteColor = spriteColor;
        this.scale = scale;
        this.baseRange = rangeX;
        range = SSVector.getVector(rangeX, rangeY);
        //        hitBoxPos = SSVector.getVector();
        targets = new ArrayList<SSMovingEntity>();
        //        SSLogger.log("dmg:" + getStat(SSEntityStat.DMG));
    }

    @Override
    public void iniView(TextureAtlas atlas) {
        super.iniView(atlas);
        //        startSprite = new SSSprite(
        //                        atlas.createSprite("effects/" + spriteName + "_start"),
        //                        spriteColor);
        //        midSprite = new SSSprite(
        //                        atlas.createSprite("effects/" + spriteName + "_mid"),
        //                        spriteColor);
        //        endSprite = new SSSprite(
        //                        atlas.createSprite("effects/" + spriteName + "_end"),
        //                        spriteColor);
        startSprite = SSSprite.create(atlas, "effects/" + spriteName + "_start",
                        -1, spriteColor);
        midSprite = SSSprite.create(atlas, "effects/" + spriteName + "_mid", -1,
                        spriteColor);
        endSprite = SSSprite.create(atlas, "effects/" + spriteName + "_end", -1,
                        spriteColor);
        startSprite.setVisible(false);
        midSprite.setVisible(false);
        endSprite.setVisible(false);
    }

    //    @Override
    //    protected void initEffectOffset(float x, float y) {
    //        super.initEffectOffset(x, y, .08f);
    //    }

    @Override
    public void init(SSBody body, SSMovingEntity owner, SSShipLogic logic) {
        super.init(body, owner, logic);
        logic.updateRange(range.getX());
        initEffectOffset(.08f, 0f);
        //        hitBoxPos.add(part.position.getX() + range.getX() / 2f,
        //                        part.position.getY());
        shipLogic = logic;
        lastState = SSEntityState.IDLE;

        FixtureDef fdef = new FixtureDef();
        fdef.filter.categoryBits = 0;
        fdef.filter.maskBits = 0;
        fdef.isSensor = true;
        PolygonShape shape = new PolygonShape();
        //        hitBoxPos.setAsBox(range.getX() / 2f, range.getY() / 2f, shape);
        float y = effectOffset.getY();
        if (part.parent.getY() < 0) y *= -1f;
        initShape(shape, effectOffset.getX() + part.parent.getX(),
                        y + part.parent.getY());
        fdef.shape = shape;
        fixture = body.createFixture(fdef, false, owner);
        fixture.setUserData(new SSWeaponFixData(owner, this));
    }

    @Override
    public void update(float delta, float x, float y, float rad, float accelMod,
                    float rotateMod, SSDirection rotateDir,
                    SSEntityState state, boolean flipY) {
        checkFireChanged(state);

        super.update(delta, x, y, rad, accelMod, rotateMod, rotateDir, state,
                        flipY);
        SSVector tmpVec = getEffectPosition(x, y, rad, flipY);

        updateEffect(delta, tmpVec.getX(), tmpVec.getY(), rad);
        tmpVec.free();

        if (fireChange) updateFireChange(state);

        lastState = state;
    }

    @Override
    public void render(Batch batch) {
        super.render(batch);
        if (currentState == SSEntityState.ATTACK)
            renderFire(batch);
    }

    @Override
    protected void updateEffect(float delta, float x, float y, float rad) {
        if (targets.size() > 0) setTarget(findTarget(x, y));
        if (currentState == SSEntityState.ATTACK)
            updateFire(delta, x, y, rad);
    }

    @Override
    public void free(SSBody body) {
        super.free(body);
        body.destroyFixture(fixture);
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (!visible) {
            startSprite.setVisible(visible);
            midSprite.setVisible(visible);
            endSprite.setVisible(visible);
        }
    }
    private void checkFireChanged(SSEntityState state) {
        currentState = state;
        fireChange = false;
        fireChange |= lastState != SSEntityState.ATTACK
                        && state == SSEntityState.ATTACK;
        fireChange |= lastState == SSEntityState.ATTACK
                        && state != SSEntityState.ATTACK;
    }


    private void updateRange(float length, float offset) {
        rangeOffset = offset;
        range.setX(length + offset);

        float y = effectOffset.getY();
        if (part.parent.getY() < 0) y *= -1f;
        updateShape((PolygonShape) fixture.getShape(),
                        effectOffset.getX() + part.parent.getX(),
                        y + part.parent.getY());
    }

    private void updateFireChange(SSEntityState state) {
        Filter filter = fixture.getFilterData();
        if (state == SSEntityState.ATTACK) {
            filter.categoryBits = SSCollType.WEAPON.getId();
            filter.maskBits = SSUtil.SSShorts.collMaskWeapon;
            updateFire(true);
        } else {
            filter.categoryBits = 0;
            filter.maskBits = 0;
            updateFire(false);
            updateRange(baseRange, 0f);
            targets.clear();
        }
        fixture.setFilterData(filter);
    }

    public float getRange() {
        return range.getX() - rangeOffset;
    }


    public SSMovingLogic getOwnerLogic() {
        return shipLogic;
    }

    public void addTarget(SSMovingEntity target) {
        if (!targets.contains(target)) {
            targets.add(target);
            if (currentTarget == null) {
                setTarget(target);
            } else {
                SSVector tmpVec = getEffectPosition(false);
                float distOld = currentTarget.getDistance(tmpVec.getX(),
                                tmpVec.getY());
                float distNew = target.getDistance(tmpVec.getX(),
                                tmpVec.getY());
                tmpVec.free();
                if (distNew < distOld)
                    setTarget(target);
            }
        }
    }

    private void setTarget(SSMovingEntity target) {
        setTarget(target, true);
    }
    private void setTarget(SSMovingEntity target, boolean update) {
        if (currentTarget == target) return;
        if (currentTarget != null && update)
            currentTarget.updateDamage(this, false);
        currentTarget = target;
        if (currentTarget != null) currentTarget.updateDamage(this, true);
    }

    public void removeTarget(SSMovingEntity target) {
        removeTarget(target, true);
    }

    public void removeTarget(SSMovingEntity target, boolean update) {
        targets.remove(target);
        if (target == currentTarget) {
            SSVector tmpVec = getEffectPosition(false);
            setTarget(findTarget(tmpVec.getX(), tmpVec.getY()), update);
            tmpVec.free();
        }
        if (targets.size() == 0) updateRange(baseRange, 0f);
    }


    private SSMovingEntity findTarget(float x, float y) {
        SSMovingEntity target = null;
        float r = range.getX();
        float ro = 0f;
        float length = r;
        for (SSMovingEntity e : targets) {
            float dist = e.getDistance(x, y);
            if (dist <= length) {
                length = dist;
                target = e;
                ro = e.getWidth() / 2f;
            }
        }
        if (length + ro < r) updateRange(length, ro);
        return target;
    }

    public void renderFire(Batch batch) {
        startSprite.render(batch);
        midSprite.render(batch);
        endSprite.render(batch);
    }

    protected void updateFire(boolean fire) {
        startSprite.setVisible(fire);
        midSprite.setVisible(fire);
        endSprite.setVisible(fire);
    }

    protected void updateStart(float x, float y, float rad, float scaleX,
                    float scaleY) {
        startSprite.setPosition(x, y);
        startSprite.setAngleRad(rad);
        startSprite.setScale(scaleX * scale, scaleY * scale);
    }

    protected void updateMid(float x, float y, float rad, float scaleX,
                    float scaleY) {
        midSprite.setPosition(x, y);
        midSprite.setAngleRad(rad);
        midSprite.setScale(scaleX * scale, scaleY * scale);
    }

    protected void updateEnd(float x, float y, float rad, float scaleX,
                    float scaleY) {
        endSprite.setPosition(x, y);
        endSprite.setAngleRad(rad);
        endSprite.setScale(scaleX * scale, scaleY * scale);
    }

    protected float getRangeScale(float range) {
        float rangeScale = (range - startSprite.getWidth() * scale
                        - endSprite.getWidth() * scale) / midSprite.getWidth()
                        / scale;
        if (rangeScale < 0f) return 0f;
        return rangeScale;
    }

    protected float getMidOffset() {
        return midSprite.getWidth() / 2f * scale;
    }

    protected float getEndOffset() {
        return endSprite.getWidth() / 2f * scale;
    }

    protected abstract void updateFire(float delta, float x, float y,
                    float rad);

    protected abstract void initShape(PolygonShape shape, float x, float y);

    protected abstract void updateShape(PolygonShape shape, float x, float y);
}
