package orw.ss.entity.equip.parts;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;

import orw.ss.SSUtil;
import orw.ss.SSVector;
import orw.ss.assets.SSAssetManager;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.SSMovingEntity;
import orw.ss.entity.SSStats;
import orw.ss.entity.equip.modules.SSShipModule;
import orw.ss.entity.logic.SSShipLogic;
import orw.ss.entity.view.SSEffect;
import orw.ss.entity.view.SSShipView;
import orw.ss.entity.view.SSSprite;
import orw.ss.enums.SSDirection;
import orw.ss.enums.SSEntityProp;
import orw.ss.enums.SSEntityState;
import orw.ss.enums.SSPoolType;
import orw.ss.enums.SSShipPartType;
import orw.ss.enums.SSSlotType;
import orw.ss.physics.SSBody;

public class SSShipPart extends SSStats {

    private SSPartLogic logic;
    protected SSSlotType slotType;
    protected SSVector position;
    protected SSEffect effect;
    protected ParticleEmitter emitter;
    protected SSShipView view;
    protected SSSprite sprite;
    private String viewName;
    private int viewIndex;
    private SSPoolType poolType;
    private String drawableName;
    protected float radians;
    private SSShipPartType type;
    protected SSShipModule parent;

    public SSShipPart(TextureAtlas atlas, SSEntityDef definition,
                      SSAssetManager assetManager, Color color,
                      float degree) {
        super(definition);
        radians = degree * MathUtils.degreesToRadians;
        type = definition.get(SSEntityProp.SHIP_PART_TYPE, SSShipPartType.class,
                null);
        drawableName = definition.get(SSEntityProp.ICON, String.class,
                SSUtil.SSStrings.EMPTY);
        poolType = definition.get(SSEntityProp.EFFECT, SSPoolType.class,
                null);
        if (poolType != null) {
            effect = assetManager.getEffect(poolType, true);
            emitter = effect.findEmitter(SSUtil.SSStrings.DEF_EMITTER);
        }
        initLogic(type);
        initView(atlas, definition, color);
    }

    public SSShipPartType getType() {
        return type;
    }

    private void initLogic(SSShipPartType type) {
        switch (type) {
            case DRIVE:
                logic = new SSDriveLogic(this);
                break;
            case THRUST:
                logic = new SSThrustLogic(this);
                break;
            case GUN:
                logic = new SSGunLogic(this);
                break;
            case ARMOR:
                logic = new SSArmorLogic(this);
                break;
            case SHIELD:
                logic = new SSShieldLogic(this);
                break;
            case LASER:
                logic = new SSLaserLogic(this);
                break;
            default:
                break;
        }
    }

    private void initView(TextureAtlas atlas, SSEntityDef definition, Color color) {
        viewName = definition.get(SSEntityProp.VIEW_SRC, String.class,
                SSUtil.SSStrings.EMPTY)
                + definition.get(SSEntityProp.VIEW_NAME, String.class,
                SSUtil.SSStrings.EMPTY);
        viewIndex = definition.get(SSEntityProp.VIEW_IND, int.class, 0);
        //        sprite = new SSSprite(atlas.createSprite(viewName, viewIndex));
        sprite = SSSprite.create(atlas, viewName, viewIndex);
        //        sprite.setAngleRad(radians);
        //        if (isPlayer) {
        //            sprite.setColor(Color.BLUE);
        //        } else {
        //            sprite.setColor(Color.RED);
        //        }
        logic.iniView(atlas);
    }

    public String getDrawableName() {
        return drawableName;
    }

    public void free(SSBody body) {
        logic.free(body);
    }

    public boolean hasEffect() {
        return poolType != null;
    }

    public void init(SSMovingEntity owner, SSBody body, SSShipLogic shipLogic,
                     SSShipView view, SSSlotType slotType,
                     SSVector position, SSShipModule parent) {
        //        SSLogger.log("init:");
        this.view = view;
        this.slotType = slotType;
        this.position = position;
        this.parent = parent;
        float posY = parent.getY() + position.getY();
        sprite.setFlip(false, posY > 0);
        if (radians != 0f && posY < 0) {
            if (slotType == SSSlotType.BACK)
                radians = SSUtil.modAngleRad(radians, MathUtils.PI / 2f);
            else radians = SSUtil.modAngleRad(radians, -MathUtils.PI / 2f);
        }

        logic.init(body, owner, shipLogic);
    }

    public void update(float delta, float x, float y, float rad, float accelMod,
                       float rotateMod, SSDirection rotateDir,
                       SSEntityState state, boolean flipY) {
        logic.update(delta, x, y, rad, accelMod, rotateMod, rotateDir, state,
                flipY);
    }

    public void render(Batch batch) {
        sprite.render(batch);
        logic.render(batch);
    }

    public int getLevel() {
        return 1;
    }


    public void removeEffects() {
        if (hasEffect()) view.removeEffect(effect);
    }

    public void setVisible(boolean visible) {
        sprite.setVisible(visible);
        logic.setVisible(visible);
    }

}
