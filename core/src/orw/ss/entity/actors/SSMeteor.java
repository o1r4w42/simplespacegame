package orw.ss.entity.actors;

import com.badlogic.gdx.physics.box2d.Fixture;

import orw.ss.entity.SSEntity;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.SSMovingEntity;
import orw.ss.entity.logic.SSEntityLogic;
import orw.ss.entity.logic.SSMeteorLogic;
import orw.ss.entity.view.SSEntityView;
import orw.ss.entity.view.SSMeteorView;
import orw.ss.enums.SSEntityType;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.physics.SSFixtureData;

public class SSMeteor extends SSMovingEntity {

    protected SSMeteorLogic meteorLogic;
    private SSMeteorView meteorView;

    public SSMeteor(SSEntityDef definition, SSEntityType type,
                    SSShip target, SSInjectionComponent injector) {
        super(definition, type, target, injector);
    }

    @Override
    protected SSEntityView createView(SSInjectionComponent injector) {
        meteorView = new SSMeteorView(injector);
        return meteorView;
    }

    @Override
    protected SSEntityLogic createLogic(SSShip target, SSInjectionComponent injector) {
        meteorLogic = new SSMeteorLogic(this, definition, body,
                meteorView, target, injector);
        return meteorLogic;
    }


    @Override
    public void parseCollision(Fixture ownFix, SSFixtureData ownData,
                               Fixture otherFix, SSFixtureData otherData, SSEntity other) {
        other.collideWith(this, ownFix, ownData, otherFix, otherData);
    }

    @Override
    public void collideWith(SSShip ship, Fixture otherFix,
                            SSFixtureData otherData, Fixture ownFix,
                            SSFixtureData ownData) {
        meteorLogic.updateCollision(ship, ship.shipLogic, otherFix, otherData,
                ownFix, ownData);
    }

    @Override
    public void collideWith(SSMeteor meteor, Fixture otherFix,
                            SSFixtureData otherData, Fixture ownFix,
                            SSFixtureData ownData) {
        meteorLogic.updateCollision(meteor, meteor.meteorLogic, otherFix,
                otherData, ownFix, ownData);
    }

    @Override
    public void collideWith(SSItem item, Fixture otherFix,
                            SSFixtureData otherData, Fixture ownFix,
                            SSFixtureData ownData) {

    }

    @Override
    public void parseCollisionEnd(Fixture ownFix, SSFixtureData ownData,
                                  Fixture otherFix, SSFixtureData otherData, SSEntity other) {
        other.endCollisionWith(this, ownFix, ownData, otherFix, otherData);
    }

    @Override
    public void endCollisionWith(SSShip ship, Fixture otherFix,
                                 SSFixtureData otherData, Fixture ownFix,
                                 SSFixtureData ownData) {
        meteorLogic.updateCollisionEnd(ship, ship.shipLogic, otherFix,
                otherData, ownFix, ownData);
    }

    @Override
    public void endCollisionWith(SSItem item, Fixture otherFix,
                                 SSFixtureData otherData, Fixture ownFix,
                                 SSFixtureData ownData) {

    }

    @Override
    public void endCollisionWith(SSMeteor meteor, Fixture otherFix,
                                 SSFixtureData otherData, Fixture ownFix,
                                 SSFixtureData ownData) {
        meteorLogic.updateCollisionEnd(meteor, meteor.meteorLogic, otherFix,
                otherData, ownFix, ownData);
    }

    //    @Override
    //    protected void createFixtures(short collId, short collMask) {
    //        FixtureDef fdef = new FixtureDef();
    //        fdef.filter.categoryBits = collId;
    //        fdef.filter.maskBits = collMask;
    //        fdef.isSensor = true;
    //
    //        CircleShape shape = new CircleShape();
    //        shape.setRadius(definition.get(SSEntityProp.BODY, float.class,
    //                        Floats.defaultRadius) * Floats.worldToBody);
    //        fdef.shape = shape;
    //        Fixture f = body.createFixture(fdef);
    //        f.setUserData(new SSBodyFixData(this));
    //    }
}
