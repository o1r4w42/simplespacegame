package orw.ss.entity.actors;

import com.badlogic.gdx.physics.box2d.Fixture;

import orw.ss.entity.SSEntity;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.SSMovingEntity;
import orw.ss.entity.equip.SSEquipFactory;
import orw.ss.entity.logic.SSEnemyLogic;
import orw.ss.entity.logic.SSEntityLogic;
import orw.ss.entity.logic.SSPlayerLogic;
import orw.ss.entity.logic.SSShipLogic;
import orw.ss.entity.view.SSEntityView;
import orw.ss.entity.view.SSShipView;
import orw.ss.enums.SSDirection;
import orw.ss.enums.SSEntityState;
import orw.ss.enums.SSEntityType;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.physics.SSFixtureData;

public class SSShip extends SSMovingEntity {

    private SSShipView shipView;
    protected SSShipLogic shipLogic;
    private SSPlayerLogic playerLogic;

    public SSShip(SSEntityDef definition, SSEntityType type,
                  SSInjectionComponent injector) {
        this(definition, type, null, injector);
    }

    public SSShip(SSEntityDef definition, SSEntityType type,
                  SSShip target, SSInjectionComponent injector) {
        super(definition, type, target, injector);
        shipLogic = (SSShipLogic) logic;
    }


    @Override
    protected SSEntityLogic createLogic(SSShip target, SSInjectionComponent injector) {
        switch (entityType) {
            case PLAYER:
                playerLogic = new SSPlayerLogic(this, definition, body,
                        shipView, injector);
                return playerLogic;
            case SHIP:
                return new SSEnemyLogic(this, definition, body, shipView,
                        target, injector);
            default:
                return new SSEnemyLogic(this, definition, body, shipView,
                        target, injector);
        }
    }

    @Override
    protected SSEntityView createView(SSInjectionComponent injector) {
        shipView = new SSShipView(injector);
        return shipView;
    }

    @Override
    public void spawn() {
        super.spawn();
        shipLogic.createEquip();
    }

    @Override
    public void parseCollision(Fixture ownFix, SSFixtureData ownData,
                               Fixture otherFix, SSFixtureData otherData, SSEntity other) {
        other.collideWith(this, ownFix, ownData, otherFix, otherData);
    }

    @Override
    public void collideWith(SSShip ship, Fixture otherFix,
                            SSFixtureData otherData, Fixture ownFix,
                            SSFixtureData ownData) {
        shipLogic.updateCollision(ship, ship.shipLogic, otherFix, otherData,
                ownFix, ownData);
    }

    @Override
    public void collideWith(SSMeteor meteor, Fixture otherFix,
                            SSFixtureData otherData, Fixture ownFix,
                            SSFixtureData ownData) {
        shipLogic.updateCollision(meteor, meteor.meteorLogic, otherFix,
                otherData, ownFix, ownData);
    }

    @Override
    public void collideWith(SSItem item, Fixture otherFix,
                            SSFixtureData otherData, Fixture ownFix,
                            SSFixtureData ownData) {
        item.getCollected(this);
    }

    @Override
    public void parseCollisionEnd(Fixture ownFix, SSFixtureData ownData,
                                  Fixture otherFix, SSFixtureData otherData, SSEntity other) {
        other.endCollisionWith(this, ownFix, ownData, otherFix, otherData);
    }

    @Override
    public void endCollisionWith(SSShip ship, Fixture otherFix,
                                 SSFixtureData otherData, Fixture ownFix,
                                 SSFixtureData ownData) {
        shipLogic.updateCollisionEnd(ship, ship.shipLogic, otherFix, otherData,
                ownFix, ownData);
    }

    @Override
    public void endCollisionWith(SSItem item, Fixture otherFix,
                                 SSFixtureData otherData, Fixture ownFix,
                                 SSFixtureData ownData) {

    }

    @Override
    public void endCollisionWith(SSMeteor meteor, Fixture otherFix,
                                 SSFixtureData otherData, Fixture ownFix,
                                 SSFixtureData ownData) {
        shipLogic.updateCollisionEnd(meteor, meteor.meteorLogic, otherFix,
                otherData, ownFix, ownData);
    }

    //    public void createEquip() {
    //        shipLogic.setSlot(0, SSDefType.PART_DRIVE_0,
    //                        entityType == SSEntityType.PLAYER, false, 0f);
    //        //        shipLogic.setSlot(0, SSDefType.PART_LASER_0,
    //        //                        entityType == SSEntityType.PLAYER, false, 135f);
    //        shipLogic.setSlot(1, SSDefType.PART_THRUST_0,
    //                        entityType == SSEntityType.PLAYER, false, 0f);
    //        //        shipLogic.setSlot(2, SSDefType.PART_GUN_0,
    //        //                        entityType == SSEntityType.PLAYER, false, 45f);
    //        //        shipLogic.setSlot(3, SSDefType.PART_LASER_0,
    //        //                        entityType == SSEntityType.PLAYER, false, 0f);
    //        //        shipLogic.setSlot(3, SSDefType.PART_THRUST_0,
    //        //                        entityType == SSEntityType.PLAYER);
    //        logic.updatePosition(getCX(), getCY());
    //    }

    //    public void installPart(SSShipPartType type, int index,
    //                    boolean enterUpgrade, float angle) {
    //        //        SSLogger.log("installPart:" + index);
    //        int size = shipLogic.getSlots().getSlotSize(index);
    //        //        SSLogger.log("installPart:" + size);
    //        if (enterUpgrade && !playerLogic.removeParts(type.getCost(), size))
    //            return;
    //        switch (type) {
    //        case ARMOR:
    //            break;
    //        case DRIVE:
    //            shipLogic.setSlot(index, SSDefType.PART_DRIVE_0,
    //                            entityType == SSEntityType.PLAYER, enterUpgrade,
    //                            angle);
    //            break;
    //        case GUN:
    //            shipLogic.setSlot(index, SSDefType.PART_GUN_0,
    //                            entityType == SSEntityType.PLAYER, enterUpgrade,
    //                            angle);
    //            break;
    //        case SHIELD:
    //            break;
    //        case THRUST:
    //            shipLogic.setSlot(index, SSDefType.PART_THRUST_0,
    //                            entityType == SSEntityType.PLAYER, enterUpgrade,
    //                            angle);
    //            break;
    //        case LASER:
    //            shipLogic.setSlot(index, SSDefType.PART_LASER_0,
    //                            entityType == SSEntityType.PLAYER, enterUpgrade,
    //                            angle);
    //            break;
    //        }
    //    }

    public void fire() {
        enterState(SSEntityState.ATTACK);
    }

    public void rotate(SSDirection dir) {
        shipLogic.setDirection(dir);
        enterState(SSEntityState.ROTATE);
    }

    public void drift(SSDirection dir) {
        if (shipLogic.canDrift()) {
            shipLogic.setDirection(dir);
            enterState(SSEntityState.DRIFT);
        } else rotate(dir);
    }

    public void fly() {
        enterState(SSEntityState.FLY);
    }

    public void stop() {
        body.stop();
    }

    public SSShipView getShipView() {
        return shipView;
    }


    public SSPlayerLogic getLogic() {
        return playerLogic;
    }


}
