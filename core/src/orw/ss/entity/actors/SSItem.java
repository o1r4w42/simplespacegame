package orw.ss.entity.actors;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Fixture;

import orw.ss.SSUtil;
import orw.ss.entity.SSEntity;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.logic.SSEntityLogic;
import orw.ss.entity.logic.SSItemLogic;
import orw.ss.entity.view.SSEntityView;
import orw.ss.entity.view.SSItemView;
import orw.ss.enums.SSDefType;
import orw.ss.enums.SSEntityType;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.physics.SSFixtureData;

public class SSItem extends SSEntity {

    private static SSDefType[] items = {
            SSDefType.ITEM_ENGINE, SSDefType.ITEM_ENGINE,
            SSDefType.ITEM_WEAPON, SSDefType.ITEM_WEAPON
    };

    public static SSDefType randomItem(boolean first) {
        if (first) {
            items[0] = SSDefType.ITEM_ENGINE;
            items[1] = SSDefType.ITEM_ENGINE;
            items[2] = SSDefType.ITEM_WEAPON;
            items[3] = SSDefType.ITEM_WEAPON;
        }
        SSDefType item = items[0];
        items[0] = items[1];
        items[1] = items[2];
        items[2] = items[3];
        SSDefType nextItem = getNext();
        while (nextItem == items[3])
            nextItem = getNext();
        items[3] = nextItem;
        return item;
    }

    private static SSDefType getNext() {
        switch (MathUtils.random(0, 3)) {
            case 0:
                return SSDefType.ITEM_ARMOR;
            case 1:
                return SSDefType.ITEM_ENGINE;
            case 2:
                return SSDefType.ITEM_POWER;
            case 3:
                return SSDefType.ITEM_WEAPON;
            default:
                return SSDefType.ITEM_WEAPON;
        }
    }

    private SSItemLogic itemLogic;
    private SSItemView itemView;

    public SSItem(SSEntityDef definition, SSEntityType type, SSShip target, SSInjectionComponent injector) {
        super(definition, type, target, injector);
        itemLogic = (SSItemLogic) logic;
        itemView = (SSItemView) view;
    }

    @Override
    protected SSEntityLogic createLogic(SSShip target, SSInjectionComponent injector) {
        return new SSItemLogic(this, definition, body, view, target, injector);
    }


    @Override
    public void initView(TextureAtlas atlas) {
        super.initView(atlas);
        itemView.setListener(itemLogic);
    }

    @Override
    public void parseCollision(Fixture ownFix, SSFixtureData ownData,
                               Fixture otherFix, SSFixtureData otherData, SSEntity other) {
        other.collideWith(this, ownFix, ownData, otherFix, otherData);
    }

    @Override
    public void collideWith(SSShip ship, Fixture otherFix,
                            SSFixtureData otherData, Fixture ownFix,
                            SSFixtureData ownData) {
        getCollected(ship);
    }

    @Override
    public void collideWith(SSItem item, Fixture otherFix,
                            SSFixtureData otherData, Fixture ownFix,
                            SSFixtureData ownData) {

    }

    @Override
    public void collideWith(SSMeteor meteor, Fixture otherFix,
                            SSFixtureData otherData, Fixture ownFix,
                            SSFixtureData ownData) {

    }

    @Override
    public void parseCollisionEnd(Fixture ownFix, SSFixtureData ownData,
                                  Fixture otherFix, SSFixtureData otherData, SSEntity other) {

    }

    @Override
    public void endCollisionWith(SSShip ship, Fixture otherFix,
                                 SSFixtureData otherData, Fixture ownFix,
                                 SSFixtureData ownData) {

    }

    @Override
    public void endCollisionWith(SSItem item, Fixture otherFix,
                                 SSFixtureData otherData, Fixture ownFix,
                                 SSFixtureData ownData) {

    }

    @Override
    public void endCollisionWith(SSMeteor meteor, Fixture otherFix,
                                 SSFixtureData otherData, Fixture ownFix,
                                 SSFixtureData ownData) {

    }

    @Override
    protected SSEntityView createView(SSInjectionComponent injector) {
        return new SSItemView(injector);
    }

    @Override
    public String toString() {
        return SSUtil.SSStrings.ITEM;
    }

    public void getCollected(SSShip ship) {
        itemLogic.getCollected(ship.getLogic());
    }


}
