package orw.ss.entity.actors;

import com.badlogic.gdx.physics.box2d.Fixture;

import orw.ss.entity.SSEntity;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.logic.SSBgObjectLogic;
import orw.ss.entity.logic.SSEntityLogic;
import orw.ss.entity.view.SSBgObjectView;
import orw.ss.entity.view.SSEntityView;
import orw.ss.enums.SSEntityType;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.physics.SSFixtureData;

public class SSBgObject extends SSEntity {

    private SSBgObjectLogic objectLogic;

    public SSBgObject(SSEntityDef definition, SSEntityType type,
                      SSShip target, SSInjectionComponent injector) {
        super(definition, type, target, injector);
        objectLogic = (SSBgObjectLogic) logic;
    }

    @Override
    protected SSEntityView createView(SSInjectionComponent injector) {
        return new SSBgObjectView(injector);
    }

    @Override
    protected SSEntityLogic createLogic(SSShip target, SSInjectionComponent injector) {
        return new SSBgObjectLogic(this, definition, body, view, target, injector);
    }

    @Override
    public void parseCollision(Fixture ownFix, SSFixtureData ownData,
                    Fixture otherFix, SSFixtureData otherData, SSEntity other) {

    }

    @Override
    public void collideWith(SSShip ship, Fixture otherFix,
                    SSFixtureData otherData, Fixture ownFix,
                    SSFixtureData ownData) {
    }

    @Override
    public void collideWith(SSItem item, Fixture otherFix,
                    SSFixtureData otherData, Fixture ownFix,
                    SSFixtureData ownData) {

    }

    @Override
    public void collideWith(SSMeteor meteor, Fixture otherFix,
                    SSFixtureData otherData, Fixture ownFix,
                    SSFixtureData ownData) {

    }

    @Override
    public void initBody(float x, float y, short collId,
                    short collMask, float angle) {
        super.initBody(x, y, collId, collMask, angle);
        objectLogic.initPosition(x, y);
    }

    @Override
    public void parseCollisionEnd(Fixture ownFix, SSFixtureData ownData,
                    Fixture otherFix, SSFixtureData otherData, SSEntity other) {

    }

    @Override
    public void endCollisionWith(SSShip ship, Fixture otherFix,
                    SSFixtureData otherData, Fixture ownFix,
                    SSFixtureData ownData) {

    }

    @Override
    public void endCollisionWith(SSItem item, Fixture otherFix,
                    SSFixtureData otherData, Fixture ownFix,
                    SSFixtureData ownData) {

    }

    @Override
    public void endCollisionWith(SSMeteor meteor, Fixture otherFix,
                    SSFixtureData otherData, Fixture ownFix,
                    SSFixtureData ownData) {

    }
}
