package orw.ss.entity;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.scenes.scene2d.Actor;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.SSVector;
import orw.ss.entity.actors.SSItem;
import orw.ss.entity.actors.SSMeteor;
import orw.ss.entity.actors.SSShip;
import orw.ss.entity.logic.SSEntityLogic;
import orw.ss.entity.view.SSEffectView;
import orw.ss.entity.view.SSEntityView;
import orw.ss.enums.SSDefType;
import orw.ss.enums.SSEntityStat;
import orw.ss.enums.SSEntityState;
import orw.ss.enums.SSEntityType;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.physics.SSBody;
import orw.ss.physics.SSContact;
import orw.ss.physics.SSFixtureData;

public abstract class SSEntity extends Actor {

    protected SSEntityDef definition;
    protected SSEntityLogic logic;
    protected SSEntityView view;
    protected SSEntityType entityType;
    protected SSBody body;

    public SSEntity(SSEntityDef definition, SSEntityType type, SSShip target, SSInjectionComponent injector) {
        super();
        this.definition = definition;
        entityType = type;
        view = createView(injector);
        body = new SSBody(injector);
        logic = createLogic(target, injector);
    }


    @Override
    public String toString() {
        return "SSEntity(" + entityType + ")";
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        view.update(delta);
        logic.update(delta);
        body.update(this);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        view.render(batch);
    }

    @Override
    public void drawDebug(ShapeRenderer shapes) {
        super.drawDebug(shapes);
        if (isActive()) logic.renderDebug(shapes, getCX(), getCY());
    }

    @Override
    public void setSize(float width, float height) {
        super.setSize(width, height);
        setOrigin(width / 2f, height / 2f);
    }

    public void updateBounds(float size) {
        float xO = getWidth() - size;
        float yO = getHeight() - size;
        setSize(size, size);
        setPosition(getX() + xO / 2f, getY() + yO / 2f);
        logic.updateBounds(size);
    }

    public void initView(TextureAtlas atlas) {
        view.initSprite(atlas, definition);
        setSize(view.getWidth(), view.getHeight());
        if (view instanceof SSEffectView)
            ((SSEffectView) view).initEffects(atlas);
        logic.viewLoaded();
    }


    public void initBody(float x, float y, short collId,
                         short collMask, float angle) {
        body.init(x, y, collId, collMask, angle, logic.getBodyType());
    }

    public void spawn() {
        logic.spawn(this);
        view.spawn();
    }


    public void reset(boolean resetBody) {
        if (resetBody) body.reset(this);
        view.reset(getCX(), getCY(), body.getBodyAngleRad());
        logic.reset(definition);
    }

    public void destroy() {
        free();
        logic.destroy();
    }

    public float getStat(SSEntityStat stat) {
        return logic.getStat(stat);
    }

    public void setBodyPosition(SSVector pos) {
        setBodyPosition(pos.getX(), pos.getY());
    }

    public void setBodyPosition(float x, float y) {
        body.setPosition(x, y);
        updatePosition(x, y);
    }

    public void updatePosition(float x, float y) {
        setPosition(x - getOriginX(), y - getOriginY());
        view.updatePosition(getCX(), getCY());
        logic.updatePosition(getCX(), getCY());
    }

    public SSEntityDef getDefinition() {
        return definition;
    }


    public float getCX() {
        return getX() + getOriginX();
    }

    public float getCY() {
        return getY() + getOriginY();
    }

    public float getCXS() {
        return getCX() * SSUtil.SSFloats.BODY_TO_WORLD;
    }

    public float getCYS() {
        return getCY() * SSUtil.SSFloats.BODY_TO_WORLD;
    }

    public void enterState(SSEntityState state) {
        logic.enterState(state);
    }

    public float getSize() {
        return Math.max(getWidth(), getHeight());
    }

    public float getDistance(SSEntity other) {
        SSVector tmpVec = SSVector.getVector(getCX(), getCY());
        tmpVec.sub(other.getCX(), other.getCY());
        tmpVec.free();
        return tmpVec.len();
    }

    public float getDistance(float x, float y) {
        SSVector tmpVec = SSVector.getVector(getCX(), getCY());
        tmpVec.sub(x, y);
        tmpVec.free();
        return tmpVec.len();
    }

        public float getAngleRad(SSEntity other) {
        return getAngleRad(other.getCX(), other.getCY());
    }

    public float getAngleRad(float x, float y) {
        SSVector tmpVec = SSVector.getVector(x, y);
        tmpVec.sub(getCX(), getCY());
        tmpVec.free();
        return tmpVec.angleRad();
    }

    public float getAngle(SSEntity other) {
        return getAngle(other.getCX(), other.getCY());
    }

    public float getAngle(float x, float y) {
        SSVector tmpVec = SSVector.getVector(x, y);
        tmpVec.sub(getCX(), getCY());
        tmpVec.free();
        return tmpVec.angle();
    }

    public boolean isActive() {
        return logic.isActive();
    }


    public void free() {
        logic.free();
        body.free();
    }

    protected abstract SSEntityView createView(SSInjectionComponent injector);

    protected abstract SSEntityLogic createLogic(SSShip target, SSInjectionComponent injector);

    public void addCollision(Contact contact) {
        body.addCollision(SSContact.getContact(contact));
    }


    public void removeCollision(Contact contact) {
        body.removeCollision(SSContact.getContact(contact));
    }

    public abstract void parseCollision(Fixture ownFix,
                                        SSFixtureData ownData, Fixture otherFix,
                                        SSFixtureData otherData, SSEntity other);

    public abstract void parseCollisionEnd(Fixture ownFix,
                                           SSFixtureData ownData, Fixture otherFix,
                                           SSFixtureData otherData, SSEntity other);

    public abstract void collideWith(SSShip ship, Fixture otherFix,
                                     SSFixtureData otherData, Fixture ownFix,
                                     SSFixtureData ownData);

    public abstract void collideWith(SSItem item, Fixture otherFix,
                                     SSFixtureData otherData, Fixture ownFix,
                                     SSFixtureData ownData);

    public abstract void collideWith(SSMeteor meteor, Fixture otherFix,
                                     SSFixtureData otherData, Fixture ownFix,
                                     SSFixtureData ownData);

    public abstract void endCollisionWith(SSShip ship, Fixture otherFix,
                    SSFixtureData otherData, Fixture ownFix,
                    SSFixtureData ownData);

    public abstract void endCollisionWith(SSItem item, Fixture otherFix,
                    SSFixtureData otherData, Fixture ownFix,
                    SSFixtureData ownData);

    public abstract void endCollisionWith(SSMeteor meteor, Fixture otherFix,
                    SSFixtureData otherData, Fixture ownFix,
                    SSFixtureData ownData);


}
