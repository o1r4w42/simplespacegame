package orw.ss.entity.logic;

import com.badlogic.gdx.math.MathUtils;

import orw.ss.SSUtil;
import orw.ss.SSVector;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.actors.SSMeteor;
import orw.ss.entity.actors.SSShip;
import orw.ss.entity.view.SSMeteorView;
import orw.ss.enums.SSEntityStat;
import orw.ss.enums.SSEntityType;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.physics.SSBody;

public class SSMeteorLogic extends SSMovingLogic {

    private SSShip target;
    private SSMeteor meteor;
    private SSMeteorView meteorView;

    public SSMeteorLogic(SSMeteor meteor, SSEntityDef definition,
                         SSBody body, SSMeteorView meteorView, SSShip target, SSInjectionComponent injector) {
        super(SSEntityType.METEOR, meteor, definition, body, meteorView, injector);
        this.meteor = meteor;
        this.target = target;
        this.meteorView = meteorView;
    }

    @Override
    public boolean update(float delta) {
        if (isOutOfRange()) respawn();
        else super.update(delta);
        return true;
    }

    @Override
    public void setViewScales() {
        float size = getStat(SSEntityStat.SIZE);
        movingView.setScales(.2f * size, .1f * size);
        meteorView.setScale(.5f * size);
    }

    @Override
    protected void removeEntity() {
        world.remove(meteor);
    }

    @Override
    protected void despawn(SSEntityLogic other) {
        super.despawn(other);
        if (other.getType() == SSEntityType.PLAYER)
            ((SSPlayerLogic) other).destroyed(this);
    }

    private void respawn() {
        entity.reset(false);

        float tva = target.getVelAngleRad();
        float dist = camera.getSpawnRange(entity.getSize());
        SSVector tmpVec = SSVector.getVector(dist, 0f);

        float mod = MathUtils.PI / 2f - MathUtils.random(MathUtils.PI);
        float angle = SSUtil.modAngleRad(tva, mod);

        tmpVec.setAngleRad(angle);
        tmpVec.add(camera.position.x, camera.position.y);
        entity.setBodyPosition(tmpVec);
        tmpVec.free();
        mod = MathUtils.PI / 4f - MathUtils.random(MathUtils.PI / 2f);
        angle = entity.getAngleRad(camera.position.x, camera.position.y);
        angle = SSUtil.modAngleRad(angle, mod);

        movingEntity.setBodyAngleVel(angle);
    }

    public int getScore() {
        return 1;
    }
}
