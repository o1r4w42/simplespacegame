package orw.ss.entity.logic;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;

import java.util.ArrayList;
import java.util.List;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.SSVector;
import orw.ss.entity.SSEntity;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.SSMovingEntity;
import orw.ss.entity.equip.parts.SSWeaponLogic;
import orw.ss.entity.view.SSEffect;
import orw.ss.entity.view.SSEffectView;
import orw.ss.entity.view.SSMovingView;
import orw.ss.enums.SSCollType;
import orw.ss.enums.SSDirection;
import orw.ss.enums.SSEntityStat;
import orw.ss.enums.SSEntityState;
import orw.ss.enums.SSEntityType;
import orw.ss.enums.SSPoolType;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.physics.SSBody;
import orw.ss.physics.SSFixtureData;
import orw.ss.physics.SSWeaponFixData;

public abstract class SSMovingLogic extends SSEntityLogic {

    protected final SSMovingEntity movingEntity;
    private final List<SSWeaponLogic> damages;
    protected final SSMovingView movingView;

    private boolean collisionDamage;
    private float collisionDamageTime;

    public SSMovingLogic(SSEntityType type, SSMovingEntity entity, SSEntityDef definition,
                         SSBody body, SSMovingView view, SSInjectionComponent injector) {
        super(type, entity, definition, body, view, BodyDef.BodyType.DynamicBody, injector);
        setStats();
        damages = new ArrayList<>();
        movingEntity = entity;
        movingView = view;
    }

    @Override
    protected SSLogicState initDefaultState() {
        return new SSMovingState();
    }

    @Override
    protected SSLogicState initDespawnState() {
        return new SSMovingDespawnState();
    }

    @Override
    public boolean update(float delta) {
        if (!super.update(delta)) return false;
        float hpReg = getStat(SSEntityStat.HP_REG);
        if (hpReg != 0f) regHp(hpReg * delta);
        for (SSWeaponLogic logic : damages)
            getDamaged(delta * logic.getStat(SSEntityStat.DMG),
                    logic.getOwnerLogic(), logic);
        if (collisionDamage) {
            collisionDamageTime += delta;
            if (collisionDamageTime >= .2f) {
                collisionDamageTime = 0f;
                collisionDamage = false;
                if (damages.size() == 0) movingView.showDamage(false);
            }
        }
        return true;
    }

    @Override
    public void reset(SSEntityDef definition) {
        super.reset(definition);
        setStats();
    }

    @Override
    public void free() {

    }

    @Override
    public boolean isOutOfRange() {
        return isAlive() && super.isOutOfRange();
    }

    protected void setStats() {
        //TODO fix
        setStat(SSEntityStat.VELOCITY_MAX, 4f);
        setStat(SSEntityStat.DMG_ARMOR, 1f);
        setStat(SSEntityStat.DMG_SHIELD, 1f);
    }

    private void regHp(float step) {
        float hitPointMax = getStat(SSEntityStat.HP_MAX);
        float hitPoints = getStat(SSEntityStat.HP);
        if (hitPointMax != hitPoints) {
            if (hitPoints + step <= hitPointMax) addStat(SSEntityStat.HP, step);
            else setStat(SSEntityStat.HP, hitPointMax);
            hitPointsChanged();
        }
    }

    public float getVelocity() {
        return body.getVelocity();
    }

    public float getVelocityMax() {
        return (float) (getStat(SSEntityStat.VELOCITY_MAX) / Math.sqrt(getStat(SSEntityStat.MASS)));
    }

    public float getBodyAngle() {
        return body.getBodyAngle();
    }

    protected void hitPointsChanged() {

    }

    protected void velocityChanged() {

    }

    protected void rotationChanged() {

    }

    protected void allStatsChanged(){
        hitPointsChanged();
        velocityChanged();
        rotationChanged();
    }

    public void updateCollision(SSMovingEntity other, SSMovingLogic otherLogic,
                                Fixture otherFix, SSFixtureData otherData, Fixture ownFix,
                                SSFixtureData ownData) {
        if (SSCollType.WEAPON
                .equalsBit(otherData.getType().getId())) {
            getWeaponDmg((SSWeaponFixData) otherData);
            return;
        } else if (SSCollType.WEAPON
                .equalsBit(ownData.getType().getId())) {
            otherLogic.getWeaponDmg((SSWeaponFixData) ownData);
            return;
        }

        float size = getStat(SSEntityStat.SIZE);
        float otherSize = otherLogic.getStat(SSEntityStat.SIZE);

        float ownSizeMod = otherSize / size;
        float otherSizeMod = size / otherSize;

        float ownAngle = movingEntity.getBodyAngleRad();
        float otherAngle = other.getBodyAngleRad();
        float angleDif = SSUtil.getAngleDiffRad(ownAngle, otherAngle);

        float dmg = computeDamage(angleDif, movingEntity.getVelocity(),
                other.getVelocity());
        float ownDmg = dmg * ownSizeMod;
        float otherDmg = dmg * otherSizeMod;

        otherAngle = entity.getAngle(other);
        ownAngle = other.getAngle(entity);
        SSDirection ownDir = SSUtil.getAngleDir(ownAngle,
                body.getBodyAngle());
        SSDirection otherDir = SSUtil.getAngleDir(otherAngle,
                other.getBodyAngle());

        if (ownDmg > 0) getDamaged(ownDmg, otherLogic, null);
        if (otherDmg > 0) otherLogic.getDamaged(otherDmg, this, null);
        if (isAlive())
            rotateCollision(ownDir, ownSizeMod);
        //            setStat(SSEntityStat.ROTATE_SPEED,
        //                            ownDir.getMod() * ownSizeMod * MathUtils.PI2);
        if (otherLogic.isAlive())
            otherLogic.rotateCollision(otherDir, otherSizeMod);
        //            otherLogic.setStat(SSEntityStat.ROTATE_SPEED,
        //                            otherDir.getMod() * otherSizeMod * MathUtils.PI2);
    }


    protected boolean isAlive() {
        return getStat(SSEntityStat.HP) > 0f;
    }

    private void rotateCollision(SSDirection dir, float sizeMod) {
        //        SSLogger.log(movingEntity + ":rotateCollision:"
        //                        + sizeMod);
        float rad = dir.getMod() * sizeMod * MathUtils.PI;
        //        SSLogger.log("rad:" + rad * MathUtils.radDeg);
        setStat(SSEntityStat.ROTATE_SPEED, rad);
    }

    public void updateCollisionEnd(SSMovingEntity other,
                                   SSMovingLogic otherLogic, Fixture otherFix,
                                   SSFixtureData otherData, Fixture ownFix,
                                   SSFixtureData ownData) {
        if (SSCollType.WEAPON.equalsBit(otherData.getType().getId())) {
            stopWeaponDmg((SSWeaponFixData) otherData);
            return;
        } else if (SSCollType.WEAPON.equalsBit(ownData.getType().getId())) {
            otherLogic.stopWeaponDmg((SSWeaponFixData) ownData);
            return;
        }
    }

    private void getWeaponDmg(SSWeaponFixData otherData) {
        SSWeaponLogic logic = otherData.getLogic();
        logic.addTarget(movingEntity);
    }

    private void stopWeaponDmg(SSWeaponFixData otherData) {
        SSWeaponLogic logic = otherData.getLogic();
        logic.removeTarget(movingEntity);
    }

    public void updateDamage(SSWeaponLogic logic, boolean start) {
        if (start) {
            if (!damages.contains(logic)) {
                if (damages.size() == 0) movingView.showDamage(true);
                damages.add(logic);
            }
        } else {
            if (damages.contains(logic)) {
                damages.remove(logic);
                if (damages.size() == 0) movingView.showDamage(false);
            }
        }
    }

    protected void getDamaged(float dmg, SSMovingLogic logic,
                              SSWeaponLogic gunLogic) {
        if (gunLogic == null) dmg *= logic.getStat(SSEntityStat.DMG_ARMOR);
        else dmg *= gunLogic.getStat(SSEntityStat.DMG_ARMOR);

        if (getStat(SSEntityStat.HP) - dmg <= 0) {
            setStat(SSEntityStat.HP, 0);
            despawn(logic);
        } else {
            addStat(SSEntityStat.HP, -dmg);
            if (gunLogic == null) {
                movingView.showDamage(true);
                collisionDamageTime = 0f;
                collisionDamage = true;
            }
        }
        hitPointsChanged();
    }

    protected void clearDamage() {
        for (SSWeaponLogic logic : damages)
            logic.removeTarget(movingEntity, false);
        damages.clear();
    }

    protected float computeDamage(float angleDif, float velocity0,
                                  float velocity1) {
        float angleMod = angleDif / MathUtils.PI + 1f;
        return (velocity0 + velocity1) * angleMod * 10f;
    }

    protected boolean clampVelocity() {
        float maxVelocity = getVelocityMax();
        if (body.getVelocity() > maxVelocity) {
            body.clampVelocity(maxVelocity);
            return true;
        }
        return false;
    }

    public abstract void setViewScales();

    protected class SSMovingState extends SSLogicState {

        public SSMovingState() {
            super(SSEntityState.FLY);
        }

        public SSMovingState(SSEntityState key) {
            super(key);
        }

        @Override
        public void update(float delta) {
            if (!isAlive()) return;
            float radians = body.getBodyAngleRad();

            if (getStat(SSEntityStat.ROTATE_SPEED) != 0f) {
                updateRotation(radians, delta);
            }

            boolean velocityChanged = false;
            if (getStat(SSEntityStat.ACCELERATION_SPEED) > 0f) {
                updateVelocity(radians);
                velocityChanged = true;
            }

            if(clampVelocity()){
                velocityChanged = true;
            }

            if (velocityChanged) {
                velocityChanged();
            }
        }

        protected void updateRotation(float radians, float delta) {
            //            SSLogger.log("updateRotation:");
            float angle = SSUtil.modAngleRad(radians,
                    getStat(SSEntityStat.ROTATE_SPEED) * delta);
            movingEntity.setBodyAngle(angle);
            rotationChanged();
        }

        protected void updateVelocity(float radians) {
            SSVector tmpVec = SSVector.getVector();
            tmpVec.set(getStat(SSEntityStat.ACCELERATION_SPEED), 0f);
            tmpVec.setAngleRad(radians);
            body.applyVelocity(tmpVec);
            tmpVec.free();
            velocityChanged();
        }

        @Override
        public void enter() {
        }

        @Override
        public void exit() {
        }

        @Override
        public void effectFinished(SSEffect effect) {
        }

    }

    protected class SSMovingDespawnState extends SSDespawnState {

        @Override
        public void enter() {
            body.clearCollisionIds();
            view.setVisible(false);
            clearDamage();
            movingView.showDestroy();
            removeEntity();
            body.stop();
        }

        @Override
        public void effectFinished(SSEffect effect) {
            super.effectFinished(effect);
            if (effect.getType() == SSPoolType.EXPLOSION) {
                entity.remove();
                body.destroy();
            }
        }
    }


}
