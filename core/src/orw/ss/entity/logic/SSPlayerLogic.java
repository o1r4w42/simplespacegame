package orw.ss.entity.logic;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.actors.SSItem;
import orw.ss.entity.actors.SSShip;
import orw.ss.entity.view.SSEffect;
import orw.ss.entity.view.SSShipView;
import orw.ss.enums.SSDefType;
import orw.ss.enums.SSDialogDef;
import orw.ss.enums.SSEntityStat;
import orw.ss.enums.SSEntityState;
import orw.ss.enums.SSEntityType;
import orw.ss.enums.SSPoolType;
import orw.ss.enums.SSUpgradePartType;
import orw.ss.grafix.SSColor;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.physics.SSBody;
import orw.ss.screen.ui.widget.SSLabelBar;
import orw.ss.screen.ui.widget.SSLabelIcon;

public class SSPlayerLogic extends SSShipLogic {

    private int[] upgradeParts;
    private int score;

    private SSLabelBar hpLabel;
    private SSLabelBar speedLabel;
    private SSLabelBar rotationLabel;
    private SSLabelBar velocityLabel;
    private SSLabelBar shieldLabel;

    private Image directionImage;
    private SSLabelIcon scoreLabel;
    private SSLabelIcon[] partLabels;

    public SSPlayerLogic(SSShip player, SSEntityDef definition, SSBody body, SSShipView view,
                         SSInjectionComponent injector) {
        super(SSEntityType.PLAYER, player, definition, body, view, SSColor.PLAYER_BLUE, injector);
        upgradeParts = new int[SSUpgradePartType.values().length];
        for (int i = 0; i < upgradeParts.length; i++)
            upgradeParts[i] = 10;
        addState(new SSUpgradeLogicState());
    }

    @Override
    protected SSLogicState initDespawnState() {
        return new SSPlayerDespawnState();
    }

    @Override
    public void viewLoaded() {
        super.viewLoaded();
        shipView.setShipColor(SSColor.PLAYER_BLUE);
    }

    @Override
    public void reset(SSEntityDef definition) {
        super.reset(definition);
        updateScore(0);
        updateParts(null);
    }

    @Override
    protected void removeEntity() {

    }

    @Override
    public void createEquip() {
        initModule(0, SSDefType.MODULE_M_START_0);
        initModule(1, SSDefType.MODULE_M_END_0);
//        initModule(2, SSDefType.MODULE_M_START_0);
//        initModule(3, SSDefType.MODULE_S_0);
//        initModule(4, SSDefType.MODULE_S_1);

        initPart(0, 0, SSDefType.PART_DRIVE_0);
        initPart(1, 0, SSDefType.PART_THRUST_0);
//        setPart(2, 0, SSDefType.PART_THRUST_0);
//        setPart(3, 0, SSDefType.PART_THRUST_0);
//        setPart(4, 0, SSDefType.PART_THRUST_0);
    }

    @Override
    public void updateBounds(float size) {
        super.updateBounds(size);
        camera.updateBounds(size);
    }

    public void destroyed(SSMeteorLogic logic) {
        updateScore(logic.getScore());
    }

    public void destroyed(SSShipLogic logic) {
        updateScore(logic.getScore());
    }

    public int getParts(SSUpgradePartType type) {
        return upgradeParts[type.getIndex()];
    }

    public boolean removeUpgradeParts(SSUpgradePartType type, int value) {
        if (upgradeParts[type.getIndex()] >= value) {
            upgradeParts[type.getIndex()] -= value;
            return true;
        }
        return false;
    }

    private int getMaxParts() {
        int max = 0;
        for (int value : upgradeParts) {
            max = Math.max(max, value);
        }
        return max;
    }

    public void collectItem(SSItem item, int score, int[] parts) {
        updateScore(score);
        updateParts(parts);
        stop();
        if (canUpgradeSlot())
            world.showDialog(SSDialogDef.UPGRADE);
    }


    private void updateParts(int[] parts) {
        for (SSUpgradePartType type : SSUpgradePartType.values()) {
            if (parts != null)
                upgradeParts[type.getIndex()] += parts[type.getIndex()];
            else upgradeParts[type.getIndex()] = 0;
            partLabels[type.getIndex()].setText(Integer.toString(upgradeParts[type.getIndex()]));
        }
    }

    private void updateScore(int score) {
        if (score == 0)
            this.score = 0;
        else this.score += score;
        scoreLabel.setText(Integer.toString(this.score));
    }

    public boolean canUpgradePart() {
        return false;
    }


    public boolean canAddPart(int slotSize) {
        return getMaxParts() >= slotSize;
    }

    public boolean canAddPart(SSUpgradePartType type, float slotSize) {
        return getParts(type) >= slotSize;
    }

    public boolean canUpdatePart(int slotSize, int level) {
        return false;
    }

    public boolean canUpgradeSlot() {
        return false;
    }

    public void setLabels(SSLabelBar hpLabel, SSLabelBar shieldLabel, SSLabelBar speedLabel,
                          SSLabelBar velocityLabel, SSLabelBar rotationLabel,
                          Image directionImage, SSLabelIcon scoreLabel, SSLabelIcon[] partLabels) {
        this.hpLabel = hpLabel;
        this.shieldLabel = shieldLabel;
        this.speedLabel = speedLabel;
        this.velocityLabel = velocityLabel;
        this.rotationLabel = rotationLabel;
        this.directionImage = directionImage;
        this.scoreLabel = scoreLabel;
        this.partLabels = partLabels;
    }

    @Override
    protected void hitPointsChanged() {
        super.hitPointsChanged();
        hpLabel.set(getStat(SSEntityStat.HP), getStat(SSEntityStat.HP_MAX));
    }

    @Override
    protected void shieldPointsChanged() {
        super.shieldPointsChanged();
        shieldLabel.set(getStat(SSEntityStat.SP), getStat(SSEntityStat.SP_MAX));
    }

    @Override
    protected void velocityChanged() {
        super.velocityChanged();
        velocityLabel.set(getVelocity() * 100, getVelocityMax() * 100);
    }

    @Override
    protected void rotationChanged() {
        super.rotationChanged();
        directionImage.setRotation(SSUtil.modAngle(getBodyAngle(), -90f));
    }

    @Override
    protected void accelerationChanged() {
        super.accelerationChanged();
        speedLabel.set(getStat(SSEntityStat.ACCELERATION_SPEED) * 100, getAccelerationMax() * 100);
    }

    @Override
    protected void rotationSpeedChanged() {
        super.rotationSpeedChanged();
        rotationLabel.set(getStat(SSEntityStat.ROTATE_SPEED) * MathUtils.radiansToDegrees,
                getRotateMax() * MathUtils.radiansToDegrees);
    }

    @Override
    public void updateRange(float range) {

    }

//    public void upgradeFrame() {
//        entity.updateDefinition(assetManager.getDefinition(SSDefType.FRAME_M_0));
//    }

    protected class SSPlayerDespawnState extends SSDespawnState {

        @Override
        public void enter() {
            body.clearCollisionIds();
            view.setVisible(false);
            clearDamage();
            movingView.showDestroy();
        }

        @Override
        public void effectFinished(SSEffect effect) {
            super.effectFinished(effect);
            if (effect.getType() == SSPoolType.EXPLOSION) {
                //                body.destroy();
            }
        }
    }

    protected class SSUpgradeLogicState extends SSMovingState {

        private float timer;
        private float time = 2f;

        public SSUpgradeLogicState() {
            super(SSEntityState.UPGRADE);
        }

        @Override
        public boolean canExit(SSLogicState other) {
            return timer >= time;
        }

        @Override
        public void enter() {
            timer = 0;
        }

        @Override
        public void update(float delta) {
            super.update(delta);
            if (!isAlive()) return;
            //            float velocityLength = movingEntity.getVelocity();
            //            if (velocityLength > 0f) {
            //                float step = getAccellMod() * delta;
            //                if (velocityLength - step <= 0)
            //                    body.stop();
            //                else body.setVelocityLength(velocityLength - step);
            //            }
            timer += delta;
            if (timer >= time) enterState(SSEntityState.FLY);
        }

    }


    //    public void upgradeShip() {
    //        enterState(SSEntityState.UPGRADE);
    //    }


}
