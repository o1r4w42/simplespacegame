package orw.ss.entity.logic;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;

import javax.inject.Inject;

import orw.ss.SSUtil;
import orw.ss.SSVector;
import orw.ss.entity.SSEntity;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.actors.SSShip;
import orw.ss.entity.equip.SSEquipFactory;
import orw.ss.entity.equip.SSShipEquip;
import orw.ss.entity.equip.modules.SSModuleSlots;
import orw.ss.entity.equip.modules.SSShipModule;
import orw.ss.entity.equip.parts.SSShipPart;
import orw.ss.entity.equip.parts.SSWeaponLogic;
import orw.ss.entity.view.SSEffect;
import orw.ss.entity.view.SSShipView;
import orw.ss.enums.SSDefType;
import orw.ss.enums.SSDirection;
import orw.ss.enums.SSEntityProp;
import orw.ss.enums.SSEntityStat;
import orw.ss.enums.SSEntityState;
import orw.ss.enums.SSEntityType;
import orw.ss.enums.SSSlotType;
import orw.ss.grafix.SSColor;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.physics.SSBody;

public abstract class SSShipLogic extends SSMovingLogic {

    private static final Interpolation accelerationInt = Interpolation.fade;
    private static final Interpolation rotateInt = Interpolation.pow2;

    protected final SSShip ship;
    protected final SSShipView shipView;
    protected final SSShipEquip shipEquip;
    private final Color shipColor;

    @Inject
    SSEquipFactory equipFactory;

    private SSDirection rotateDir = SSDirection.LEFT;
    private boolean canDrift;
    private boolean hasFrontThrust;
    private boolean hasBackThrust;

    public SSShipLogic(SSEntityType type, SSShip ship, SSEntityDef definition, SSBody body,
                       SSShipView view, SSInjectionComponent injector) {
        this(type, ship, definition, body, view, SSColor.ENEMY_RED, injector);
    }

    public SSShipLogic(SSEntityType type, SSShip ship, SSEntityDef definition, SSBody body,
                       SSShipView view, Color shipColor, SSInjectionComponent injector) {
        super(type, ship, definition, body, view, injector);
        injector.injectShipLogic(this);
        this.ship = ship;
        this.shipColor = shipColor;
        shipView = view;

        initRotateLogic();
        initAttackLogic();
        initDriftLogic();

        shipEquip = new SSShipEquip(equipFactory);
    }

    @Override
    public void spawn(SSEntity owner) {
        super.spawn(owner);
        shipEquip.init(body);
    }

    @Override
    protected SSLogicState initDefaultState() {
        return new SSShipMovingState();
    }


    @Override
    public void setViewScales() {
        movingView.setScales(.5f, .25f);
    }

    @Override
    protected void setStats() {
        super.setStats();
        setStat(SSEntityStat.HP_MAX, getStat(SSEntityStat.HP));
        setStat(SSEntityStat.SP_MAX, getStat(SSEntityStat.SP));
    }

    @Override
    public void reset(SSEntityDef definition) {
        super.reset(definition);
        canDrift = false;
        hasFrontThrust = false;
        hasBackThrust = false;
        shipEquip.clear();
        createEquip();
        view.updateAngleRad(body.getBodyAngleRad());
    }

    @Override
    public void free() {
        super.free();
        shipEquip.clear();
    }

    @Override
    public boolean update(float delta) {
        if (!super.update(delta)) return false;
        float spReg = getStat(SSEntityStat.SP_REG);
        if (spReg != 0f) regSp(spReg * delta);
        float accelerationMod = getStat(SSEntityStat.ACCELERATION_SPEED)
                / getAccelerationMax();
        float rotateMod = Math.abs(getStat(SSEntityStat.ROTATE_SPEED)
                / getRotateMax());
        float angle = body.getBodyAngleRad();
        float x = entity.getCX();
        float y = entity.getCY();

        shipEquip.update(delta, x, y, angle, accelerationMod, rotateMod, rotateDir, getState());
        return true;
    }


    @Override
    protected void getDamaged(float dmg, SSMovingLogic logic,
                              SSWeaponLogic gunLogic) {
        if (getStat(SSEntityStat.SP) > 0f) {
            if (gunLogic == null) dmg *= logic.getStat(SSEntityStat.DMG_SHIELD);
            else dmg *= gunLogic.getStat(SSEntityStat.DMG_SHIELD);
            addStat(SSEntityStat.SP, -dmg);
            float sp = getStat(SSEntityStat.SP);
            if (sp < 0f) {
                shipView.showShieldDamage(false);
                dmg = -sp / logic.getStat(SSEntityStat.DMG_SHIELD);
                setStat(SSEntityStat.SP, 0);
                super.getDamaged(dmg, logic, gunLogic);
            } else {
                shipView.showShieldDamage(true);
                super.getDamaged(0f, logic, gunLogic);
            }
            shieldPointsChanged();
        } else {
            shipView.showShieldDamage(false);
            super.getDamaged(dmg, logic, gunLogic);
        }
    }

    @Override
    public void renderDebug(ShapeRenderer renderer, float x, float y) {
        super.renderDebug(renderer, x, y);
        shipEquip.renderDebug(renderer, x, y, body.getBodyAngleRad());
    }


    @Override
    protected void removeEntity() {
        world.remove(ship);
    }

    @Override
    protected void despawn(SSEntityLogic other) {
        super.despawn(other);
        if (other.getType() == SSEntityType.PLAYER)
            ((SSPlayerLogic) other).destroyed(this);
    }

    private void regSp(float step) {
        float shieldMax = getStat(SSEntityStat.SP_MAX);
        float shield = getStat(SSEntityStat.SP);
        if (shieldMax != shield) {
            if (shield + step <= shieldMax) addStat(SSEntityStat.SP, step);
            else setStat(SSEntityStat.SP, shieldMax);
            shieldPointsChanged();
        }
    }

    public void installModule(int index, SSEntityDef def) {
        shipEquip.setModule(index, def, movingEntity, body, this, shipView, 0f, 0f, shipColor, false);
        allStatsChanged();
    }

    protected void initModule(int index, SSDefType type) {
        shipEquip.setModule(index, shipEquip.getDef(type), movingEntity, body, this, shipView,
                0f, 0f, shipColor, true);
        allStatsChanged();
    }

    public void installPart(int moduleIndex, int slotIndex, SSEntityDef def) {
        shipEquip.setPart(moduleIndex, slotIndex, def, 0f, shipColor, movingEntity,
                body, this, shipView);
        allStatsChanged();
    }

    protected void initPart(int moduleIndex, int slotIndex, SSDefType type) {
        shipEquip.setPart(moduleIndex, slotIndex, shipEquip.getDef(type), 0f, shipColor,
                movingEntity, body, this, shipView);
        allStatsChanged();
    }

    public void updateSlotStats(SSShipPart part, SSSlotType slotType) {
        switch (part.getType()) {
            case ARMOR:
                break;
            case DRIVE:
                break;
            case GUN:
                break;
            case SHIELD:
                break;
            case THRUST:
                if (slotType == SSSlotType.FRONT) hasFrontThrust = true;
                else if (slotType == SSSlotType.BACK) hasBackThrust = true;
                break;
            case LASER:
                break;
        }
        canDrift = hasFrontThrust && hasBackThrust;
    }

    public SSDirection getRotateDir() {
        return rotateDir;
    }

    protected void initRotateLogic() {
        addState(new SSRotateState());
    }


    protected void initAttackLogic() {
        addState(new SSAttackState());
    }

    protected void initDriftLogic() {
        addState(new SSDriftState());
    }


    public void setDirection(SSDirection dir) {
        rotateDir = dir;
    }

    public boolean canDrift() {
        return canDrift;
    }

    public int getScore() {
        return 0;
    }

    protected float getRotateMod() {
        return (float) (getStat(SSEntityStat.ROTATE_MOD) / Math.sqrt(getStat(SSEntityStat.MASS)));
    }

    public float getRotateMax() {
        return (float) (getStat(SSEntityStat.ROTATE_MAX) / Math.sqrt(getStat(SSEntityStat.MASS)));
    }

    protected float getAccelerationMod() {
        return (float) (getStat(SSEntityStat.ACCELERATION_MOD) / Math.sqrt(getStat(SSEntityStat.MASS)));
    }

    public float getAccelerationMax() {
        return (float) (getStat(SSEntityStat.ACCELERATION_MAX) / Math.sqrt(getStat(SSEntityStat.MASS)));
    }


    protected void stop() {
        setStat(SSEntityStat.ACCELERATION_SPEED, 0f);
        setStat(SSEntityStat.ACCELERATION_STEP, 0f);
        setStat(SSEntityStat.ROTATE_SPEED, 0f);
        setStat(SSEntityStat.ROTATE_STEP, 0f);
        allStatsChanged();
    }

    public SSShipModule getModule(int index) {
        return shipEquip.getModule(index);
    }

    public SSModuleSlots getModuleSlots() {
        return shipEquip.getModuleSlots();
    }

    public SSShipPart getModulePart(int moduleIndex, int slotIndex) {
        return shipEquip.getModulePart(moduleIndex, slotIndex);
    }

    protected void accelerationChanged() {

    }

    protected void rotationSpeedChanged() {

    }

    protected void shieldPointsChanged() {

    }

    @Override
    protected void allStatsChanged() {
        super.allStatsChanged();
        shieldPointsChanged();
        accelerationChanged();
        rotationSpeedChanged();
    }

    public abstract void createEquip();

    public abstract void updateRange(float range);


    protected class SSShipMovingState extends SSMovingState {

        public SSShipMovingState() {
            super();
        }

        public SSShipMovingState(SSEntityState key) {
            super(key);
        }

        @Override
        public void update(float delta) {
            float rotateMax = getRotateMax();
            float rotateSpeed = getStat(SSEntityStat.ROTATE_SPEED);
            modRotation(delta, rotateSpeed, rotateMax);

            float accelerationMax = getAccelerationMax();
            float accelerationSpeed = getStat(SSEntityStat.ACCELERATION_SPEED);
            modAcceleration(delta, accelerationSpeed, accelerationMax);

            super.update(delta);
        }

        protected void modRotation(float delta, float rotateSpeed,
                                   float rotateMax) {
            if (rotateSpeed == 0f) return;
            float step = delta * getRotateMod();
            float rotateStep = getStat(SSEntityStat.ROTATE_STEP);
            if (Math.abs(rotateSpeed) - step <= 0f) rotateStep = 0f;
            else {
                if (rotateStep > 0f) rotateStep -= step;
                else rotateStep += step;
            }
            applyRotation(rotateStep, rotateMax);
        }

        protected void applyRotation(float rotateStep, float rotateMax) {
            setStat(SSEntityStat.ROTATE_STEP, rotateStep);
            float rotateSpeed = rotateInt.apply(0f, rotateMax,
                    Math.abs(rotateStep));
            if (rotateStep < 0f) rotateSpeed *= -1f;
            setStat(SSEntityStat.ROTATE_SPEED, rotateSpeed);
            rotationSpeedChanged();
        }

        private void modAcceleration(float delta, float accelerationSpeed,
                                     float accelerationMax) {
            if (accelerationSpeed == accelerationMax) return;
            float step = getAccelerationMod() * delta;
            float accelerationStep = getStat(SSEntityStat.ACCELERATION_STEP);
            accelerationStep += step;
            if (accelerationStep > 1f) accelerationStep = 1f;
            applyAcceleration(accelerationStep, accelerationMax);
            accelerationChanged();
        }

        protected void applyAcceleration(float accelerationStep, float accelerationMax) {
            setStat(SSEntityStat.ACCELERATION_STEP, accelerationStep);
            float accelerationSpeed = accelerationInt.apply(0f, accelerationMax, accelerationStep);
            setStat(SSEntityStat.ACCELERATION_SPEED, accelerationSpeed);
        }
    }

    protected class SSRotateState extends SSShipMovingState {


        public SSRotateState() {
            super(SSEntityState.ROTATE);
        }

        public SSRotateState(SSEntityState key) {
            super(key);
        }

        @Override
        protected void modRotation(float delta, float rotateSpeed,
                                   float rotateMax) {
            if (rotateSpeed * rotateDir.getMod() == rotateMax) return;
            float step = delta * getRotateMod();
            float rotateStep = getStat(SSEntityStat.ROTATE_STEP);
            rotateStep += step * rotateDir.getMod();
            if (rotateStep > 1f) rotateStep = 1f;
            else if (rotateStep < -1f) rotateStep = -1f;
            applyRotation(rotateStep, rotateMax);
        }

    }

    protected class SSAttackState extends SSShipMovingState {

        public SSAttackState() {
            super(SSEntityState.ATTACK);
        }

    }

    private class SSDriftState extends SSLogicState {

        public SSDriftState() {
            super(SSEntityState.DRIFT);
        }

        @Override
        public void enter() {
            //            SSLogger.log("enter drift");
            stop();
        }

        @Override
        public void update(float delta) {
            SSVector tmpVec = SSVector.getVector();
            float radians = body.getBodyAngleRad();
            if (rotateDir == SSDirection.LEFT)
                radians = SSUtil.modAngleRad(radians, MathUtils.PI / 2f);
            else radians = SSUtil.modAngleRad(radians, -MathUtils.PI / 2f);
            tmpVec.set(getRotateMod() * 2f, 0);
            tmpVec.setAngleRad(radians);

            body.applyVelocity(tmpVec);
            tmpVec.free();

            clampVelocity();
            velocityChanged();
        }

        @Override
        public void exit() {

        }

        @Override
        public void effectFinished(SSEffect effect) {

        }
    }

}
