package orw.ss.entity.logic;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.BodyDef;

import orw.ss.SSUtil;
import orw.ss.SSVector;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.actors.SSItem;
import orw.ss.entity.actors.SSShip;
import orw.ss.entity.view.SSEffect;
import orw.ss.entity.view.SSEffectView;
import orw.ss.entity.view.SSEntityView;
import orw.ss.enums.SSEntityProp;
import orw.ss.enums.SSEntityState;
import orw.ss.enums.SSEntityType;
import orw.ss.enums.SSUpgradePartType;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.physics.SSBody;

public class SSItemLogic extends SSEntityLogic  {

    private SSItem item;
    private boolean collected;
    private SSShip target;
    private int[] parts;
    private int score;

    public SSItemLogic(SSItem item, SSEntityDef definition, SSBody body,
                       SSEntityView view, SSShip target, SSInjectionComponent injector) {
        super(SSEntityType.ITEM, item, definition, body, view, BodyDef.BodyType.StaticBody, injector);
        this.item = item;
        this.target = target;
        parts = new int[SSUpgradePartType.values().length];
        SSUpgradePartType partType = definition.get(SSEntityProp.PART,
                SSUpgradePartType.class,
                null);
        parts[partType.getIndex()] = 1;
        score = 10;
    }

    @Override
    protected SSLogicState initDefaultState() {
        return new SSItemState();
    }

    @Override
    public boolean update(float delta) {
        if (isOutOfRange()) respawn();
        else super.update(delta);
        return true;
    }

    @Override
    protected void removeEntity() {
        world.remove(item);
    }

    @Override
    public void free() {

    }

    private void respawn() {
        //        SSLogger.log("respawn");
        float tva = target.getVelAngleRad();
        float dist = camera.getSpawnRange(entity.getSize());
        SSVector tmpVec = SSVector.getVector(dist, 0f);
        float mod = MathUtils.PI / 4f - MathUtils.random(MathUtils.PI / 2f);
        float angle = SSUtil.modAngleRad(tva, mod);
        tmpVec.setAngleRad(angle);
        tmpVec.add(camera.position.x, camera.position.y);
        entity.setBodyPosition(tmpVec);
        tmpVec.free();
    }

    public void getCollected(SSPlayerLogic logic) {
        if (!collected) {
            despawn(null);
            logic.collectItem(item, score, parts);
            collected = true;
        }
    }

    protected class SSItemState extends SSLogicState {

        public SSItemState() {
            super(SSEntityState.IDLE);
        }

        @Override
        public void update(float delta) {
        }

        @Override
        public void enter() {
        }

        @Override
        public void exit() {
        }

        @Override
        public void effectFinished(SSEffect effect) {
        }

    }

}
