package orw.ss.entity.logic;

import com.badlogic.gdx.physics.box2d.BodyDef;

import orw.ss.SSUtil;
import orw.ss.SSVector;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.actors.SSBgObject;
import orw.ss.entity.actors.SSShip;
import orw.ss.entity.view.SSEffect;
import orw.ss.entity.view.SSEntityView;
import orw.ss.enums.SSEntityState;
import orw.ss.enums.SSEntityType;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.physics.SSBody;

public class SSBgObjectLogic extends SSEntityLogic {

    private SSShip target;
    private SSVector offset;
    private SSVector lastPos;
    private float step = SSUtil.SSIntegers.SCREEN_HEIGHT * SSUtil.SSFloats.WORLD_TO_BODY;
    private SSBgObject object;

    public SSBgObjectLogic(SSBgObject object, SSEntityDef definition, SSBody body, SSEntityView view,
                           SSShip target, SSInjectionComponent injector) {
        super(SSEntityType.OBJECT, object, definition, body, view, BodyDef.BodyType.StaticBody, injector);
        this.object = object;
        this.target = target;
        offset = SSVector.getVector();
        lastPos = SSVector.getVector();
    }

    @Override
    protected SSLogicState initDefaultState() {
        return new SSObjectState();
    }

    @Override
    protected void removeEntity() {
        world.remove(object);
    }

    @Override
    public void free() {
        offset.free();
        lastPos.free();
    }

    public void initPosition(float x, float y) {
        offset.set(x, y);
        lastPos.set(x, y);
    }

    private class SSObjectState extends SSLogicState {

        public SSObjectState() {
            super((SSEntityState.IDLE));
        }

        @Override
        public void update(float delta) {
            float x = ((int) (target.getCX() / step)) * step + offset.getX();
            float y = ((int) (target.getCY() / step)) * step + offset.getY();
            if (lastPos.getX() != x || lastPos.getY() != y) {
                lastPos.set(x, y);
                entity.setBodyPosition(x, y);
            }
        }

        @Override
        public void enter() {
        }

        @Override
        public void exit() {
        }

        @Override
        public void effectFinished(SSEffect effect) {
        }
    }
}
