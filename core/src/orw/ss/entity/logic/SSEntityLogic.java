package orw.ss.entity.logic;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import java.util.HashMap;

import javax.inject.Inject;

import orw.ORWLogger;
import orw.ss.assets.SSAssetManager;
import orw.ss.entity.SSEntity;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.SSStats;
import orw.ss.entity.view.SSEffect;
import orw.ss.entity.view.SSEffectView;
import orw.ss.entity.view.SSEntityView;
import orw.ss.enums.SSEntityState;
import orw.ss.enums.SSEntityType;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.physics.SSBody;
import orw.ss.screen.SSCamera;
import orw.ss.entity.SSWorld;

public abstract class SSEntityLogic extends SSStats implements SSEffectView.SSEffectListener {

    private final SSEntityType type;
    protected final SSEntity entity;
    protected SSEntityDef definition;
    protected final SSBody body;
    protected final SSEntityView view;
    private final HashMap<SSEntityState, SSLogicState> states;
    private final SSLogicState defaultState;
    private final SSEntityState defaultStateType;
    private final BodyType bodyType;

    @Inject
    SSAssetManager assetManager;
    @Inject
    SSWorld world;
    @Inject
    SSCamera camera;

    private boolean active;
    private boolean forceState;
    private SSLogicState currentState;
    private SSLogicState nextState;

    public SSEntityLogic(SSEntityType type, SSEntity entity, SSEntityDef definition, SSBody body,
                         SSEntityView view, BodyType bodyType, SSInjectionComponent injector) {
        super(definition);
        injector.injectEntityLogic(this);
        this.type = type;
        this.entity = entity;
        this.definition = definition;
        this.body = body;
        this.view = view;
        this.bodyType = bodyType;


        states = new HashMap<>();
        defaultState = initDefaultState();
        addState(defaultState);
        addState(initDespawnState());
        defaultStateType = defaultState.getKey();
        currentState = defaultState;
        nextState = defaultState;
        currentState.enter();
    }


    @Override
    public String toString() {
        return entity.toString();
    }

    @Override
    public void effectFinished(SSEffect effect) {
        currentState.effectFinished(effect);
    }

    public void renderDebug(ShapeRenderer shapes, float x, float y) {

    }

    public SSEntityType getType() {
        return type;
    }


    public void spawn(SSEntity owner) {
        active = true;
        body.createBody(owner);
    }

    public boolean update(float delta) {
        if (nextState != currentState &&
                (forceState || currentState.canExit(nextState) && nextState.canEnter(currentState))) {
            currentState.exit();
            currentState = nextState;
            currentState.enter();
        }
        currentState.update(delta);
        return isActive();
    }

    public void updatePosition(float x, float y) {

    }

    public boolean isState(SSEntityState state) {
        return currentState.getKey() == state;
    }

    public boolean isActive() {
        return active;
    }

//    public boolean isAlive() {
//    return currentState.key != SSEntityState.DESPAWN;
//}

    protected void despawn(SSEntityLogic other) {
        enterState(SSEntityState.DESPAWN, true);
    }

    public void reset(SSEntityDef definition) {
        setStats(definition);
        enterDefaultState();
    }

    protected void enterDefaultState() {
        enterState(defaultStateType, true);
    }

    protected void addState(SSLogicState state) {
        //        SSLogger.log(entity + ":addState:" + state.getKey());
        if (states.containsKey(state.getKey()))
            ORWLogger.log(entity + ":addState:" + state.getKey() + " DOUBLE!");
        states.put(state.getKey(), state);
    }

    public void enterState(SSEntityState key) {
        enterState(key, false);
    }

    private void enterState(SSEntityState key, boolean force) {
        nextState = getState(key);
        forceState = force;
    }

    public SSEntityState getState() {
        return currentState.getKey();
    }

    protected SSLogicState getState(SSEntityState key) {
        if (states.containsKey(key)) return states.get(key);
        return defaultState;
    }

    public boolean isOutOfRange() {
        return camera.isOutOfRange(entity);
    }

    protected SSLogicState initDespawnState() {
        return new SSDespawnState();
    }

    public void destroy() {
//        body.destroy(world);
        entity.remove();
    }


    public void viewLoaded() {
    }

    public void updateBounds(float size) {
    }

    protected abstract SSLogicState initDefaultState();

    protected abstract void removeEntity();

    public abstract void free();

    public BodyType getBodyType() {
        return bodyType;
    }

//    public void updateDefinition(SSEntityDef definition) {
//        this.definition = definition;
//    }

    protected abstract class SSLogicState {

        private SSEntityState key;

        public SSLogicState(SSEntityState key) {
            this.key = key;
        }

        public boolean canEnter(SSLogicState other) {
            return true;
        }

        public boolean canExit(SSLogicState other) {
            return true;
        }

        public SSEntityState getKey() {
            return key;
        }

        public abstract void enter();

        public abstract void update(float delta);

        public abstract void exit();

        public abstract void effectFinished(SSEffect effect);
    }

    protected class SSDespawnState extends SSLogicState {

        public SSDespawnState() {
            super(SSEntityState.DESPAWN);

        }

        @Override
        public void enter() {
            //            entity.despawn(true, true);
            ORWLogger.log("enter despawn state:" + entity);
            entity.remove();
            //            if (view instanceof SSEffectView)
            //                ((SSEffectView) view).removeEffectView();
            removeEntity();
            body.destroy();
        }

        @Override
        public void update(float delta) {

        }

        @Override
        public void exit() {
        }

        @Override
        public void effectFinished(SSEffect effect) {
        }

    }


}
