package orw.ss.entity.logic;

import com.badlogic.gdx.math.MathUtils;

import orw.ss.SSUtil;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.actors.SSShip;
import orw.ss.entity.view.SSShipView;
import orw.ss.enums.SSDirection;
import orw.ss.enums.SSEntityState;
import orw.ss.enums.SSEntityType;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.physics.SSBody;

public class SSEnemyLogic extends SSShipLogic {

    protected SSShip target;
    private float rotateView = MathUtils.PI / 32f;
    private float range;

    public SSEnemyLogic(SSShip ship, SSEntityDef definition,
                        SSBody body, SSShipView view, SSShip target, SSInjectionComponent injector) {
        super(SSEntityType.SHIP, ship, definition, body, view, injector);
        this.target = target;
    }

    @Override
    protected SSLogicState initDefaultState() {
        return new SSEnemyMovingState();
    }

    @Override
    protected void initRotateLogic() {
        addState(new SSEnemyRotateState());
    }

    @Override
    protected void initAttackLogic() {
        addState(new SSEnemyAttackState());
    }

    @Override
    public void createEquip() {
        //        installPart(SSShipPartType.DRIVE, 0, 0f);
        //        installPart(SSShipPartType.THRUST, 1, 0f);
        //        installPart(SSShipPartType.GUN, 2, 0f);
    }

    @Override
    public void updateRange(float range) {
        this.range = Math.max(this.range, range);
    }

    private boolean inView() {
        return SSUtil.getAngleDiffRad(ship.getAngleRad(target),
                        ship.getBodyAngleRad()) < rotateView;
    }

    private boolean inRange() {
        return target.getDistance(entity) < range;
    }

    private class SSEnemyMovingState extends SSShipMovingState {

        public SSEnemyMovingState() {
            super();
        }

        @Override
        public void update(float delta) {
            if (!inView()) ship.enterState(SSEntityState.ROTATE);
            else if (inRange()) ship.enterState(SSEntityState.ATTACK);
            else super.update(delta);
        }
    }

    private class SSEnemyRotateState extends SSRotateState {

        @Override
        public void update(float delta) {
            SSDirection dir = SSUtil.getAngleDir(ship.getAngle(target),
                            ship.getBodyAngle());
            setDirection(dir);
            super.update(delta);
            if (inView()) {
                if (inRange()) ship.enterState(SSEntityState.ATTACK);
                else ship.enterState(SSEntityState.FLY);
            }
        }

    }

    private class SSEnemyAttackState extends SSAttackState {

        @Override
        public void update(float delta) {
            if (!inRange()) {
                if (inView()) ship.enterState(SSEntityState.FLY);
                else ship.enterState(SSEntityState.ROTATE);
            } else super.update(delta);
        }

    }

}
