package orw.ss.entity.view;

import orw.ss.injection.SSInjectionComponent;

public abstract class SSScalingView extends SSEffectView {

    private float[] scaleBounds;
    private float scale = 1f;
    private float scaleStep;
    private boolean scaleUp;

    public SSScalingView(SSInjectionComponent injector, float min, float max, float step) {
        super(injector);
        scaleBounds = new float[2];
        scaleBounds[0] = min;
        scaleBounds[1] = max;
        scaleStep = step;

    }

    @Override
    public void update(float delta) {
        super.update(delta);
        if (scaleUp) {
            scale += scaleStep * delta;
            if (scale >= scaleBounds[1]) {
                scale = scaleBounds[1];
                scaleUp = false;
            }
        } else {
            scale -= scaleStep * delta;
            if (scale <= scaleBounds[0]) {
                scale = scaleBounds[0];
                scaleUp = true;
            }
        }
        setScale(scale);
    }


}
