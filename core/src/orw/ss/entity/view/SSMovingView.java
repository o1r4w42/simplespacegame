package orw.ss.entity.view;

import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter.GradientColorValue;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import javax.inject.Inject;

import orw.ss.SSUtil;
import orw.ss.assets.SSAssetManager;
import orw.ss.enums.SSPoolType;
import orw.ss.injection.SSInjectionComponent;

public abstract class SSMovingView extends SSEffectView {

    private SSEffect dmgEffect;
    private ParticleEmitter dmgEmitter;
    private SSEffect destroyEffect;
    private ParticleEmitter destroyEmitter;
    private int[] dmgCountValues;
    private float dmgScale = .5f;
    private float destroyScale = .25f;

    @Inject
    SSAssetManager assetManager;

    public SSMovingView(SSInjectionComponent injectionComponent) {
        super(injectionComponent);
        injectionComponent.injectMovingView(this);
        dmgCountValues = new int[2];
    }

    @Override
    public void initEffects(TextureAtlas atlas) {
        initDestroyEffect();
        initDamageEffect();
    }

    @Override
    public void addEffects() {
        addEffect(dmgEffect);
    }

    private void initDestroyEffect() {
        destroyEffect = assetManager.getEffect(SSPoolType.EXPLOSION, true);
        destroyEmitter = destroyEffect.findEmitter(SSUtil.SSStrings.DEF_EMITTER);

        GradientColorValue tint = destroyEmitter.getTint();
        initDestroyColor(tint.getColors());

        destroyEffect.setScale(destroyScale);
    }

    private void initDamageEffect() {
        dmgEffect = assetManager.getEffect(SSPoolType.DAMAGE, true);
        dmgEmitter = dmgEffect.findEmitter(SSUtil.SSStrings.DEF_EMITTER);

        GradientColorValue tint = dmgEmitter.getTint();
        initDmgColor(tint.getColors());

        dmgCountValues[0] = dmgEmitter.getMinParticleCount();
        dmgCountValues[1] = dmgEmitter.getMaxParticleCount();

        dmgEmitter.setMinParticleCount(0);
        dmgEmitter.setMaxParticleCount(0);

        dmgEffect.setScale(dmgScale);
    }

    @Override
    public void reset(float x, float y, float angle) {
        super.reset(x, y, angle);
        addEffect(dmgEffect);
    }

    protected void initDestroyColor(float[] colors) {
        colors[0] = 127f / 255f;
        colors[1] = 51f / 255f;
        colors[2] = 0f;
        colors[3] = 1f;
        colors[4] = 178f / 255f;
        colors[5] = 127f / 255f;
    }

    protected void initDmgColor(float[] colors) {
        colors[0] = 127f / 255f;
        colors[1] = 51f / 255f;
        colors[2] = 0f;
        colors[3] = 127 / 255f;
        colors[4] = 51f / 255f;
        colors[5] = 0f;
    }

    @Override
    public void updatePosition(float x, float y) {
        super.updatePosition(x, y);
        dmgEffect.setPosition(x, y);
        destroyEffect.setPosition(x, y);
    }

    public void showDamage(boolean show) {
        if (show) {
            dmgEmitter.setMinParticleCount(dmgCountValues[0]);
            dmgEmitter.setMaxParticleCount(dmgCountValues[1]);
        } else {
            dmgEmitter.setMinParticleCount(0);
            dmgEmitter.setMaxParticleCount(0);
        }
    }

    public void setScales(float dmgScale, float destroyScale) {
        this.dmgScale = dmgScale;
        this.destroyScale = destroyScale;
    }

    public void showDestroy() {
        removeEffect(dmgEffect);
        removeEffect(destroyEffect);
        addEffect(destroyEffect);
    }
}
