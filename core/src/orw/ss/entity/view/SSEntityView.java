package orw.ss.entity.view;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import javax.inject.Inject;

import orw.ss.SSUtil;
import orw.ss.assets.SSAssetManager;
import orw.ss.entity.SSEntityDef;
import orw.ss.enums.SSEntityProp;
import orw.ss.injection.SSInjectionComponent;

public abstract class SSEntityView {

    private SSSprite sprite;
    protected String name;
    protected int index;
    private String source;
    private Color color;

    @Inject
    SSAssetManager assetManager;

    public SSEntityView(SSInjectionComponent injector) {
        injector.injectEntityView(this);
    }

    public void initSprite(TextureAtlas atlas, SSEntityDef definition) {
        name = definition.get(SSEntityProp.VIEW_SRC, String.class,
                        SSUtil.SSStrings.EMPTY)
                        + definition.get(SSEntityProp.VIEW_NAME,
                                        String.class,
                                        SSUtil.SSStrings.EMPTY);
        index = definition.get(SSEntityProp.VIEW_IND, int.class, 0);
        source = name + SSUtil.SSStrings.UNDER_SCORE + index;

        sprite = SSSprite.create(atlas, name, index);
        color = new Color(Color.WHITE);
        sprite.setColor(color);
    }

    public void updateSpriteSource(SSEntityDef definition) {
        name = definition.get(SSEntityProp.VIEW_SRC, String.class,
                SSUtil.SSStrings.EMPTY)
                + definition.get(SSEntityProp.VIEW_NAME,
                String.class,
                SSUtil.SSStrings.EMPTY);
        index = definition.get(SSEntityProp.VIEW_IND, int.class, 0);
        source = name + SSUtil.SSStrings.UNDER_SCORE + index;
    }

    public void spawn() {
        setVisible(true);
    }

    public void render(Batch batch) {
        renderSprite(batch);
    }

    protected void renderSprite(Batch batch) {
        sprite.render(batch);
    }

    public void updatePosition(float x, float y) {
        sprite.setPosition(x, y);
    }

    public void updateAngleRad(float rad) {
        sprite.setAngleRad(rad);
    }

    public float getHeight() {
        return sprite.getHeight();
    }

    public float getWidth() {
        return sprite.getWidth();
    }

    public String getSpriteSource() {
        return source;
    }

    public void setVisible(boolean visible) {
        sprite.setVisible(visible);
    }

    public void setAlpha(float a) {
        color.set(color.r, color.g, color.b, a);
        sprite.setColor(color);
    }

    public void setColor(Color color) {
        this.color.set(color);
        sprite.setColor(this.color);
    }

    public void setScale(float scale) {
        sprite.setScale(scale);
    }

    public void reset(float x, float y, float angle) {
        setVisible(true);
        updatePosition(x, y);
        updateAngleRad(angle);
    }

    public abstract void update(float delta);


}
