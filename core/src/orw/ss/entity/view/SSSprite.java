package orw.ss.entity.view;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;

import orw.ORWLogger;
import orw.ss.SSUtil;

public class SSSprite {

    private Sprite sprite;
    private float xOffset;
    private float yOffset;
    private boolean visible;

    public static SSSprite create(TextureAtlas atlas, String name, int index) {
        return create(atlas, name, index, Color.WHITE);
    }

    public static SSSprite create(TextureAtlas atlas, String name, int index,
                    Color color) {
        Sprite s;
        if (index < 0) s = atlas.createSprite(name);
        else s = atlas.createSprite(name, index);
        if (s != null) return new SSSprite(s, color);
        return null;
    }

    private SSSprite(Sprite sprite, Color color) {
        this.sprite = sprite;
        sprite.setScale(SSUtil.SSFloats.WORLD_TO_BODY);
        xOffset = sprite.getWidth() / 2f;
        yOffset = sprite.getHeight() / 2f;
        sprite.setCenter(xOffset, yOffset);
        sprite.setColor(color);
        visible = true;
    }

    public void render(Batch batch) {
        if (visible) sprite.draw(batch);
    }

    public void setFlip(boolean x, boolean y) {
        sprite.setFlip(x, y);
    }

    public void setPosition(float x, float y) {
        sprite.setPosition(x - xOffset, y - yOffset);
    }

    public void setAngleRad(float angle) {
        setAngle(angle * MathUtils.radiansToDegrees);
    }

    public void setAngle(float angle) {
        sprite.setRotation(angle);
    }

    public float getWidth() {
        return sprite.getWidth() * SSUtil.SSFloats.WORLD_TO_BODY;
    }

    public float getWidthS() {
        return getWidth() * sprite.getScaleX();
    }

    public float getHeight() {
        return sprite.getHeight() * SSUtil.SSFloats.WORLD_TO_BODY;
    }

    public float getHeightS() {
        return getHeight() * sprite.getScaleY();
    }


    public void setColor(Color color) {
        sprite.setColor(color);
    }


    public void setScaleX(float scale) {
        sprite.setScale(scale * SSUtil.SSFloats.WORLD_TO_BODY, sprite.getScaleY());
    }

    public void setScaleY(float scale) {
        sprite.setScale(sprite.getScaleX(), scale * SSUtil.SSFloats.WORLD_TO_BODY);
    }

    public void setScale(float scale) {
        sprite.setScale(scale * SSUtil.SSFloats.WORLD_TO_BODY);
    }

    public void setScale(float scaleX, float scaleY) {
        sprite.setScale(scaleX * SSUtil.SSFloats.WORLD_TO_BODY,
                        scaleY * SSUtil.SSFloats.WORLD_TO_BODY);

    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void setBounds() {
        ORWLogger.log("width:" + sprite.getBoundingRectangle().width);
        ORWLogger.log("height:" + sprite.getBoundingRectangle().height);
        ORWLogger.log("scale:" + sprite.getScaleX());
        //        sprite.setCenter(0f, 0f);
        //        sprite.setScale(1f);
        //        //        sprite.setBounds(20f, 0f, 40f, 80f);
        //        setScale(1f);
        //        sprite.setCenter(xOffset, yOffset);
    }

}
