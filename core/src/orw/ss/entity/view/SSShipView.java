package orw.ss.entity.view;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter.GradientColorValue;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import java.util.ArrayList;
import java.util.List;

import orw.ss.SSUtil;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.equip.modules.SSShipModule;
import orw.ss.entity.equip.parts.SSShipPart;
import orw.ss.enums.SSPoolType;
import orw.ss.injection.SSInjectionComponent;

public class SSShipView extends SSMovingView {

    private SSEffect shieldEffect;
    private ParticleEmitter shieldEmitter;
    private List<SSShipPart> parts;
    private List<SSShipModule> modules;
    private boolean shieldDmg;
    private int[] shieldCountValues;
    private float shieldScale = 1f;

    public SSShipView(SSInjectionComponent injectionComponent) {
        super(injectionComponent);
        shieldCountValues = new int[2];
        parts = new ArrayList<>();
        modules = new ArrayList<>();
    }

    @Override
    public void initSprite(TextureAtlas atlas, SSEntityDef definition) {
        super.initSprite(atlas, definition);
        //        SSLogger.log("source:" + getSpriteSource());
        //        atlas.
        //        colorSprite = new SSSprite(
        //                        atlas.createSprite(name + Strings.ENDING_COLOR, index));
        //        colorSprite = SSSprite.create(atlas, name + Strings.ENDING_COLOR, index,
        //                        SSColor.ENEMY_RED);
        //        colorSprite.setColor(Color.RED);
        //        SSLogger.log("colorSprite:" + colorSprite);
        setAlpha(0f);

    }

    //    @Override
    //    public void render(Batch batch) {
    //        super.render(batch);
    //        //        colorSprite.render(batch);
    //        for (SSShipPart part : parts)
    //            part.render(batch);
    //    }

    @Override
    protected void renderSprite(Batch batch) {
        super.renderSprite(batch);
        //        if (colorSprite != null) colorSprite.render(batch);
        for (SSShipModule module : modules)
            module.render(batch);
        for (SSShipPart part : parts)
            part.render(batch);
    }

    @Override
    public void updatePosition(float x, float y) {
        super.updatePosition(x, y);
        //        if (colorSprite != null) colorSprite.setPosition(x, y);
        shieldEffect.setPosition(x, y);
    }

    @Override
    public void updateAngleRad(float rad) {
        super.updateAngleRad(rad);
        //        if (colorSprite != null) colorSprite.setAngleRad(rad);
    }

    @Override
    public void initEffects(TextureAtlas atlas) {
        super.initEffects(atlas);
        initShieldEffect();
    }

    @Override
    public void addEffects() {
        super.addEffects();
        //        SSLogger.log("addEffects");
        addEffect(shieldEffect);
    }

    @Override
    public void showDestroy() {
        super.showDestroy();
        shieldEmitter.setMinParticleCount(0);
        shieldEmitter.setMaxParticleCount(0);
        for (SSShipPart part : parts)
            part.removeEffects();
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        for (SSShipModule module : modules)
            module.setVisible(visible);
        for (SSShipPart part : parts)
            part.setVisible(visible);
    }


    private void initShieldEffect() {
        shieldEffect = assetManager.getEffect(SSPoolType.SHIELD, true);
        shieldEffect.setScale(shieldScale);
        shieldEmitter = shieldEffect.findEmitter(SSUtil.SSStrings.DEF_EMITTER);

        GradientColorValue tint = shieldEmitter.getTint();
        initShieldColor(tint.getColors());

        shieldCountValues[0] = shieldEmitter.getMinParticleCount();
        shieldCountValues[1] = shieldEmitter.getMaxParticleCount();
        shieldEmitter.setMinParticleCount(0);
        shieldEmitter.setMaxParticleCount(0);
    }

    private void initShieldColor(float[] colors) {
        colors[0] = 127f / 255f;
        colors[1] = 1f;
        colors[2] = 1f;
        colors[3] = 0f;
        colors[4] = 148f / 255f;
        colors[5] = 1f;
    }

    //    public void setActives(List<SSShipPart> parts,
    //                    List<SSShipModule> modules) {
    //        this.parts = parts;
    //        this.modules = modules;
    //    }

    public void showShieldDamage(boolean show) {
        if (shieldDmg != show) {
            //            SSLogger.log("showShieldDamage:" + show);
            shieldDmg = show;
            showDamage(true);
        }
    }

    @Override
    public void showDamage(boolean show) {
        //        SSLogger.log("showDamage:" + show + ", " + shieldDmg);
        if (shieldDmg) {
            super.showDamage(false);
            if (show) {
                shieldEmitter.setMinParticleCount(shieldCountValues[0]);
                shieldEmitter.setMaxParticleCount(shieldCountValues[1]);
            } else {
                shieldEmitter.setMinParticleCount(0);
                shieldEmitter.setMaxParticleCount(0);
            }
        } else {
            shieldEmitter.setMinParticleCount(0);
            shieldEmitter.setMaxParticleCount(0);
            super.showDamage(show);
        }
    }

    public void setShipColor(Color color) {
        //        if (colorSprite != null) colorSprite.setColor(color);
    }

    public void addPart(SSShipPart part) {
        parts.add(part);
    }

    public void removePart(SSShipPart part) {
        parts.remove(part);
    }

    public void addModule(SSShipModule module) {
        modules.add(module);
    }

    public void removeModule(SSShipModule module) {
        modules.remove(module);
    }


}
