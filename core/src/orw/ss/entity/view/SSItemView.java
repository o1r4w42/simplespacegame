package orw.ss.entity.view;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter.GradientColorValue;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter.ScaledNumericValue;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import orw.ss.SSUtil;
import orw.ss.entity.SSEntityDef;
import orw.ss.enums.SSEntityProp;
import orw.ss.enums.SSPoolType;
import orw.ss.enums.SSUpgradePartType;
import orw.ss.injection.SSInjectionComponent;

public class SSItemView extends SSScalingView {

    private SSEffect effect;
    private ParticleEmitter emitter;
    private Color color;

    public SSItemView(SSInjectionComponent injector) {
        super(injector, .5f, 1f, 1f);
    }

    @Override
    public void initSprite(TextureAtlas atlas, SSEntityDef definition) {
        super.initSprite(atlas, definition);
        SSUpgradePartType partType = definition.get(SSEntityProp.PART,
                SSUpgradePartType.class,
                null);
        color = partType.getColor();
        setColor(color);
    }


    @Override
    public void initEffects(TextureAtlas atlas) {
        effect = assetManager.getEffect(SSPoolType.DEFAULT, false);
        emitter = effect.findEmitter(SSUtil.SSStrings.DEF_EMITTER);

        ScaledNumericValue angle = emitter.getAngle();
        angle.setLow(0, 360);
        angle.setHigh(0, 360);

        ScaledNumericValue scaleX = emitter.getXScale();
        scaleX.setLow(4f);
        scaleX.setHigh(8f);

        ScaledNumericValue life = emitter.getLife();
        life.setLow(40, 80);
        life.setHigh(100, 200);

        GradientColorValue tint = emitter.getTint();
        float[] colors = tint.getColors();
        colors[0] = color.r;
        colors[1] = color.g;
        colors[2] = color.b;
        colors[3] = color.r;
        colors[4] = color.g;
        colors[5] = color.b;

        emitter.setMinParticleCount(0);
        emitter.setMaxParticleCount(400);
    }

    @Override
    public void updatePosition(float x, float y) {
        super.updatePosition(x, y);
        effect.setPosition(x, y);
    }

    @Override
    protected void addEffects() {
        addEffect(effect);
    }

}
