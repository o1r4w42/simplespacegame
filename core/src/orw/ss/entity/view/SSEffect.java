package orw.ss.entity.view;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;

import orw.ss.SSUtil;
import orw.ss.enums.SSPoolType;

public class SSEffect {

    private PooledEffect effect;
    private SSPoolType type;
    private float scale = 1f;
    private boolean scaled;
    public boolean active;
    private boolean front;

    public SSEffect(PooledEffect effect, SSPoolType type, boolean front) {
        this.effect = effect;
        this.type = type;
        this.front = front;
    }

    public void update(float delta) {
        effect.update(delta);
    }

    public boolean isComplete() {
        return effect.isComplete();
    }

    public void scaleEffect() {
        if (!scaled) {
            effect.scaleEffect(scale * SSUtil.SSFloats.WORLD_TO_BODY);
            scaled = true;
        }
    }

    public void start() {
        effect.start();
        active = true;
    }

    public void draw(Batch batch) {
        effect.draw(batch);
    }

    public ParticleEmitter findEmitter(String name) {
        return effect.findEmitter(name);
    }

    public void setPosition(float x, float y) {
        effect.setPosition(x, y);
    }

    public SSPoolType getType() {
        return type;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public boolean isFront() {
        return front;
    }

}
