package orw.ss.entity.view;

import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter.GradientColorValue;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter.ScaledNumericValue;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;

import orw.ss.SSUtil;
import orw.ss.enums.SSPoolType;
import orw.ss.injection.SSInjectionComponent;

public class SSMeteorView extends SSMovingView {

    private SSEffect thrustEffect;
    private ParticleEmitter thrustEmitter;
    private float thrustScale = 1f;
    private int[] thrustCountValues;
    private float[] lifeValues = {160f, 320f};
    private float[] scaleXValues = {24f, 32f};

    public SSMeteorView(SSInjectionComponent injector) {
        super(injector);
        thrustCountValues = new int[2];
    }

    @Override
    public void initEffects(TextureAtlas atlas) {
        super.initEffects(atlas);
        initThrustEffect();
    }

    @Override
    public void addEffects() {
        super.addEffects();
        addEffect(thrustEffect);
        thrustEmitter.setMinParticleCount(thrustCountValues[0]);
        thrustEmitter.setMaxParticleCount(thrustCountValues[1]);
        //        SSLogger.log("addEffects");
    }

    @Override
    public void reset(float x, float y, float angle) {
        thrustEmitter.setMinParticleCount(0);
        thrustEmitter.setMaxParticleCount(0);
        //        SSLogger.log("reset");
        super.reset(x, y, angle);
        //        addEffect(thrustEffect);
    }

    @Override
    public void updatePosition(float x, float y) {
        super.updatePosition(x, y);
        thrustEffect.setPosition(x, y);
    }

    @Override
    public void updateAngleRad(float rad) {
        super.updateAngleRad(rad);
        float a = SSUtil.modAngleRad(rad, -MathUtils.PI)
                * MathUtils.radiansToDegrees;
        ScaledNumericValue angleVal = thrustEmitter.getAngle();
        angleVal.setLow(a);
        angleVal.setHigh(a);
    }

    @Override
    public void showDestroy() {
        super.showDestroy();
        thrustEmitter.setMinParticleCount(0);
        thrustEmitter.setMaxParticleCount(0);
        removeEffect(thrustEffect);
    }


    private void initThrustEffect() {
        thrustEffect = assetManager.getEffect(SSPoolType.DEFAULT, false);
        thrustEmitter = thrustEffect.findEmitter(SSUtil.SSStrings.DEF_EMITTER);
        GradientColorValue tint = thrustEmitter.getTint();
        initThrustColor(tint.getColors());
        thrustEffect.setScale(thrustScale);
        thrustCountValues[0] = thrustEmitter.getMinParticleCount();
        thrustCountValues[1] = thrustEmitter.getMaxParticleCount();
        ScaledNumericValue life = thrustEmitter.getLife();
        life.setLow(lifeValues[0]);
        life.setHigh(lifeValues[1]);
        ScaledNumericValue scaleX = thrustEmitter.getXScale();
        scaleX.setHigh(scaleXValues[0]);
        scaleX.setLow(scaleXValues[1]);
    }

    private void initThrustColor(float[] colors) {
        //        SSLogger.log("initThrustColor:" + colors.length);
        colors[0] = 1f;
        colors[1] = 178f / 255f;
        colors[2] = 127f / 255f;
        colors[3] = 127f / 255f;
        colors[4] = 51f / 255f;
        colors[5] = 0f;
    }

    @Override
    public void setScale(float scale) {
        thrustScale = scale;
    }
}
