package orw.ss.entity.view;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import java.util.ArrayList;
import java.util.List;

import orw.ss.assets.SSAssetManager;
import orw.ss.injection.SSInjectionComponent;

public abstract class SSEffectView extends SSEntityView {

    private List<SSEffect> effectsBack;
    private List<SSEffect> effectsFront;
    private List<SSEffect> finishedEffects;
    private SSEffectListener effectListener;

    public SSEffectView(SSInjectionComponent injector) {
        super(injector);
        effectsBack = new ArrayList<>();
        effectsFront = new ArrayList<>();
        finishedEffects = new ArrayList<>();
    }

    @Override
    public void spawn() {
        super.spawn();
        addEffects();
    }

    @Override
    public void update(float delta) {
        for (SSEffect effect : effectsBack) {
            if (effect.active) {
                effect.update(delta);
                if (effect.isComplete()) finishedEffects.add(effect);
            }
        }
        for (SSEffect effect : effectsFront) {
            if (effect.active) {
                effect.update(delta);
                if (effect.isComplete()) finishedEffects.add(effect);
            }
        }
        if (finishedEffects.size() > 0) {
            for (SSEffect effect : finishedEffects) {
                if (effect.isFront()) effectsFront.remove(effect);
                else effectsBack.remove(effect);
                effectListener.effectFinished(effect);
            }
            finishedEffects.clear();
        }
    }

    @Override
    public void render(Batch batch) {
        if (effectsBack.size() > 0) {
            int dst = batch.getBlendDstFunc();
            int src = batch.getBlendSrcFunc();
            for (SSEffect effect : effectsBack)
                effect.draw(batch);
            batch.setBlendFunction(src, dst);
        }
        super.render(batch);
        if (effectsFront.size() > 0) {
            int dst = batch.getBlendDstFunc();
            int src = batch.getBlendSrcFunc();
            for (SSEffect effect : effectsFront)
                effect.draw(batch);
            batch.setBlendFunction(src, dst);
        }
    }

    @Override
    public void reset(float x, float y, float angle) {
        super.reset(x, y, angle);
        effectsFront.clear();
        effectsBack.clear();
        finishedEffects.clear();
        addEffects();
    }

    public void setListener(SSEffectListener listener) {
        effectListener = listener;
    }

    public void addEffect(SSEffect effect) {
        effect.scaleEffect();
        effect.start();
        if (effect.isFront()) effectsFront.add(effect);
        else effectsBack.add(effect);
    }

    public void removeEffect(SSEffect effect) {
        effect.active = false;
        if (effect.isFront()) effectsFront.remove(effect);
        else effectsBack.remove(effect);
    }

    public abstract void initEffects(TextureAtlas atlas);

    protected abstract void addEffects();

    public interface SSEffectListener {
        public void effectFinished(SSEffect effect);
    }
}
