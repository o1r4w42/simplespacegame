package orw.ss.physics;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;

import java.util.ArrayList;
import java.util.List;

import orw.ss.SSPool;
import orw.ss.entity.SSEntity;

public class SSContact extends SSPool {

    private static List<SSContact> pool = new ArrayList<SSContact>();

    public static SSContact getContact(Contact c) {
        SSContact contact = (SSContact) getPool(pool);
        if (contact == null) {
            contact = new SSContact();
        }
        contact.set(c);
        return contact;
    }

    public Fixture fixA;
    public Fixture fixB;
    public SSFixtureData dataA;
    public SSFixtureData dataB;
    public SSEntity entA;
    public SSEntity entB;

    @Override
    public void free() {
        pool.add(this);
    }

    private void set(Contact contact) {
        fixA = contact.getFixtureA();
        fixB = contact.getFixtureB();
        dataA = (SSFixtureData) fixA.getUserData();
        dataB = (SSFixtureData) fixB.getUserData();
        entA = dataA.getOwner();
        entB = dataB.getOwner();
    }


}
