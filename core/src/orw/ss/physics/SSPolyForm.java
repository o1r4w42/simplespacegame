package orw.ss.physics;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;

import orw.ss.SSUtil;

public class SSPolyForm extends SSCollForm {

    private String points;
    private Vector2[] vertices;

    public SSPolyForm(float x, float y, String points) {
        super(x, y);
        this.points = points;
    }

    @Override
    protected Shape createShape() {
        String[] split = points.split(" ");
        vertices = new Vector2[split.length - 1];
        for (int i = 0; i < split.length - 1; i++) {
            String[] subSplit = split[i].split(",");
            vertices[i] = new Vector2(
                            Float.parseFloat(subSplit[0]) * SSUtil.SSFloats.WORLD_TO_BODY
                                            + x,
                            Float.parseFloat(subSplit[1]) * SSUtil.SSFloats.WORLD_TO_BODY
                                            + y);
        }
        PolygonShape shape = new PolygonShape();
        shape.set(vertices);
        return shape;
    }

    public Vector2[] getVertices() {
        return vertices;
    }

}
