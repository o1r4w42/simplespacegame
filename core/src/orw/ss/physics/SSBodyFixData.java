package orw.ss.physics;

import orw.ss.entity.SSEntity;
import orw.ss.enums.SSCollType;

public class SSBodyFixData extends SSFixtureData {

    public SSBodyFixData(SSEntity owner) {
        super(owner, SSCollType.BODY);
    }

}
