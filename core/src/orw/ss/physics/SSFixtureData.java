package orw.ss.physics;

import orw.ss.entity.SSEntity;
import orw.ss.enums.SSCollType;

public abstract class SSFixtureData {

    private SSEntity owner;
    private SSCollType type;

    public SSFixtureData(SSEntity owner, SSCollType type) {
        this.owner = owner;
        this.type = type;
    }

    public SSEntity getOwner() {
        return owner;
    }

    public SSCollType getType() {
        return type;
    }
}
