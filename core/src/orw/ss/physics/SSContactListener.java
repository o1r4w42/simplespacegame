package orw.ss.physics;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

import orw.ss.entity.SSEntity;

public class SSContactListener implements ContactListener {

    @Override
    public void beginContact(Contact contact) {
        Fixture fixA = contact.getFixtureA();
        SSFixtureData dataA = (SSFixtureData) fixA.getUserData();
        SSEntity entA = dataA.getOwner();
        entA.addCollision(contact);
    }

    @Override
    public void endContact(Contact contact) {
        Fixture fixA = contact.getFixtureA();
        SSFixtureData dataA = (SSFixtureData) fixA.getUserData();
        SSEntity entA = dataA.getOwner();
        entA.removeCollision(contact);
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }

}
