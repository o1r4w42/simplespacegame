package orw.ss.physics;

import orw.ss.entity.SSEntity;
import orw.ss.entity.equip.parts.SSWeaponLogic;
import orw.ss.enums.SSCollType;

public class SSWeaponFixData extends SSFixtureData {

    private SSWeaponLogic logic;

    public SSWeaponFixData(SSEntity owner, SSWeaponLogic logic) {
        super(owner, SSCollType.WEAPON);
        this.logic = logic;
    }

    public SSWeaponLogic getLogic() {
        return logic;
    }
}
