package orw.ss.physics;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Shape;

import orw.ss.SSUtil;

public class SSCircleForm extends SSCollForm {

    private float radius;

    public SSCircleForm(float x, float y, float radius) {
        super(x, y);
        this.radius = radius * SSUtil.SSFloats.WORLD_TO_BODY / 2f;
    }

    @Override
    protected Shape createShape() {
        CircleShape shape = new CircleShape();
        shape.setRadius(radius);
        Vector2 pos = shape.getPosition();
        pos.set(x + radius, y + radius);
        shape.setPosition(pos);
        return shape;
    }

}
