package orw.ss.physics;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;

import orw.ss.SSUtil;

public class SSRectForm extends SSCollForm {

    private float width;
    private float height;

    public SSRectForm(float x, float y, float width, float height) {
        super(x, y);
        this.width = width * SSUtil.SSFloats.WORLD_TO_BODY / 2f;
        this.height = height * SSUtil.SSFloats.WORLD_TO_BODY / 2f;
    }

    @Override
    protected Shape createShape() {
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(width, height, new Vector2(x + width, y + height), 0);
        return shape;
    }


}
