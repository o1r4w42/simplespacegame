package orw.ss.physics;

import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;

import orw.ss.SSUtil;
import orw.ss.entity.SSEntity;

public abstract class SSCollForm {

    protected float x;
    protected float y;

    public SSCollForm(float x, float y) {
        this.x = x * SSUtil.SSFloats.WORLD_TO_BODY;
        this.y = y * SSUtil.SSFloats.WORLD_TO_BODY;
    }

    public void createFixture(SSEntity owner, SSBody body, short collId,
                              short collMask, boolean sensor, float restitution) {
        FixtureDef fdef = new FixtureDef();
        fdef.filter.categoryBits = collId;
        fdef.filter.maskBits = collMask;
        fdef.isSensor = sensor;
        fdef.restitution = restitution;
        fdef.shape = createShape();
        Fixture f = body.createFixture(fdef, true, owner);
        f.setUserData(new SSBodyFixData(owner));
    }

    protected abstract Shape createShape();
}
