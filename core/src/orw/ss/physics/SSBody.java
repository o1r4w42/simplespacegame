package orw.ss.physics;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import java.util.ArrayList;

import javax.inject.Inject;

import orw.ORWLogger;
import orw.ss.SSVector;
import orw.ss.entity.SSEntity;
import orw.ss.entity.SSEntityDef;
import orw.ss.enums.SSEntityProp;
import orw.ss.enums.SSEntityStat;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.entity.SSWorld;

public class SSBody {
    private final ArrayList<SSContact> newCollisions;
    private final ArrayList<SSContact> solvedCollisions;
    private final SSVector lastPos;
    private final SSVector firstPos;

    private Body body;
    private BodyDef bodyDef;
    private short collId;
    private short collMask;
    private Fixture bodyFixture;
    private SSCollForm collForm;

    @Inject
    SSWorld world;

    public SSBody(SSInjectionComponent injector) {
        injector.injectBody(this);

        newCollisions = new ArrayList<>();
        solvedCollisions = new ArrayList<>();
        lastPos = SSVector.getVector();
        firstPos = SSVector.getVector();
    }

    public void applyVelocity(SSVector velocity) {
        //        velocity.setZero();
        body.applyForceToCenter(velocity.getX(), velocity.getY(), true);
    }

    public void init(float x, float y, short collId, short collMask,
                     float angle, BodyType bodyType) {
        this.collId = collId;
        this.collMask = collMask;
        bodyDef = new BodyDef();
        bodyDef.position.set(x, y);
        bodyDef.type = bodyType;
        bodyDef.angle = angle * MathUtils.degreesToRadians;
        lastPos.set(x, y);
        firstPos.set(x, y);
    }

    public void initVelocity(float velocity) {
        bodyDef.linearVelocity.set(velocity, 0f);
        bodyDef.linearVelocity.setAngleRad(bodyDef.angle);
    }

    public void free() {
        lastPos.free();
        firstPos.free();
        for (SSContact contact : newCollisions)
            contact.free();
        newCollisions.clear();
        for (SSContact contact : solvedCollisions)
            contact.free();
        solvedCollisions.clear();
    }

    public void reset(SSEntity owner) {
        resetCollisionIds();
        body.setTransform(bodyDef.position, bodyDef.angle);
        body.setLinearVelocity(bodyDef.linearVelocity);
        setPositionToBody(owner);
    }

    public void update(SSEntity owner) {
        if (newCollisions.size() > 0) {
            for (SSContact c : newCollisions)
                checkCollision(c, owner);
            newCollisions.clear();
        }
        if (solvedCollisions.size() > 0) {
            for (SSContact c : solvedCollisions)
                checkCollisionEnd(c, owner);
            solvedCollisions.clear();
        }
    }

    public void destroy() {
        if (body != null) {
            bodyFixture = null;
            world.destroyBody(body);
            body = null;
        }
    }

    public void createBody(SSEntity owner) {
        body = world.createBody(bodyDef);
        owner.updatePosition(firstPos.getX(), firstPos.getY());

        SSEntityDef definition = owner.getDefinition();

        collForm = definition.get(SSEntityProp.BODY, SSCollForm.class, null);
        if (collForm == null) return;

        float rest = 1.5f;
        if (definition.getStat(SSEntityStat.SIZE) != 0)
            rest = .5f + 1f / definition.getStat(SSEntityStat.SIZE);
        boolean sensor = definition.get(SSEntityProp.SENSOR, boolean.class,
                false);
        collForm.createFixture(owner, this, collId, collMask, sensor, rest);

    }

    public Fixture createFixture(FixtureDef fdef, boolean add, SSEntity owner) {
        Fixture fixture = body.createFixture(fdef);
        if (add) {
            if (bodyFixture == null) bodyFixture = fixture;
            else ORWLogger.log(ORWLogger.LogCategory.ERROR,
                    "Double body fixture for " + owner + "!");
        }
        return fixture;
    }

    public void destroyFixture(Fixture fixture) {
        body.destroyFixture(fixture);
    }


    public void resetCollisionIds() {
        setCollisionIds(collId, collMask);
    }

    public void clearCollisionIds() {
        setCollisionIds((short) 0, (short) 0);
    }


    private void setCollisionIds(short collId, short collMask) {
        Filter filter = bodyFixture.getFilterData();
        filter.categoryBits = collId;
        filter.maskBits = collMask;
        bodyFixture.setFilterData(filter);
    }

    public void addCollision(SSContact contact) {
        newCollisions.add(contact);
    }

    public void removeCollision(SSContact contact) {
        solvedCollisions.add(contact);
    }

    private void checkCollision(SSContact contact, SSEntity owner) {
        if (contact.dataA.getOwner() != owner) {
            ORWLogger.log(ORWLogger.LogCategory.ERROR,
                    "checkCollision: dataA.getOwner()!=this");
        }
        if (owner.isActive() && contact.entB.isActive())
            owner.parseCollision(contact.fixA, contact.dataA, contact.fixB,
                    contact.dataB, contact.entB);
        contact.free();
    }

    private void checkCollisionEnd(SSContact contact, SSEntity owner) {
        if (contact.dataA.getOwner() != owner) {
            ORWLogger.log(ORWLogger.LogCategory.ERROR,
                    "checkCollisionEnd: dataA.getOwner()!=this");
        }
        if (owner.isActive() && contact.entB.isActive())
            owner.parseCollisionEnd(contact.fixA, contact.dataA, contact.fixB,
                    contact.dataB, contact.entB);
        contact.free();
    }

    public float getBodyAngleRad() {
        return body.getAngle();
    }

    public float getBodyAngle() {
        return body.getAngle() * MathUtils.radiansToDegrees;
    }

    public float getVelocity() {
        return body.getLinearVelocity().len();
    }

    public float getVelAngleRad() {
        return body.getLinearVelocity().angleRad();
    }

    public float getVelAngle() {
        return body.getLinearVelocity().angle();
    }


    public void setVelocityAngleRad(float rad) {
        body.setLinearVelocity(body.getLinearVelocity().setAngleRad(rad));
    }

    public void setVelocityLength(float len) {
        body.setLinearVelocity(body.getLinearVelocity().setLength(len));
    }

    public void clampVelocity(float max) {
//        if (getVelocity() > max)
        body.setLinearVelocity(body.getLinearVelocity().setLength(max));
    }

    public void stop() {
        body.setLinearVelocity(0, 0);
    }

    public void setPosition(float x, float y) {
        body.setTransform(x, y, body.getAngle());
    }

    public void setPositionToBody(SSEntity owner) {
        SSVector tmpVec = SSVector.getVector(body.getPosition());
        if (lastPos.getX() != tmpVec.getX() || lastPos.getY() != tmpVec.getY()) {
            lastPos.set(tmpVec.getX(), tmpVec.getY());
            owner.updatePosition(tmpVec.getX(), tmpVec.getY());
        }
        tmpVec.free();
    }

    public void setAngleRad(float rad) {
        body.setTransform(body.getWorldCenter(), rad);
    }

    public Fixture getBodyFixture() {
        return bodyFixture;
    }

    public SSCollForm getCollForm() {
        return collForm;
    }


}
