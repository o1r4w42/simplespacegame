package orw.ss.injection;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import orw.ss.SimpleSpaceGame;
import orw.ss.assets.SSAssetManager;
import orw.ss.assets.SSSkin;
import orw.ss.entity.SSEntityFactory;
import orw.ss.entity.SSWorld;
import orw.ss.entity.equip.SSEquipFactory;
import orw.ss.input.SSInputController;
import orw.ss.input.SSPlayerController;
import orw.ss.screen.SSCamera;
import orw.ss.screen.SSScreenHandler;
import orw.ss.screen.ui.SSUserInterface;

@Module
public class SSInjectionModule {

    private final SimpleSpaceGame game;

    public SSInjectionModule(SimpleSpaceGame game) {
        this.game = game;
    }

    @Provides
    @Singleton
    SpriteBatch spriteBatch() {
        return new SpriteBatch();
    }

    @Provides
    @Singleton
    SSAssetManager assetManager() {
        return new SSAssetManager();
    }

    @Provides
    @Singleton
    SSSkin skin() {
        return new SSSkin(game.getInjector());
    }

    @Provides
    @Singleton
    SSScreenHandler screenHandler() {
        return new SSScreenHandler();
    }

    @Provides
    @Singleton
    SSCamera camera() {
        return new SSCamera();
    }

    @Provides
    @Singleton
    SSWorld world() {
        return new SSWorld(game.getInjector());
    }

    @Provides
    @Singleton
    SSInputController inputController() {
        return new SSInputController();
    }

    @Provides
    @Singleton
    SSEntityFactory entityFactory() {
        return new SSEntityFactory(game.getInjector());
    }

    @Provides
    @Singleton
    SSEquipFactory equipFactory() {
        return new SSEquipFactory(game.getInjector());
    }

    @Provides
    @Singleton
    SSPlayerController playerController() {
        return new SSPlayerController();
    }

    @Provides
    @Singleton
    SSUserInterface userInterface() {
        return new SSUserInterface(game.getInjector());
    }

}
