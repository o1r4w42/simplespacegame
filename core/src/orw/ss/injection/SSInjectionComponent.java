package orw.ss.injection;

import javax.inject.Singleton;

import dagger.Component;
import orw.ss.SimpleSpaceGame;
import orw.ss.assets.SSSkin;
import orw.ss.entity.SSEntityFactory;
import orw.ss.entity.SSWorld;
import orw.ss.entity.equip.SSEquipFactory;
import orw.ss.entity.logic.SSEntityLogic;
import orw.ss.entity.logic.SSShipLogic;
import orw.ss.entity.view.SSEntityView;
import orw.ss.entity.view.SSMovingView;
import orw.ss.physics.SSBody;
import orw.ss.screen.SSScreen;
import orw.ss.screen.ui.SSUserInterface;
import orw.ss.screen.ui.dialog.SSDialog;
import orw.ss.screen.ui.menu.SSMenu;

@Singleton
@Component(modules = SSInjectionModule.class)
public interface SSInjectionComponent {

    void injectSimpleSpaceGame(SimpleSpaceGame simpleSpaceGame);

    void injectScreen(SSScreen screen);
    void injectUserInterface(SSUserInterface userInterface);
    void injectDialog(SSDialog dialog);
    void injectMenu(SSMenu menu);

    void injectSkin(SSSkin skin);

    void injectWorld(SSWorld world);

    void injectBody(SSBody body);

    void injectEntityFactory(SSEntityFactory entityFactory);
    void injectEquipFactory(SSEquipFactory equipFactory);

    void injectEntityView(SSEntityView entityView);
    void injectMovingView(SSMovingView movingView);

    void injectEntityLogic(SSEntityLogic entityLogic);
    void injectShipLogic(SSShipLogic shipLogic);
}
