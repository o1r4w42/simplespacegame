package orw.ss;

import java.util.List;

public abstract class SSPool {

    protected static SSPool getPool(List<? extends SSPool> pool) {
        if (pool.size() > 0) {
            SSPool p = pool.get(0);
            pool.remove(0);
            return p;
        }
        return null;
    }

    public abstract void free();
}
