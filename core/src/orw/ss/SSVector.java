package orw.ss;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

import orw.ORWLogger;

public class SSVector extends SSPool {

    private static List<SSVector> pool = new ArrayList<SSVector>();
    private static int vectorCount;
    private static boolean counting;

    public static SSVector getVector() {
        return getVector(0f, 0f);
    }

    public static SSVector getVector(Vector2 vector) {
        return getVector(vector.x, vector.y);
    }

    public static SSVector getVector(SSVector vector) {
        return getVector(vector.getX(), vector.getY());
    }

    public static SSVector getVector(float x, float y) {
        boolean wasCounting = counting;
        SSVector vector = (SSVector) getPool(pool);
        if (vector == null) {
            vector = new SSVector();
            vectorCount++;
            counting = true;
        }
        else counting = false;
        vector.set(x, y);
        return vector;
    }

    public static void dispose() {
        if (pool.size() != vectorCount)
            ORWLogger.log(ORWLogger.LogCategory.ERROR,
                            (vectorCount - pool.size()) + " vecti not free!");
    }

    private Vector2 vector;

    public SSVector() {
        vector = new Vector2();
    }

    @Override
    public String toString() {
        return vector.toString();
    }

    @Override
    public void free() {
        pool.add(this);
    }

    public void set(float x, float y) {
        vector.set(x, y);
    }

    public void set(SSVector other) {
        set(other.getX(), other.getY());
    }

    public float getX() {
        return vector.x;
    }

    public float getY() {
        return vector.y;
    }

    public void sub(float x, float y) {
        vector.sub(x, y);
    }

    public float len() {
        return vector.len();
    }

    public float angleRad() {
        return vector.angleRad();
    }

    public float angle() {
        return vector.angle();
    }

    public void setAngle(float angle) {
        vector.setAngle(angle);
    }
    public void setAngleRad(float angle) {
        vector.setAngleRad(angle);
    }

    public void scl(float len) {
        vector.scl(len);
    }

    public void add(float x, float y) {
        vector.add(x, y);
    }

    public void nor() {
        vector.nor();
    }

    public void setZero() {
        vector.setZero();
    }

    public void setLength(float len) {
        vector.setLength(len);
    }

    public boolean isZero() {
        return vector.isZero();
    }

    public void add(SSVector other) {
        add(other.getX(), other.getY());
    }

    public void setX(float x) {
        vector.x = x;
    }

    public void setY(float y) {
        vector.y = y;
    }

    public boolean equals(float x, float y) {
        return vector.epsilonEquals(x, y);
    }
}
