package orw.ss.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

import java.util.HashMap;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.view.SSEffect;
import orw.ss.enums.SSDefType;
import orw.ss.enums.SSEntityProp;
import orw.ss.enums.SSEntityStat;
import orw.ss.enums.SSModuleType;
import orw.ss.enums.SSPoolType;
import orw.ss.enums.SSShipPartType;
import orw.ss.enums.SSSlotType;
import orw.ss.enums.SSUpgradePartType;
import orw.ss.grafix.SSGrafix;

public class SSAssetManager extends AssetManager {

    private final HashMap<SSDefType, SSEntityDef> definitions;
    private final HashMap<SSPoolType, ParticleEffectPool> effectPools;
    private final XmlReader reader;

    public SSAssetManager() {
        definitions = new HashMap<>();
        effectPools = new HashMap<>();
        reader = new XmlReader();

        FileHandleResolver resolver = new InternalFileHandleResolver();
        setLoader(FreeTypeFontGenerator.class,
                new FreeTypeFontGeneratorLoader(resolver));
        setLoader(BitmapFont.class, ".ttf",
                new FreetypeFontLoader(resolver));
    }

    public void load() {
        load(SSUtil.SSStrings.FILE_SPRITE_ATLAS, TextureAtlas.class);

        FreetypeFontLoader.FreeTypeFontLoaderParameter mySmallFont =
                new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        mySmallFont.fontFileName = SSUtil.SSStrings.FILE_FONT_DEF;
        mySmallFont.fontParameters.size = 32;
        load(SSUtil.SSStrings.FILE_FONT_DEF, BitmapFont.class, mySmallFont);

        initTextures();

        loadDefinitions();
    }

    private void initTextures() {
        Pixmap pm;
        Texture t;

        pm = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        SSGrafix.fillRect(pm, Color.WHITE, 0, 0, 1, 1);
        t = new Texture(pm);
        addAsset(SSUtil.SSStrings.WHITE_PIXEL, Texture.class, t);
        pm.dispose();
    }

    private void loadDefinitions() {
        initDefinitions(SSUtil.SSStrings.FILE_BG_DEFS);
        initDefinitions(SSUtil.SSStrings.FILE_PART_DEFS);
        initDefinitions(SSUtil.SSStrings.FILE_ITEM_DEFS);
        initDefinitions(SSUtil.SSStrings.FILE_MODULE_DEFS);
        initDefinitions(SSUtil.SSStrings.FILE_FRAME_DEFS);
        initDefinitions(SSUtil.SSStrings.FILE_MET_DEFS);
    }

    private void initDefinitions(String name) {
        XmlReader.Element root = reader.parse(Gdx.files.internal(SSUtil.SSStrings.PRE_DEF + name));
        XmlReader.Element props = root.getChildByName(SSUtil.SSStrings.PROPERTIES);

        if (root != null) {
            Array<XmlReader.Element> tiles = root.getChildrenByName(SSUtil.SSStrings.TILE);
            for (XmlReader.Element tile : tiles) {
                SSEntityDef def = initDefinition(tile);
                if (props != null) for (XmlReader.Element prop : props
                        .getChildrenByName(SSUtil.SSStrings.PROPERTY))
                    initDefProp(def, prop, root);
            }
        }
    }

    private SSEntityDef initDefinition(XmlReader.Element tile) {
        XmlReader.Element img = tile.getChildByName(SSUtil.SSStrings.IMAGE);
        String fullSource = img.getAttribute(SSUtil.SSStrings.SOURCE);

        String[] split = SSUtil.splitSource(fullSource, SSUtil.SSStrings.FILE_PNG);
        SSEntityDef def = new SSEntityDef(SSDefType.get(split[0] + split[1]));

        def.addProperty(SSEntityProp.VIEW_SRC, split[0]);
        def.addProperty(SSEntityProp.VIEW_NAME, split[2]);
        def.addProperty(SSEntityProp.VIEW_IND, Integer.parseInt(split[3]));

        XmlReader.Element props = tile.getChildByName(SSUtil.SSStrings.PROPERTIES);
        if (props != null)
            for (XmlReader.Element prop : props.getChildrenByName(SSUtil.SSStrings.PROPERTY))
                initDefProp(def, prop, img);

        XmlReader.Element group = tile.getChildByName(SSUtil.SSStrings.OBJECT_GROUP);
        if (group != null) {
            Array<XmlReader.Element> objects = group.getChildrenByName(SSUtil.SSStrings.OBJECT);
            for (XmlReader.Element object : objects)
                initDefProp(def, object, img);
        }
        addDefinition(def);
        return def;
    }

    private void initDefProp(SSEntityDef def, XmlReader.Element prop, XmlReader.Element parent) {
        String name = prop.getAttribute(SSUtil.SSStrings.NAME);
        SSEntityProp entProp = SSEntityProp.get(name);
        if (entProp != null) entProp.parse(def, prop, parent);
    }

    private void addDefinition(SSEntityDef definition) {
        definitions.put(definition.getDefType(), definition);
    }

    public SSEntityDef getDefinition(SSDefType type) {
        if (!definitions.containsKey(type)) ORWLogger.log(ORWLogger.LogCategory.ERROR,
                "SSEntityDef " + type + " not found!");
        return definitions.get(type);
    }


    public void initEffects() {
        TextureAtlas atlas = get(SSUtil.SSStrings.FILE_SPRITE_ATLAS, TextureAtlas.class);

        for (SSPoolType type : SSPoolType.values())
            loadEffect(type, atlas);
    }

    private void loadEffect(SSPoolType type, TextureAtlas atlas) {
        loadEffect(type.getSource(), type, atlas, 10, 100);
    }

    private void loadEffect(String name, SSPoolType type, TextureAtlas atlas,
                            int cap, int max) {
        ParticleEffect effect = new ParticleEffect();
        effect.load(Gdx.files.internal(SSUtil.SSStrings.PRE_PART + name),
                atlas, SSUtil.SSStrings.PRE_PART);
        effect.setEmittersCleanUpBlendFunction(false);
        effectPools.put(type, new ParticleEffectPool(effect, cap, max));
    }

    public SSEffect getEffect(SSPoolType type, boolean front) {
        return new SSEffect(effectPools.get(type).obtain(), type, front);
    }


    public static final SSEntityParser boolParser = new SSEntityParser() {

        @Override
        public void parse(SSEntityDef def, XmlReader.Element element, XmlReader.Element parent,
                          String name, SSEntityProp prop) {
            boolean value = Boolean
                    .parseBoolean(element.getAttribute(SSUtil.SSStrings.VALUE));
            def.addProperty(prop, value);
        }

    };

    public static final SSEntityParser effectParser = new SSEntityParser() {

        @Override
        public void parse(SSEntityDef def, XmlReader.Element element, XmlReader.Element parent,
                          String name, SSEntityProp prop) {
            String value = element.getAttribute(SSUtil.SSStrings.VALUE);
            SSPoolType type = SSPoolType.get(value);
            if (type != null) def.addProperty(prop, type);
        }

    };
    public static final SSEntityParser shipPartTypeParser = new SSEntityParser() {

        @Override
        public void parse(SSEntityDef def, XmlReader.Element element, XmlReader.Element parent,
                          String name, SSEntityProp prop) {
            String value = element.getAttribute(SSUtil.SSStrings.VALUE);
            SSShipPartType type = SSShipPartType.get(value);
            if (type != null) def.addProperty(prop, type);
        }

    };
    public static final SSEntityParser bodyParser = new SSEntityParser() {

        @Override
        public void parse(SSEntityDef def, XmlReader.Element element, XmlReader.Element parent,
                          String name, SSEntityProp prop) {
            float pw = parent.getFloatAttribute(SSUtil.SSStrings.WIDTH, 0f);
            float ph = parent.getFloatAttribute(SSUtil.SSStrings.HEIGHT, 0f);
            float x = element.getFloatAttribute(SSUtil.SSStrings.X, 0f);
            float y = element.getFloatAttribute(SSUtil.SSStrings.Y, 0f);
            float w = element.getFloatAttribute(SSUtil.SSStrings.WIDTH, 0f);
            float h = element.getFloatAttribute(SSUtil.SSStrings.HEIGHT, 0f);
            XmlReader.Element ellipse = element.getChildByName("ellipse");
            if (ellipse != null) {
                def.addCircleBody(x - pw / 2f, y - ph / 2f, Math.max(w, h));
                return;
            }
            XmlReader.Element polyline = element.getChildByName("polyline");
            if (polyline != null) {
                def.addPolyBody(x - pw / 2f, y - ph / 2f,
                        polyline.getAttribute("points"));
                return;
            }
            def.addRectBody(x - pw / 2f, y - ph / 2f, w, h);
        }

    };

    public static final SSEntityParser fileParser = new SSEntityParser() {

        @Override
        public void parse(SSEntityDef def, XmlReader.Element element, XmlReader.Element parent,
                          String name, SSEntityProp prop) {
            String value = element.getAttribute(SSUtil.SSStrings.VALUE);
            String[] split = SSUtil.splitSource(value, ".png");
            value = split[0] + split[1];
            def.addProperty(prop, value);
        }

    };
    public static final SSEntityParser statParser = new SSEntityParser() {

        @Override
        public void parse(SSEntityDef def, XmlReader.Element element, XmlReader.Element parent,
                          String name, SSEntityProp prop) {
            SSEntityStat stat = SSEntityStat.get(name);
            float value = element.getFloatAttribute(SSUtil.SSStrings.VALUE, 0f);
//            ORWLogger.log("stat "+name+":"+value);
            def.setStat(stat, value);
        }

    };
    public static final SSEntityParser angleParser = new SSEntityParser() {

        @Override
        public void parse(SSEntityDef def, XmlReader.Element element, XmlReader.Element parent,
                          String name, SSEntityProp prop) {
            SSEntityStat stat = SSEntityStat.get(name);
            float value = element.getFloatAttribute(SSUtil.SSStrings.VALUE, 0f);
            def.setStat(stat, value * MathUtils.degreesToRadians);
        }

    };

    public static final SSEntityParser slotParser = new SSEntityParser() {

        @Override
        public void parse(SSEntityDef def, XmlReader.Element element, XmlReader.Element parent,
                          String name, SSEntityProp prop) {
            String typeName = element.getAttribute(SSUtil.SSStrings.TYPE);
            SSSlotType type = SSSlotType.get(typeName);
            if (type == null) return;
            float pw = Float.parseFloat(parent.getAttribute(SSUtil.SSStrings.WIDTH));
            float ph = Float.parseFloat(parent.getAttribute(SSUtil.SSStrings.HEIGHT));
            float x = Float.parseFloat(element.getAttribute(SSUtil.SSStrings.X));
            float y = Float.parseFloat(element.getAttribute(SSUtil.SSStrings.Y));
            float w = Float.parseFloat(element.getAttribute(SSUtil.SSStrings.WIDTH));
            float h = Float.parseFloat(element.getAttribute(SSUtil.SSStrings.HEIGHT));

            boolean add = false;
            XmlReader.Element props = element.getChildByName(SSUtil.SSStrings.PROPERTIES);

            if (props == null) {
                def.addSlot(type, x - pw / 2 + w / 2, y - ph / 2 + h / 2, w, h,
                        add);
                return;
            }
            for (XmlReader.Element e : props.getChildrenByName(SSUtil.SSStrings.PROPERTY)) {
                String eName = e.getAttribute(SSUtil.SSStrings.NAME);
                switch (eName) {
                    case SSUtil.SSStrings.ADD:
                        add = e.getBooleanAttribute(SSUtil.SSStrings.VALUE);
                        break;
                }
            }
            def.addSlot(type, x - pw / 2 + w / 2, y - ph / 2 + h / 2, w, h,
                    add);
        }

    };

    public static final SSEntityParser moduleParser = new SSEntityParser() {

        @Override
        public void parse(SSEntityDef def, XmlReader.Element element, XmlReader.Element parent,
                          String name, SSEntityProp prop) {
            String typeName = element.getAttribute(SSUtil.SSStrings.TYPE);
            SSModuleType type = SSModuleType.get(typeName);
            if (type == null) return;

            float pw = Float.parseFloat(parent.getAttribute(SSUtil.SSStrings.WIDTH));
            float ph = Float.parseFloat(parent.getAttribute(SSUtil.SSStrings.HEIGHT));
            float x = Float.parseFloat(element.getAttribute(SSUtil.SSStrings.X));
            float y = Float.parseFloat(element.getAttribute(SSUtil.SSStrings.Y));
            float w = Float.parseFloat(element.getAttribute(SSUtil.SSStrings.WIDTH));
            float h = Float.parseFloat(element.getAttribute(SSUtil.SSStrings.HEIGHT));
            XmlReader.Element props = element.getChildByName(SSUtil.SSStrings.PROPERTIES);

            int index = 0;
            int[][] points = null;
            float[] size = new float[2];
//            int[] preModules = null;
            String[] split;

            for (XmlReader.Element e : props
                    .getChildrenByName(SSUtil.SSStrings.PROPERTY)) {
                String eName = e.getAttribute(SSUtil.SSStrings.NAME);
                switch (eName) {
                    case SSUtil.SSStrings.INDEX:
                        index = Integer.parseInt(e.getAttribute(SSUtil.SSStrings.VALUE));
                        break;
                    case SSUtil.SSStrings.POINTS:
                        String pointString = e.getText();
                        split = pointString.split(SSUtil.SSStrings.NL);
                        points = new int[split.length][3];
                        for (int i = 0; i < split.length; i++) {
                            String[] subSplit = split[i].split(SSUtil.SSStrings.COMMA);
                            points[i][0] = Integer.parseInt(subSplit[0]);
                            points[i][1] = (int) Float.parseFloat(subSplit[1]);
                            points[i][2] = (int) Float.parseFloat(subSplit[2]);
                        }
                        break;
                    case SSUtil.SSStrings.SIZE:
                        String sizeString = e.getAttribute(SSUtil.SSStrings.VALUE);
                        split = sizeString.split(SSUtil.SSStrings.COMMA);
                        size[0] = Float.parseFloat(split[0]) * SSUtil.SSFloats.WORLD_TO_BODY;
                        size[1] = Float.parseFloat(split[1]) * SSUtil.SSFloats.WORLD_TO_BODY;
                        break;
//                    case SSUtil.SSStrings.PRE_MODULES:
//                        String preModuleString = e.getAttribute(SSUtil.SSStrings.VALUE);
//                        split = preModuleString.split(SSUtil.SSStrings.COMMA);
//                        preModules = new int[split.length];
//                        for (int i=0;i<split.length;i++) {
//                            preModules[i] = Integer.valueOf(split[i]);
//                        }
//                        break;
                }
            }
//            def.addModule(type, x - pw / 2 + w / 2, y - ph / 2 + h / 2, index,
//                    points, size);
        }

    };

    public static final SSEntityParser partParser = new SSEntityParser() {

        @Override
        public void parse(SSEntityDef def, XmlReader.Element element, XmlReader.Element parent,
                          String name, SSEntityProp prop) {
            String value = element.getAttribute(SSUtil.SSStrings.VALUE);
            SSUpgradePartType type = SSUpgradePartType.get(value);
            def.addProperty(prop, type);
        }

    };

}
