package orw.ss.assets;

import com.badlogic.gdx.utils.XmlReader;

import orw.ss.entity.SSEntityDef;
import orw.ss.enums.SSEntityProp;

public interface SSEntityParser {
    public void parse(SSEntityDef def, XmlReader.Element element, XmlReader.Element parent,
                      String name, SSEntityProp prop);

}
