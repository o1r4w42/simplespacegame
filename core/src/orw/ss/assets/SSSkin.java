package orw.ss.assets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import javax.inject.Inject;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.injection.SSInjectionComponent;

public class SSSkin extends Skin{

    @Inject
    SSAssetManager assetManager;

    private Button.ButtonStyle defButtonStyle;
    private Label.LabelStyle defLabelStyle;

    public SSSkin(SSInjectionComponent injector) {
        injector.injectSkin(this);
    }


    public void init() {
        TextureAtlas atlas = assetManager.get(SSUtil.SSStrings.FILE_SPRITE_ATLAS,
                TextureAtlas.class);
        addRegions(atlas);

        Texture t = assetManager.get(SSUtil.SSStrings.WHITE_PIXEL,
                Texture.class);
        TextureRegion tr = new TextureRegion(t);
        TextureRegionDrawable trd = new TextureRegionDrawable(tr);
        add(SSUtil.SSStrings.WHITE_PIXEL, trd, Drawable.class);

        BitmapFont defFont = assetManager.get(SSUtil.SSStrings.FILE_FONT_DEF,
                BitmapFont.class);
        defLabelStyle = addLabelStyle(SSUtil.SSStrings.LABEL_STYLE_DEFAULT,
                defFont, Color.WHITE);

        defButtonStyle = createButtonStyle(assetManager,
                SSUtil.SSStrings.BUTTON_STYLE_DEFAULT,
                SSUtil.SSStrings.WHITE_PIXEL);
        defButtonStyle.pressedOffsetX = 0;
        defButtonStyle.pressedOffsetY = 0;
        defButtonStyle.checkedOffsetX = 0;
        defButtonStyle.checkedOffsetY = 0;

        Button.ButtonStyle style = createButtonStyle(assetManager,
                SSUtil.SSStrings.BUTTON_STYLE_SQR_64,
                "controls/butSqr64_up", "controls/butSqr64_down");
        style.pressedOffsetX = 0;
        style.pressedOffsetY = -4;
        style.checkedOffsetX = 0;
        style.checkedOffsetY = 0;

        style = createButtonStyle(assetManager, SSUtil.SSStrings.BUTTON_STYLE_SQR_280,
                "controls/butSqr280_up", "controls/butSqr280_down");
        style.pressedOffsetX = 0;
        style.pressedOffsetY = -16;
        style.checkedOffsetX = 0;
        style.checkedOffsetY = 0;

        style = createButtonStyle(assetManager, SSUtil.SSStrings.BUTTON_STYLE_RECT_320,
                "controls/butRect320_up", "controls/butRect320_down");
        style.pressedOffsetX = 0;
        style.pressedOffsetY = -16;
        style.checkedOffsetX = 0;
        style.checkedOffsetY = 0;

        style = createButtonStyle(assetManager, SSUtil.SSStrings.BUTTON_STYLE_RECT_64,
                "controls/butRect64_up", "controls/butRect64_down");
        style.pressedOffsetX = 0;
        style.pressedOffsetY = -4;
        style.checkedOffsetX = 0;
        style.checkedOffsetY = 0;

    }

    protected Label.LabelStyle addLabelStyle(String name, BitmapFont font,
                                             Color color) {
        Label.LabelStyle ls = new Label.LabelStyle(font, color);
        add(name, ls, Label.LabelStyle.class);
        return ls;
    }

    public Label.LabelStyle getLabelStyle(String name) {
        if (has(name, Label.LabelStyle.class)) return get(name, Label.LabelStyle.class);
        ORWLogger.log(ORWLogger.LogCategory.WARNING,
                "getLabelStyle:" + name + " not found!");
        return defLabelStyle;
    }


    private Button.ButtonStyle createButtonStyle(SSAssetManager am, String name,
                                                  String up) {
        return createButtonStyle(am, name, up, null);
    }

    private Button.ButtonStyle createButtonStyle(SSAssetManager am, String name,
                                           String up, String down) {
        return createButtonStyle(am, name, up, down, null);
    }

    private Button.ButtonStyle createButtonStyle(SSAssetManager am, String name,
                                           String up, String down, String act) {
        return createButtonStyle(am, name, up, down, act, -1);
    }

    protected Button.ButtonStyle createButtonStyle(SSAssetManager am, String name,
                                             String up, String down, String act, int ox) {
        Drawable trdUp = getDrawable(up);
        Drawable trdDown = trdUp;
        Drawable trdAct = trdUp;
        if (down != null)
            trdDown = getDrawable(down);
        if (act != null) trdAct = getDrawable(act);

        Button.ButtonStyle bs = new Button.ButtonStyle(trdUp, trdDown, trdAct);
        bs.checkedOffsetX = ox;
        add(name, bs, Button.ButtonStyle.class);
        return bs;
    }


    public Button.ButtonStyle getButtonStyle(String name) {
        if (has(name, Button.ButtonStyle.class)) return get(name, Button.ButtonStyle.class);
        ORWLogger.log(ORWLogger.LogCategory.WARNING,
                "getButtonStyle:" + name + " not found!");
        return defButtonStyle;
    }
}
