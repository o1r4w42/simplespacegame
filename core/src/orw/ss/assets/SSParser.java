package orw.ss.assets;

import com.badlogic.gdx.utils.XmlReader;

import orw.ss.entity.SSEntityDef;

public interface SSParser {
    public void parse(SSEntityDef def, XmlReader.Element element, XmlReader.Element parent);
}
