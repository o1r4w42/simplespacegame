package orw.ss;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import javax.inject.Inject;

import orw.ss.assets.SSAssetManager;
import orw.ss.assets.SSSkin;
import orw.ss.injection.DaggerSSInjectionComponent;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.injection.SSInjectionModule;
import orw.ss.screen.SSScreen;
import orw.ss.screen.SSScreenHandler;

public class SimpleSpaceGame extends Game {

    private SSInjectionComponent injector;

    @Inject
    SpriteBatch batch;
    @Inject
    SSAssetManager assetManager;
    @Inject
    SSSkin skin;
    @Inject
    SSScreenHandler screenHandler;

    @Override
    public void create() {
        injector = DaggerSSInjectionComponent.builder()
                .sSInjectionModule(new SSInjectionModule(this))
                .build();
        injector.injectSimpleSpaceGame(this);
        assetManager.load();
        assetManager.finishLoading();
        assetManager.initEffects();
        skin.init();
        screenHandler.addScreen(
                new SSScreen(this));
        setScreen(screenHandler.init());
    }

    @Override
    public void dispose() {
        super.dispose();
        screenHandler.dispose();
        batch.dispose();
        skin.dispose();
        assetManager.dispose();
    }

    public SSInjectionComponent getInjector() {
        return injector;
    }
}
