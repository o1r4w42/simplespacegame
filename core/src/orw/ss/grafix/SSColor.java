package orw.ss.grafix;

import com.badlogic.gdx.graphics.Color;

public class SSColor {

    public static Color create(int r, int g, int b) {
        return create(r, g, b, 1f);
    }
    public static Color create(int r, int g, int b, float a) {
        return new Color(r / 255f, g / 255f, b / 255f, a);
    }

    public static final Color PLAYER_BLUE = create(22, 73, 136);
    public static final Color ENEMY_RED = create(127, 30, 55);
}
