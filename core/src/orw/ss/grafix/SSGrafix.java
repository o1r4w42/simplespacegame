package orw.ss.grafix;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;

public class SSGrafix {

    public static void drawRoundRect(Pixmap pm, Color color, int x, int y,
                    int width, int height, int radius) {
        fillRect(pm, color, x + radius, y + radius, width - radius * 2,
                        height - radius * 2);
        drawCircle(pm, color, x + radius, y + radius, radius);
        drawCircle(pm, color, x + width - radius - 1, y + radius, radius);
        drawCircle(pm, color, x + radius, y + height - radius - 1, radius);
        drawCircle(pm, color, x + width - radius - 1, y + height - radius - 1,
                        radius);
        fillRect(pm, color, x + radius, y, width - radius * 2, radius);
        fillRect(pm, color, x + radius, y + height - radius, width - radius * 2,
                        radius);
        fillRect(pm, color, x, y + radius, radius, height - 2 * radius);
        fillRect(pm, color, x + width - radius, y + radius, radius,
                        height - 2 * radius);
    }

    private static void drawCircle(Pixmap pm, Color color, int x, int y,
                    int radius) {
        pm.setColor(color);
        pm.fillCircle(x, y, radius);
    }

    public static void fillRect(Pixmap pm, Color color, int x, int y, int width,
                    int height) {
        pm.setColor(color);
        pm.fillRectangle(x, y, width, height);
    }

    public static void drawRect(Pixmap pm, Color color, int x, int y, int width,
                    int height) {
        pm.setColor(color);
        //        pm.fillRectangle(x, y, width, height);
        pm.drawLine(x, y, x + width, y);
        pm.drawLine(x, y, x, y + height);
        pm.drawLine(x, y + height, x + width, y);
        pm.drawLine(x + width, y, x, y + height);
    }

    public static void drawTriangle(Pixmap pm, Color color, int x1, int y1,
                    int x2, int y2, int x3, int y3) {
        pm.setColor(color);
        pm.fillTriangle(x1, y1, x2, y2, x3, y3);
    }

    public static float[] create(int r, int g, int b) {
        float color[] = new float[3];
        color[0] = r / 255f;
        color[1] = g / 255f;
        color[2] = b / 255f;
        return color;
    }

}
