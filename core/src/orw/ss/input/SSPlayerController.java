package orw.ss.input;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import orw.ss.SSUtil;
import orw.ss.entity.actors.SSShip;
import orw.ss.enums.SSDirection;
import orw.ss.enums.SSEntityStat;
import orw.ss.screen.ui.widget.SSLabelBar;

public class SSPlayerController {

    private final boolean[] input;
    private final long[] pressedUp;

    private SSShip player;
    private boolean running;
    private long tapOffset = 200;
    private boolean newInput;

    public SSPlayerController() {
        input = new boolean[2];
        pressedUp = new long[2];
        pressedUp[0] = System.currentTimeMillis();
        pressedUp[1] = System.currentTimeMillis();
    }

    public void update(float delta) {
        if (!running || !player.isActive()) return;
        if (newInput) {
            long time = System.currentTimeMillis();
            if (input[0] && input[1]) {
                player.fire();
            } else if (input[0]) {
                if (time - pressedUp[0] <= tapOffset)
                    player.drift(SSDirection.LEFT);
                else player.rotate(SSDirection.LEFT);
            } else if (input[1]) {
                if (time - pressedUp[1] <= tapOffset)
                    player.drift(SSDirection.RIGHT);
                else player.rotate(SSDirection.RIGHT);
            } else {
                player.fly();
            }
            newInput = false;
        }
    }

    public void setInput(int ind, boolean pressed) {
        newInput = true;
        input[ind] = pressed;
        if (!pressed) pressedUp[ind] = System.currentTimeMillis();
    }

    public SSShip getPlayer() {
        return player;
    }

    public void init(SSShip player) {
        this.player = player;
    }

    public void start() {
        player.spawn();
        running = true;
    }

    public void stop() {
        running = false;
        player.reset(true);
    }
}
