package orw.ss.input;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class SSInputController implements InputProcessor {

    private Vector2 screenMouse;
    private Vector3 worldMouse;
    private boolean[] mouseDown;
    private boolean[] mouseChanged;
    private List<MouseAction> mouseActions;
    private boolean[] keyDown;
    private boolean[] keyChanged;
    private List<KeyAction> keyActions;

    public SSInputController() {
        this(3, 300);
    }

    public SSInputController(int mouseCount, int keyCount) {
        screenMouse = new Vector2();
        worldMouse = new Vector3();
        mouseDown = new boolean[mouseCount];
        mouseChanged = new boolean[mouseCount];
        mouseActions = new ArrayList<MouseAction>();
        keyDown = new boolean[keyCount];
        keyChanged = new boolean[keyCount];
        keyActions = new ArrayList<KeyAction>();
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode < keyDown.length) {
            if (!keyDown[keycode]) keyChanged[keycode] = true;
            keyDown[keycode] = true;
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode < keyDown.length) {
            if (keyDown[keycode]) keyChanged[keycode] = true;
            keyDown[keycode] = false;
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer,
                    int button) {
        if (pointer > 0) return false;
        screenMouse.set(screenX, screenY);
        if (button < mouseDown.length) {
            if (!mouseDown[button]) mouseChanged[button] = true;
            mouseDown[button] = true;
        }
        worldMouse.set(screenX, screenY, 0);
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (pointer > 0) return false;
        screenMouse.set(screenX, screenY);
        if (button < mouseDown.length) {
            if (mouseDown[button]) mouseChanged[button] = true;
            mouseDown[button] = false;
        }
        worldMouse.set(screenX, screenY, 0);
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public void update(float delta) {
        for (MouseAction ma : mouseActions) {
            if ((!ma.triggerOnChange() && mouseDown[ma.getMousecode()])
                            || mouseChanged[ma.getMousecode()])
                ma.mouseChanged(mouseDown[ma.getMousecode()], screenMouse.x,
                                screenMouse.y, worldMouse.x, worldMouse.y);
        }
        for (KeyAction ka : keyActions) {
            if ((!ka.triggerOnChange() && keyDown[ka.getKeycode()])
                            || keyChanged[ka.getKeycode()])
                ka.keyChanged(keyDown[ka.getKeycode()]);
        }
        for (int i = 0; i < mouseChanged.length; i++)
            mouseChanged[i] = false;
        for (int i = 0; i < keyChanged.length; i++)
            keyChanged[i] = false;
    }

    public void addMouseAction(MouseAction ma) {
        if (!mouseActions.contains(ma) && ma.getMousecode() < mouseDown.length)
            mouseActions.add(ma);
    }

    public void addKeyAction(KeyAction ka) {
        if (!keyActions.contains(ka) && ka.getKeycode() < keyDown.length)
            keyActions.add(ka);
    }

    public interface MouseAction {

        public boolean triggerOnChange();

        public void mouseChanged(boolean pressed, float sx, float sy, float wx,
                                 float wy);

        public int getMousecode();
    }

    public interface KeyAction {

        public boolean triggerOnChange();

        public abstract void keyChanged(boolean pressed);

        public int getKeycode();
    }
}
