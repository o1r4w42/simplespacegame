package orw.ss.screen;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.SSVector;
import orw.ss.entity.SSEntity;
import orw.ss.entity.actors.SSShip;
import orw.ss.enums.SSEntityStat;

public class SSCamera extends OrthographicCamera {

    private SSShip player;
    private SSVector offset;
    private float offsetMax = 3.4f;
    private float zoomMax = 1.25f;
    private Actor actor;
    private float despawnRange;
    private float spawnRange;

    public SSCamera() {
        float size = SSUtil.SSIntegers.SCREEN_HEIGHT / 2f;
        spawnRange = ((float) (Math.sqrt(size * size * 2f))
                * SSUtil.SSFloats.WORLD_TO_BODY);
        despawnRange = spawnRange + 2.56f;

        offset = SSVector.getVector();
        actor = new SSCameraActor();
    }

    public void free() {
        offset.free();
    }

    public void update(float delta) {
        if (!player.isActive()) return;

        float angle = player.getBodyAngle();
        float mod = MathUtils.round(player.getVelocity() / player.getLogic().getVelocityMax() * 100f) / 100f;

        up.set(0, -1, 0);
        direction.set(0, 0, -1);
        rotate(SSUtil.modAngle(-angle, 270f));

        offset.set(MathUtils.lerp(0f, offsetMax, mod), 0f);
        offset.setAngle(SSUtil.modAngle(angle, 180f));

        zoom = MathUtils.lerp(1f, zoomMax, mod);
//        zoom = .5f;

        position.set(player.getCX() - offset.getX(), player.getCY() - offset.getY(), 0f);

        update();
    }

    public void setPlayer(SSShip ship) {
        player = ship;
        position.set(player.getCX(), player.getCY(), 0f);
        offset.setZero();
        update();
    }

    public Actor getActor() {
        return actor;
    }

    public void updateBounds(float size) {
        zoomMax = 1f + size / 4f;
        offsetMax = 3.8f - size / 4f;
    }

    private class SSCameraActor extends Actor {

        @Override
        public void drawDebug(ShapeRenderer renderer) {
            super.drawDebug(renderer);
            ShapeRenderer.ShapeType lastType = renderer.getCurrentType();
            Color lastColor = renderer.getColor();
            renderer.set(ShapeRenderer.ShapeType.Line);
            renderer.setColor(Color.RED);

            renderer.circle(position.x, position.y, despawnRange * zoom);
            renderer.setColor(Color.GREEN);

            renderer.circle(position.x, position.y, spawnRange * zoom);
            renderer.set(lastType);
            renderer.setColor(lastColor);
        }
    }

    public boolean isOutOfRange(SSEntity entity) {
        float dist = entity.getDistance(position.x, position.y);
        dist -= entity.getSize() / 2f;
        return dist >= despawnRange * zoom;
    }

    public float getSpawnRange(float size) {
        return MathUtils.random(spawnRange + size, despawnRange - size);
    }
}
