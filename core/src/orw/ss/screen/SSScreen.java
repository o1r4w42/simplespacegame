package orw.ss.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;

import javax.inject.Inject;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.SimpleSpaceGame;
import orw.ss.input.SSInputController;
import orw.ss.input.SSPlayerController;
import orw.ss.screen.ui.SSUserInterface;
import orw.ss.entity.SSWorld;

public class SSScreen implements Screen {

    private final FitViewport viewPort;
    private final Stage stage;
    private final Color clearColor;

    @Inject
    SpriteBatch batch;
    @Inject
    SSWorld world;
    @Inject
    SSCamera camera;
    @Inject
    SSInputController inputController;
    @Inject
    SSPlayerController playerController;
    @Inject
    SSUserInterface userInterface;

    private boolean renderUI = true;
    private boolean debug;
    private boolean renderEffects = true;

    public SSScreen(SimpleSpaceGame game) {
        game.getInjector().injectScreen(this);

        clearColor = new Color(0, 0, 0, 1);

        InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(inputController);
        Gdx.input.setCatchBackKey(true);
        Gdx.input.setInputProcessor(multiplexer);

        viewPort = new FitViewport(
                SSUtil.SSIntegers.SCREEN_WIDTH * SSUtil.SSFloats.WORLD_TO_BODY,
                SSUtil.SSIntegers.SCREEN_HEIGHT * SSUtil.SSFloats.WORLD_TO_BODY,
                camera);
        viewPort.apply();

        userInterface.addInput(multiplexer);

        stage = new Stage(viewPort, batch);
        world.init(stage);
        userInterface.parseWidgets();

        initKeyActions();

        world.start();

        toggleDebug();
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b,
                clearColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined);

        stage.draw();

        world.render(debug);

        if (renderUI) userInterface.render();

        update(delta);
    }

    private void update(float delta) {
        inputController.update(delta);

        world.update(delta);

        userInterface.update(delta);

        ORWLogger.gameFrameNr++;
    }

    @Override
    public void resize(int width, int height) {
        viewPort.update(width, height, false);
        userInterface.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        userInterface.dispose();
        world.dispose();
    }

    private void toggleDebug() {
        debug = ! debug;
        stage.setDebugAll(debug);
        userInterface.setDebug(debug);
    }


    private void initKeyActions() {
        inputController.addKeyAction(initDebugKeyAction());
        inputController.addKeyAction(initUIKeyAction());
        inputController.addKeyAction(initEffectKeyAction());
        inputController.addKeyAction(initLeftKeyAction());
        inputController.addKeyAction(initRightKeyAction());
        inputController.addKeyAction(initTKeyAction());
        inputController.addKeyAction(initBackKeyAction());
        inputController.addKeyAction(initEscKeyAction());
    }

    private SSInputController.KeyAction initDebugKeyAction() {
        return new SSInputController.KeyAction() {
            @Override
            public boolean triggerOnChange() {
                return true;
            }

            @Override
            public void keyChanged(boolean pressed) {
                if (pressed) {
                    toggleDebug();
                }
            }

            @Override
            public int getKeycode() {
                return Input.Keys.F;
            }
        };
    }

    private SSInputController.KeyAction initBackKeyAction() {
        return new SSInputController.KeyAction() {
            @Override
            public boolean triggerOnChange() {
                return true;
            }

            @Override
            public void keyChanged(boolean pressed) {
                if (pressed) {
                    userInterface.handleBack();
                }
            }

            @Override
            public int getKeycode() {
                return Input.Keys.BACK;
            }
        };
    }

    private SSInputController.KeyAction initEscKeyAction() {
        return new SSInputController.KeyAction() {
            @Override
            public boolean triggerOnChange() {
                return true;
            }

            @Override
            public void keyChanged(boolean pressed) {
                if (pressed) {
                    userInterface.handleBack();
                }
            }

            @Override
            public int getKeycode() {
                return Input.Keys.ESCAPE;
            }
        };
    }

    private SSInputController.KeyAction initUIKeyAction() {
        return new SSInputController.KeyAction() {
            @Override
            public boolean triggerOnChange() {
                return true;
            }

            @Override
            public void keyChanged(boolean pressed) {
                if (pressed) {
                    renderUI = !renderUI;
                }
            }

            @Override
            public int getKeycode() {
                return Input.Keys.R;
            }
        };
    }

    private SSInputController.KeyAction initTKeyAction() {
        return new SSInputController.KeyAction() {
            @Override
            public boolean triggerOnChange() {
                return true;
            }

            @Override
            public void keyChanged(boolean pressed) {
                if (pressed) {
                    ORWLogger.log("_______________________________");
                }
            }

            @Override
            public int getKeycode() {
                return Input.Keys.T;
            }
        };
    }

    private SSInputController.KeyAction initEffectKeyAction() {
        return new SSInputController.KeyAction() {
            @Override
            public boolean triggerOnChange() {
                return true;
            }

            @Override
            public void keyChanged(boolean pressed) {
                if (pressed) {
                    renderEffects = !renderEffects;
                }
            }

            @Override
            public int getKeycode() {
                return Input.Keys.E;
            }
        };
    }

    private SSInputController.KeyAction initLeftKeyAction() {
        return new SSInputController.KeyAction() {
            @Override
            public boolean triggerOnChange() {
                return true;
            }

            @Override
            public void keyChanged(boolean pressed) {
                playerController.setInput(0, pressed);
            }

            @Override
            public int getKeycode() {
                return Input.Keys.A;
            }
        };
    }

    private SSInputController.KeyAction initRightKeyAction() {
        return new SSInputController.KeyAction() {
            @Override
            public boolean triggerOnChange() {
                return true;
            }

            @Override
            public void keyChanged(boolean pressed) {
                playerController.setInput(1, pressed);
            }

            @Override
            public int getKeycode() {
                return Input.Keys.D;
            }
        };
    }


}
