package orw.ss.screen.ui;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;

import javax.inject.Inject;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.assets.SSAssetManager;
import orw.ss.enums.SSDialogDef;
import orw.ss.enums.SSMenuDef;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.screen.ui.dialog.SSDialog;
import orw.ss.screen.ui.dialog.SSPauseDialog;
import orw.ss.screen.ui.dialog.SSShipDialog;
import orw.ss.screen.ui.menu.SSGameMenu;
import orw.ss.screen.ui.menu.SSMenu;

public class SSUserInterface implements Disposable {

    private final OrthographicCamera camera;
    private final FitViewport viewPort;
    private final Stage stage;
    private final Table rootTable;
    private final SSMenu[] menus;
    private final SSDialog[] dialogs;

    @Inject
    SpriteBatch batch;
    @Inject
    SSAssetManager assetManager;

    private SSGameMenu gameMenu;

    private SSShipDialog shipDialog;
    private SSPauseDialog pauseDialog;

    private SSMenu currentMenu;
    private SSDialog currentDialog;

    public SSUserInterface(SSInjectionComponent injector) {
        injector.injectUserInterface(this);
        camera = new OrthographicCamera();
        viewPort = new FitViewport(
                SSUtil.SSIntegers.SCREEN_WIDTH,
                SSUtil.SSIntegers.SCREEN_HEIGHT,
                camera);

        stage = new Stage(viewPort, batch);
        rootTable = new Table();
        rootTable.setName(SSUtil.SSStrings.rootTable);
        rootTable.setFillParent(true);
        stage.addActor(rootTable);

        menus = new SSMenu[SSMenuDef.values().length];
        gameMenu = new SSGameMenu(injector);
        menus[SSMenuDef.GAME.getInd()] = gameMenu;
        initMenu(SSMenuDef.GAME.getInd());


        dialogs = new SSDialog[SSDialogDef.values().length];
        shipDialog = new SSShipDialog(injector, this, assetManager);
        pauseDialog = new SSPauseDialog(injector, this);
        dialogs[SSDialogDef.UPGRADE.getInd()] = shipDialog;
        dialogs[SSDialogDef.PAUSE.getInd()] = pauseDialog;
    }

    private void initMenu(int ind) {
        currentMenu = menus[ind];
        rootTable.addActor(currentMenu);
    }
    public void enterMenu(SSMenuDef menuDef) {
        enterMenu(menuDef.getInd());
    }

    private void enterMenu(int ind) {
        currentMenu.remove();
        currentMenu = menus[ind];
        rootTable.addActor(currentMenu);
    }


//    public Matrix4 getCam() {
//        return camera.combined;
//    }

    public void showDialog(SSDialogDef def) {
        currentMenu.setTouchable(Touchable.disabled);
        if (currentDialog != null) {
            currentDialog.remove();
            currentDialog.exited();
        }
        currentDialog = dialogs[def.getInd()];
        currentDialog.entered();
        rootTable.addActor(currentDialog);
    }

    public void exitDialog() {
        if (currentDialog != null) {
            currentDialog.exited();
            currentDialog.remove();
            currentDialog = null;
        }
        currentMenu.setTouchable(Touchable.enabled);
    }

    public void handleBack() {
        if (currentDialog == null) showDialog(SSDialogDef.PAUSE);
        else if(!currentDialog.handleBack()){
            exitDialog();
        }
    }


    @Override
    public void dispose() {
        stage.dispose();
    }

    public void update(float delta) {
        stage.act(delta);
    }

    public void render() {
        batch.setProjectionMatrix(camera.combined);
        stage.draw();
    }

    public void setDebug(boolean debug) {
        stage.setDebugAll(debug);
    }

    public void addInput(InputMultiplexer multiplexer) {
        multiplexer.addProcessor(stage);
    }

    public void resize(int width, int height) {
        viewPort.update(width, height, false);
    }

    public void parseWidgets() {
        gameMenu.parseWidgets();
    }

//    public boolean touchDown(int screenX, int screenY, int pointer,
//                             int button) {
//        return stage.touchDown(screenX, screenY, pointer, button);
//    }
//
//    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
//        return stage.touchUp(screenX, screenY, pointer, button);
//    }
//
//    public boolean touchDragged(int x, int y, int pointer) {
//        return stage.touchDragged(x, y, pointer);
//    }
//
//    public boolean mouseMoved(int screenX, int screenY) {
//        return stage.mouseMoved(screenX, screenY);
//    }
//
//    public boolean scrolled(int amount) {
//        return stage.scrolled(amount);
//    }
}
