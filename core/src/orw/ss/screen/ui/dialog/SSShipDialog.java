package orw.ss.screen.ui.dialog;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import javax.inject.Inject;

import orw.ss.SSUtil;
import orw.ss.assets.SSAssetManager;
import orw.ss.entity.SSEntityDef;
import orw.ss.entity.actors.SSShip;
import orw.ss.entity.logic.SSPlayerLogic;
import orw.ss.enums.SSModuleType;
import orw.ss.enums.SSSlotType;
import orw.ss.enums.SSUpgradePartType;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.screen.ui.SSUserInterface;
import orw.ss.screen.ui.widget.SSButtonText;
import orw.ss.screen.ui.widget.SSLabelPart;
import orw.ss.screen.ui.widget.SSPartList;
import orw.ss.screen.ui.widget.SSViewModule;
import orw.ss.screen.ui.widget.SSViewShip;
import orw.ss.screen.ui.widget.SSWidget;

public class SSShipDialog extends SSDialog {

    private final SSAssetManager assetManager;
    private SSLabelPart[] partLabels;
    private SSViewShip shipView;
    private SSViewModule moduleView;
    private SSPartList partList;
    private Table midTable;
    private SSWidget currentView;

    @Inject
    public SSShipDialog(SSInjectionComponent injector, SSUserInterface userInterface, SSAssetManager assetManager) {
        super("SSShipDialog", injector, userInterface);
        this.assetManager = assetManager;
    }

    @Override
    protected void create(Table window) {
        partList = new SSPartList(skin, this);

        Table top = new Table();
        createParts(top);
        window.add(top);
        window.row();

        float height = SSUtil.SSIntegers.SCREEN_HEIGHT / 2f;
        float width = SSUtil.SSIntegers.SCREEN_HEIGHT;

        shipView = new SSViewShip(skin, width, height, this);

        moduleView = new SSViewModule(skin, width, height, this);

        midTable = new Table();
        window.add(midTable).size(width, height);
        window.row();


        Table bot = new Table();
        createButtons(bot);
        window.add(bot).width(SSUtil.SSIntegers.SCREEN_HEIGHT);

    }

    @Override
    public boolean handleBack() {
        if (currentView == moduleView) {
            enterView(shipView);
            return true;
        }
        return false;
    }

    @Override
    public void entered() {
        super.entered();
        resetView();
    }

    @Override
    public void exited() {
        super.exited();
        partList.remove();
        currentView = null;
    }

    private void resetView() {
        partList.remove();
        if (currentView == shipView) {
            userInterface.exitDialog();
            currentView = null;
            return;
        }
        enterView(shipView);
    }

    private void enterView(SSWidget view) {
        partList.remove();
        midTable.clearChildren();
        midTable.addActor(view);
        currentView = view;
        update(playerController.getPlayer());
    }

    private void createParts(Table top) {
        partLabels = new SSLabelPart[SSUpgradePartType.values().length];
        for (SSUpgradePartType type : SSUpgradePartType.values()) {
            partLabels[type.getIndex()] = new SSLabelPart(skin, type);
            top.add(partLabels[type.getIndex()]).size(128, 64).left();
        }
    }

    private void createButtons(Table mid) {
        ClickListener backListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                resetView();
            }
        };
        SSButtonText backButton = SSUtil.createTextButton(skin, "Back", backListener,
                SSUtil.SSStrings.BUTTON_STYLE_RECT_320, Color.RED);
        mid.add(backButton);
    }

    private void update(SSShip player) {
        SSPlayerLogic logic = player.getLogic();
        shipView.update(logic);
        for (SSLabelPart label : partLabels)
            label.setText(Integer.toString(logic.getParts(label.getType())));
    }

    public void showAddParts(int moduleIndex, int slotIndex,
                             SSSlotType slotType, int size, float x, float y) {
        partList.addParts(moduleIndex, slotIndex, slotType, size, x, y, assetManager);
    }

    public void installModule(int index, SSEntityDef definition) {
        partList.remove();
        playerController.getPlayer().getLogic().installModule(index, definition);
        update(playerController.getPlayer());
    }

    public void installPart(int moduleIndex, int slotIndex, int slotSize, SSEntityDef def) {
        partList.remove();
        playerController.getPlayer().getLogic().installPart(moduleIndex, slotIndex, def);
        update(playerController.getPlayer());
    }


    public void showAddModules(int index, SSModuleType type, float x, float y) {
        partList.addModules(index, type, x, y, assetManager);
    }

    public void showModule(int index) {
        enterView(moduleView);
        moduleView.update(index, playerController.getPlayer().getLogic());
    }

}
