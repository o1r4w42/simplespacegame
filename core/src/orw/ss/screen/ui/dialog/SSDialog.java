package orw.ss.screen.ui.dialog;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import javax.inject.Inject;

import orw.ss.SSUtil;
import orw.ss.assets.SSSkin;
import orw.ss.entity.SSWorld;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.input.SSPlayerController;
import orw.ss.screen.ui.SSUITable;
import orw.ss.screen.ui.SSUserInterface;

public abstract class SSDialog extends SSUITable {

    protected final SSUserInterface userInterface;

    @Inject
    SSWorld world;
    @Inject
    SSSkin skin;
    @Inject
    SSPlayerController playerController;

    public SSDialog(String name, SSInjectionComponent injector, SSUserInterface userInterface) {
        super(name);
        injector.injectDialog(this);
        this.userInterface = userInterface;

        setFillParent(true);
        Table window = new Table();
        window.setBackground(skin.getDrawable(SSUtil.SSStrings.WHITE_PIXEL));
        window.setColor(Color.GRAY);
        add(window).size(SSUtil.SSIntegers.SCREEN_HEIGHT);
        create(window);
    }

    public void entered() {
        world.pauseWorld(false);
    }

    public void exited() {
        world.pauseWorld(true);
    }

    protected abstract void create(Table window);

    public abstract boolean handleBack();
}
