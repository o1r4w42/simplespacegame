package orw.ss.screen.ui.dialog;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import javax.inject.Inject;

import orw.ss.SSUtil;
import orw.ss.enums.SSDialogDef;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.screen.ui.SSUserInterface;
import orw.ss.screen.ui.widget.SSButtonText;

public class SSPauseDialog extends SSDialog {

    //    private SSButtonText upgradeButton;
//    private SSPlayerController playerController;
    //    private SSPlayerLogic playerLogic;
    @Inject
    public SSPauseDialog(SSInjectionComponent injector, SSUserInterface userInterface) {
        super("SSPauseDialog", injector, userInterface);
//        this.playerController = playerController;
        //        playerLogic = playerController.getPlayer().getLogic();
    }

    @Override
    protected void create(Table window) {
        Table top = new Table();
        window.add(top);
        window.row();

        ClickListener restartListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                world.restart();
                userInterface.exitDialog();
            }
        };
        SSButtonText restartButton = SSUtil.createTextButton(skin, "Restart",
                restartListener, SSUtil.SSStrings.BUTTON_STYLE_RECT_320,
                Color.RED);
        top.add(restartButton);

        ClickListener exitListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                userInterface.exitDialog();
            }
        };
        SSButtonText exitButton = SSUtil.createTextButton(skin, "Continue",
                exitListener, SSUtil.SSStrings.BUTTON_STYLE_RECT_320,
                Color.BLUE);
        top.add(exitButton);

        ClickListener upgradeListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                userInterface.showDialog(SSDialogDef.UPGRADE);
            }
        };
        SSButtonText upgradeButton = SSUtil.createTextButton(skin, "Ship", upgradeListener,
                SSUtil.SSStrings.BUTTON_STYLE_RECT_320, Color.GREEN);
        window.add(upgradeButton);
    }

    @Override
    public boolean handleBack() {
        return false;
    }

    @Override
    public void entered() {
        super.entered();
//        if (playerController.getPlayer().getLogic().canUpgradeSlot()) {
//            upgradeButton.setColor(Color.GREEN);
//            //            upgradeButton.setTouchable(Touchable.enabled);
//        } else {
//            upgradeButton.setColor(Color.LIGHT_GRAY);
//            //            upgradeButton.setTouchable(Touchable.disabled);
//        }
    }
}
