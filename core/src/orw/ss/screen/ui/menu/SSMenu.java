package orw.ss.screen.ui.menu;

import javax.inject.Inject;

import orw.ss.assets.SSSkin;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.input.SSPlayerController;
import orw.ss.screen.ui.SSUITable;

public abstract class SSMenu extends SSUITable {

    protected final SSInjectionComponent injector;

    @Inject
    protected SSSkin skin;
    @Inject
    protected SSPlayerController playerController;

    public SSMenu(String name, SSInjectionComponent injector) {
        super(name);
        this.injector = injector;
        injector.injectMenu(this);
        setFillParent(true);
        create();
    }

    public abstract void create();

}

