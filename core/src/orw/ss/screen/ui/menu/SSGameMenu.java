package orw.ss.screen.ui.menu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import orw.ss.SSUtil;
import orw.ss.assets.SSSkin;
import orw.ss.enums.SSUpgradePartType;
import orw.ss.injection.SSInjectionComponent;
import orw.ss.screen.ui.widget.SSLabelBar;
import orw.ss.screen.ui.widget.SSLabelIcon;

public class SSGameMenu extends SSMenu {

    private SSLabelBar hpLabel;
    private SSLabelBar shieldLabel;
    private SSLabelBar speedLabel;
    private SSLabelBar velocityLabel;
    private SSLabelBar rotationLabel;
    private Image directionImage;
    private SSLabelIcon scoreLabel;
    private SSLabelIcon[] partLabels;

    public SSGameMenu(SSInjectionComponent injector) {
        super(SSUtil.SSStrings.GameMenu, injector);
    }

    @Override
    public void create() {
        float w = (SSUtil.SSIntegers.SCREEN_WIDTH - SSUtil.SSIntegers.SCREEN_HEIGHT) / 2f;
        float h = SSUtil.SSIntegers.SCREEN_HEIGHT;
        Table left = new Table();
        Table mid = new Table();
        Table right = new Table();
        add(left).size(w, h);
        add(mid).size(h, h);
        add(right).size(w, h);

        createLeft(skin, left, w, h);
        createRight(skin, right, w, h);
    }

    private void createLeft(SSSkin skin, Table left, float w, float h) {
        Table leftTop = new Table();
        left.setBackground(skin.getDrawable(SSUtil.SSStrings.WHITE_PIXEL));
        left.setColor(Color.GRAY);
        EventListener leftListener = new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                                     int pointer, int button) {
                playerController.setInput(0, true);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer,
                                int button) {
                playerController.setInput(0, false);
            }
        };
        Button leftBtn = SSUtil.createButton(skin, leftListener, SSUtil.SSStrings.BUTTON_STYLE_SQR_280);
        leftBtn.setColor(Color.GREEN);
        left.add(leftTop).size(w, h - w);
        left.row();
        left.add(leftBtn);

        createLeftTop(leftTop, w, h);
    }

    private void createLeftTop(Table top, float w, float h) {
        hpLabel = new SSLabelBar(skin, "icons/ico_hp", Color.RED, w);
        shieldLabel = new SSLabelBar(skin, "icons/ico_sp", Color.BLUE, w);
        speedLabel = new SSLabelBar(skin, "icons/ico_acceleration", Color.GREEN, w);
        velocityLabel = new SSLabelBar(skin, "icons/ico_velocity", Color.GREEN, w);
        rotationLabel = new SSLabelBar(skin, "icons/ico_rotation", Color.GREEN, w);
        directionImage = new Image(skin.getDrawable("icons/ico_direction"));
        directionImage.pack();
        directionImage.setOrigin(directionImage.getWidth() / 2f, directionImage.getHeight() / 2f);
        directionImage.setScaleX(-1f);

        top.add(hpLabel).size(w, 64);
        top.row();
        top.add(shieldLabel).size(w, 64);
        top.row();
        top.add(speedLabel).size(w, 64);
        top.row();
        top.add(velocityLabel).size(w, 64);
        top.row();
        top.add(rotationLabel).size(w, 64);
        top.row();
        top.add(directionImage);
    }

    private void createRight(SSSkin skin, Table right, float w, float h) {
        Table rightTop = new Table();
        right.setBackground(skin.getDrawable(SSUtil.SSStrings.WHITE_PIXEL));
        right.setColor(Color.GRAY);

        ClickListener rightListener = new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                                     int pointer, int button) {
                playerController.setInput(1, true);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer,
                                int button) {
                playerController.setInput(1, false);
            }
        };
        Button rightBtn = SSUtil.createButton(skin, rightListener, SSUtil.SSStrings.BUTTON_STYLE_SQR_280);
        rightBtn.setColor(Color.GREEN);
        right.add(rightTop).size(w, h - w);
        right.row();
        right.add(rightBtn);

        createRightTop(skin, rightTop, w, h);
    }

    private void createRightTop(SSSkin skin, Table rightTop, float w, float h) {
        scoreLabel = new SSLabelIcon(skin, "icons/ico_star", Color.YELLOW, w);
        rightTop.add(scoreLabel);
        rightTop.row();

        partLabels = new SSLabelIcon[SSUpgradePartType.values().length];
        for (SSUpgradePartType type : SSUpgradePartType.values()) {
            partLabels[type.getIndex()] = new SSLabelIcon(skin, type.getSource(), type.getColor(), w);
            rightTop.add(partLabels[type.getIndex()]);
            rightTop.row();
        }
    }

    public void parseWidgets() {
        playerController.getPlayer().getLogic().setLabels(hpLabel, shieldLabel, speedLabel,
                velocityLabel, rotationLabel, directionImage, scoreLabel, partLabels);
    }
}
