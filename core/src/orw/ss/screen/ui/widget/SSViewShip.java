package orw.ss.screen.ui.widget;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.util.ArrayList;
import java.util.List;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.SSVector;
import orw.ss.assets.SSSkin;
import orw.ss.entity.equip.modules.SSModuleSlots;
import orw.ss.entity.logic.SSPlayerLogic;
import orw.ss.entity.logic.SSShipLogic;
import orw.ss.entity.view.SSShipView;
import orw.ss.enums.SSModuleType;
import orw.ss.screen.ui.dialog.SSShipDialog;

public class SSViewShip extends SSWidget {

    private final float padding = 45f;
    private final float scale = 3f;

    private final SSShipDialog shipDialog;
    private List<SSButtonModule> moduleButtons;
    private Table buttonTable;
    private SSScrollPane scrollPane;
    private float width;
    private float height;

    public SSViewShip(SSSkin skin, float width, float height, SSShipDialog shipDialog) {
        super(skin, "SShipView", width, height);
        this.shipDialog = shipDialog;
        moduleButtons = new ArrayList<>();
    }

    @Override
    public void create(float width, float height) {
        scrollPane = new SSScrollPane(skin, width, height);
        addActor(scrollPane);

        buttonTable = new Table();
        scrollPane.getScrollTable().add(buttonTable).size(width * 1.25f);
    }

    public void update(SSPlayerLogic logic) {
        updateImages(logic);
        updateButtons(logic);
    }

    private void updateImages(SSPlayerLogic logic) {
        width = Math.round(logic.getModuleSlots().getViewWidth() * SSUtil.SSFloats.BODY_TO_WORLD * scale);
        height = Math.round(logic.getModuleSlots().getViewHeight() * SSUtil.SSFloats.BODY_TO_WORLD * scale);

        buttonTable.remove();
        Table scrollTable = scrollPane.getScrollTable();
        scrollTable.reset();
        scrollTable.add(buttonTable).size(width + padding * 2f, height + padding * 2f);
        scrollTable.pack();

        scrollPane.layout();
        scrollPane.setScrollPercent(.5f);
    }

    private void updateButtons(SSShipLogic logic) {
        buttonTable.clear();
        SSModuleSlots slots = logic.getModuleSlots();
        int counter = 0;
        for (int i = 0; i < slots.getSize(); i++) {
            for (int j = 0; j < slots.getSize(i); j++) {
                SSButtonModule moduleButton;
                SSModuleType type = slots.getType(i);
                if (counter < moduleButtons.size()) {
                    moduleButton = moduleButtons.get(counter);
                    moduleButton.set(i, type);
                } else {
                    moduleButton = SSUtil.createModuleButton(skin, i, type, shipDialog);
                    moduleButtons.add(moduleButton);
                }
                buttonTable.addActor(moduleButton);

                SSVector pos = slots.getPosition(i, j);
                float x = width / 2f + pos.getX() * SSUtil.SSFloats.BODY_TO_WORLD * scale + padding;
                float y = height / 2f + pos.getY() * SSUtil.SSFloats.BODY_TO_WORLD * scale + padding;
                moduleButton.setPosition(x, y);
                moduleButton.update(logic, scale, pos.getY() < 0);
                counter++;
            }
        }

    }

}
