package orw.ss.screen.ui.widget;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import orw.ss.SSUtil;
import orw.ss.assets.SSAssetManager;
import orw.ss.assets.SSSkin;
import orw.ss.entity.SSEntityDef;
import orw.ss.enums.SSDefType;
import orw.ss.enums.SSModuleType;
import orw.ss.enums.SSShipPartType;
import orw.ss.enums.SSSlotType;
import orw.ss.enums.SSUpgradePartType;
import orw.ss.screen.ui.dialog.SSShipDialog;

public class SSPartList extends SSWidget {

    private final SSShipDialog shipDialog;
    private List<SSButtonUpgrade> buttons;
    private boolean active;
    private Table scrollTable;
    private List<SSEntityDef> definitions;

    public SSPartList(SSSkin skin, SSShipDialog shipDialog) {
        super(skin, "SSPartList", 0, 0);
        this.shipDialog = shipDialog;
        setBackground(skin.getDrawable(SSUtil.SSStrings.WHITE_PIXEL));
        setColor(Color.DARK_GRAY);
        definitions = new ArrayList<>();
        buttons = new ArrayList<>();
    }

    @Override
    public void create(float width, float height) {
        setSize(64f * 5f, 64f * 6f);

        setTouchable(Touchable.enabled);
        Table top = new Table();
        add(top);

        ClickListener exitListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                remove();
                active = false;
            }
        };
        SSButtonText exitButton = SSUtil.createTextButton(skin, "X", exitListener,
                SSUtil.SSStrings.BUTTON_STYLE_SQR_64, Color.RED);
        top.add(exitButton);

        for (SSUpgradePartType upt : SSUpgradePartType.values()) {
            Image image = new Image(skin.getDrawable(upt.getSource()));
            image.pack();
            image.setColor(upt.getColor());
            top.add(image);
        }

        scrollTable = new Table();
        ScrollPane scrollPane = new ScrollPane(scrollTable);
        row();
        add(scrollPane).size(64f * 5f);
    }

    @Override
    public boolean remove() {
        active = false;
        return super.remove();
    }

    public void addModules(int index, SSModuleType type, float x, float y, SSAssetManager assetManager) {
        update(x, y);

        addModules(type, assetManager);

        for (int i = 0; i < definitions.size(); i++) {
            SSButtonUpgrade but;
            if (i < buttons.size()) {
                but = buttons.get(i);
            } else {
                but = SSUtil.createUpgradeButton(skin, shipDialog);
                buttons.add(but);
            }
            but.updateModules(definitions.get(i), index);
            scrollTable.add(but).padTop(4).padBottom(4);
            scrollTable.row();
        }
    }

    private void addModules(SSModuleType type, SSAssetManager assetManager) {
        switch (type) {
            case FRONT:
            case BACK:
                definitions.add(assetManager.getDefinition(SSDefType.MODULE_S_0));
                definitions.add(assetManager.getDefinition(SSDefType.MODULE_S_1));
                break;
            case MID:
                definitions.add(assetManager.getDefinition(SSDefType.MODULE_M_START_0));
                definitions.add(assetManager.getDefinition(SSDefType.MODULE_M_END_0));
                break;
        }
    }

    public void addParts(int moduleIndex, int slotIndex, SSSlotType type, int size,
                         float x, float y, SSAssetManager assetManager) {
        update(x, y);

        addParts(type, size, assetManager);

        for (int i = 0; i < definitions.size(); i++) {
            SSButtonUpgrade but;
            if (i < buttons.size()) {
                but = buttons.get(i);
            } else {
                but = SSUtil.createUpgradeButton(skin, shipDialog);
                buttons.add(but);
            }
            but.updateParts(definitions.get(i), moduleIndex, slotIndex, size);
            scrollTable.add(but).padTop(4).padBottom(4);
            scrollTable.row();
        }
    }

    private void addParts(SSSlotType type, int size, SSAssetManager assetManager) {
        for (SSShipPartType spt : SSShipPartType.values()){
            if (spt.canFit(type)){
                definitions.add(assetManager.getDefinition(SSDefType.get(spt, size)));
            }
        }
    }

    private void update(float x, float y) {
        if (!active) {
            active = true;
            shipDialog.addActor(this);
        }
        x -= getWidth();
        y -= getHeight();
        y = Math.max(y, 0);
        x = Math.max(x, 0);

        setPosition(x, y);

        scrollTable.clear();
        definitions.clear();
    }

}
