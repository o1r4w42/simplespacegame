package orw.ss.screen.ui.widget;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import orw.ss.assets.SSSkin;
import orw.ss.entity.equip.parts.SSShipPart;
import orw.ss.entity.logic.SSPlayerLogic;
import orw.ss.enums.SSSlotType;
import orw.ss.screen.ui.dialog.SSShipDialog;

public class SSButtonSlot extends SSButtonImage {

    private int slotIndex;
    private SSSlotType type;
    private int size;
    private SSShipPart part;
    private int moduleIndex;


    public SSButtonSlot(SSSkin skin, ButtonStyle style, int moduleIndex, int slotIndex,
                        SSSlotType type, int size, SSShipDialog shipDialog) {
        super(skin, null, style);
        set(moduleIndex, slotIndex, type, size);
        addListener(shipDialog);
    }

    public void set(int moduleIndex, int slotIndex, SSSlotType type, int size) {
        this.moduleIndex = moduleIndex;
        this.slotIndex = slotIndex;
        this.type = type;
        this.size = size;
    }

    private void addListener(final SSShipDialog shipDialog) {
        ClickListener listener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (part == null) {
                    x = event.getStageX();
                    y = event.getStageY();
                    shipDialog.showAddParts(moduleIndex, slotIndex, type, size, x, y);
                }
            }
        };
        addListener(listener);
    }

    public void update(SSPlayerLogic logic) {
        part = logic.getModulePart(moduleIndex, slotIndex);
        if (part != null) {
            String name = part.getDrawableName();
            if (!name.isEmpty()) {
                setImage(skin.getDrawable(name));
                if (logic.canUpdatePart(size, part.getLevel())) {
                    setColor(Color.GREEN);
                    setTouchable(Touchable.enabled);
                } else {
                    setColor(Color.LIGHT_GRAY);
                    setTouchable(Touchable.disabled);
                }
            } else {
                setImage(null);
                setColor(Color.LIGHT_GRAY);
                setTouchable(Touchable.disabled);
            }
        } else {
            setImage(null);
            if (logic.canAddPart(size)) {
                setColor(Color.BLUE);
                setTouchable(Touchable.enabled);
            } else {
                setColor(Color.LIGHT_GRAY);
                setTouchable(Touchable.disabled);
            }
        }
    }
}
