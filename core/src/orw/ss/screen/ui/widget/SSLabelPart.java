package orw.ss.screen.ui.widget;

import orw.ss.assets.SSSkin;
import orw.ss.enums.SSUpgradePartType;

public class SSLabelPart extends SSLabelImage {

    private SSUpgradePartType type;

    public SSLabelPart(SSSkin skin, SSUpgradePartType type) {
        super(skin, type.getSource(), type.getColor());
        this.type = type;
    }

    public SSUpgradePartType getType() {
        return type;
    }
}
