package orw.ss.screen.ui.widget;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import orw.ss.SSUtil;
import orw.ss.assets.SSSkin;

public class SSLabelImage extends SSWidget {

    public SSLabelImage(SSSkin skin, String name, Color color) {
        super(skin, name, 0, 0);
        image.setColor(color);
        textLabel.setColor(color);
    }

    private Label textLabel;
    private Image image;


    @Override
    public void create(float width, float height) {
        image = new Image(skin.getDrawable(name));
        add(image);
        textLabel = SSUtil.createLabel(skin);
        add(textLabel).expand();
    }


    public void setText(String text) {
        textLabel.setText(text);
    }

}
