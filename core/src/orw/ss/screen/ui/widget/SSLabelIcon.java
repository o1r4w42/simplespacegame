package orw.ss.screen.ui.widget;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;

import orw.ss.SSUtil;
import orw.ss.assets.SSSkin;

public class SSLabelIcon extends SSWidget {

    private Image image;
    private Label textLabel;

    public SSLabelIcon(SSSkin skin, String name, Color color, float width) {
        super(skin, name, width, 64f);
        image.setColor(color);

    }

    @Override
    public void create(float width, float height) {
        image = new Image(skin.getDrawable(name));
        add(image);
        textLabel = SSUtil.createLabel(skin, "0");
        textLabel.setAlignment(Align.center);
        add(textLabel).size(width - height, height);
    }

    public void setText(String text) {
        textLabel.setText(text);
    }

}
