package orw.ss.screen.ui.widget;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import orw.ss.SSUtil;
import orw.ss.assets.SSSkin;

public class SSLabelBar extends SSWidget {

    private Label textLabelStart;
    private Label textLabelEnd;
    private Image bar;
    private Image image;

    public SSLabelBar(SSSkin skin, String name, Color color, float width) {
        super(skin, name, width, 64f);
        bar.setColor(color);
        image.setColor(color);
    }

    @Override
    public void create(float width, float height) {

        image = new Image(skin.getDrawable(name));
        add(image);

        Image bg = new Image(skin.getDrawable(SSUtil.SSStrings.WHITE_PIXEL));
        bg.setColor(Color.BLACK);
        add(bg).size(width - image.getWidth() - 16f, height - 4).padLeft(16f);

        bar = new Image(skin.getDrawable(SSUtil.SSStrings.WHITE_PIXEL));

        bar.setSize(width - image.getWidth() - 16f, height - 4);
        bar.setPosition(image.getWidth() + 16f, 2);
        addActor(bar);

        Table textTable = new Table();
        textTable.setSize(width - image.getWidth() - 16f, height - 4);
        textTable.setPosition(image.getWidth() + 16f, 2);
        addActor(textTable);

        textLabelStart = SSUtil.createLabel(skin, SSUtil.SSStrings.EMPTY);
        textTable.add(textLabelStart).expand().right();
        Label textLabelMid = SSUtil.createLabel(skin, SSUtil.SSStrings.SLASH);
        textTable.add(textLabelMid);
        textLabelEnd = SSUtil.createLabel(skin, SSUtil.SSStrings.EMPTY);
        textTable.add(textLabelEnd).expand().left();

    }


    public void set(float cur, float max) {
        if (cur > max) cur = max;
        textLabelStart.setText(Integer.toString(MathUtils.round(cur)));
        textLabelEnd.setText(Integer.toString(MathUtils.round(max)));
        bar.setScaleX(Math.abs(cur / max));
    }

}
