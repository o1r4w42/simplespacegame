package orw.ss.screen.ui.widget;

import com.badlogic.gdx.scenes.scene2d.ui.Button;

import orw.ss.assets.SSSkin;

public class SSButton extends Button {

    protected final SSSkin skin;

    public SSButton(SSSkin skin, ButtonStyle style) {
        super(style);
        this.skin = skin;
    }
}
