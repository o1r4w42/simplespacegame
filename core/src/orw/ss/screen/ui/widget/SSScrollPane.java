package orw.ss.screen.ui.widget;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import orw.ss.assets.SSSkin;

public class SSScrollPane extends SSWidget {

    private Table scrollTable;
    private ScrollPane scrollPane;
    private Image scrollIndicatorRight;
    private Image scrollIndicatorLeft;
    private Image scrollIndicatorTop;
    private Image scrollIndicatorBot;

    public SSScrollPane(SSSkin skin, float width, float height) {
        super(skin, "SSScrollPane", width, height);
    }

    @Override
    protected void create(float width, float height) {
        setSize(width, height);

        scrollTable = new Table();

        scrollPane = new ScrollPane(scrollTable);
        scrollPane.addListener(e -> {
            updateScrollIndicators();
            return false;
        });

        add(scrollPane).fill();
        scrollPane.setOverscroll(false, false);
        scrollPane.layout();


        scrollIndicatorRight = new Image(skin.getDrawable("icons/ico_acceleration"));
        scrollIndicatorRight.pack();
        scrollIndicatorRight.setPosition(width - scrollIndicatorRight.getWidth() * .75f,
                height / 2f - scrollIndicatorRight.getHeight() / 4f);
        scrollIndicatorRight.setScale(.5f);
        addActor(scrollIndicatorRight);

        scrollIndicatorLeft = new Image(skin.getDrawable("icons/ico_acceleration"));
        scrollIndicatorLeft.pack();
        scrollIndicatorLeft.setScale(-.5f, .5f);
        scrollIndicatorLeft.setPosition(scrollIndicatorRight.getWidth() * .75f,
                height / 2f - scrollIndicatorRight.getHeight() / 4f);
        addActor(scrollIndicatorLeft);

        scrollIndicatorTop = new Image(skin.getDrawable("icons/ico_acceleration"));
        scrollIndicatorTop.pack();
        scrollIndicatorTop.setScale(.5f);
        scrollIndicatorTop.setPosition(width / 2f - scrollIndicatorRight.getWidth() / 2f,
                height - scrollIndicatorRight.getHeight());
        scrollIndicatorTop.setOrigin(scrollIndicatorTop.getWidth() / 2f, scrollIndicatorTop.getHeight() / 2f);
        scrollIndicatorTop.setRotation(90f);
        addActor(scrollIndicatorTop);

        scrollIndicatorBot = new Image(skin.getDrawable("icons/ico_acceleration"));
        scrollIndicatorBot.pack();
        scrollIndicatorBot.setScale(.5f);
        scrollIndicatorBot.setPosition(width / 2f - scrollIndicatorRight.getWidth() / 2f,
                0);
        scrollIndicatorBot.setOrigin(scrollIndicatorTop.getWidth() / 2f, scrollIndicatorTop.getHeight() / 2f);
        scrollIndicatorBot.setRotation(270f);
        addActor(scrollIndicatorBot);
    }

    public Table getScrollTable() {
        return scrollTable;
    }

    public void setScrollPercent(float percent) {
        scrollPane.setScrollPercentX(percent);
        scrollPane.setScrollPercentY(percent);
        scrollPane.updateVisualScroll();
        scrollIndicatorLeft.setVisible(scrollTable.getWidth() > getWidth());
        scrollIndicatorRight.setVisible(scrollTable.getWidth() > getWidth());
        scrollIndicatorTop.setVisible(scrollTable.getHeight() > getHeight());
        scrollIndicatorBot.setVisible(scrollTable.getHeight() > getHeight());
    }

    private void updateScrollIndicators() {
        scrollIndicatorLeft.setVisible(scrollTable.getWidth() > getWidth() &&
                scrollPane.getScrollPercentX() != 0f);
        scrollIndicatorRight.setVisible(scrollTable.getWidth() > getWidth() &&
                scrollPane.getScrollPercentX() != 1f);
        scrollIndicatorBot.setVisible(scrollTable.getHeight() > getHeight() &&
                scrollPane.getScrollPercentY() != 1f);
        scrollIndicatorTop.setVisible(scrollTable.getHeight() > getHeight() &&
                scrollPane.getScrollPercentY() != 0f);
    }
}
