package orw.ss.screen.ui.widget;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import orw.ss.SSUtil;
import orw.ss.assets.SSSkin;
import orw.ss.screen.ui.SSUITable;

public class SSButtonText extends SSButton {

    private Label textLabel;

    public SSButtonText(SSSkin skin, String text, ButtonStyle style, Color color) {
        super(skin, style);
        textLabel = SSUtil.createLabel(skin, text);
        textLabel.pack();
        textLabel.setPosition(getWidth() / 2f - textLabel.getWidth() / 2f,
                getHeight() / 2f - textLabel.getHeight() / 2f);
        addActor(textLabel);
        setColor(color);
    }

    @Override
    public void setColor(Color color) {
        super.setColor(color);
        textLabel.setColor(color);
    }

    public void setText(String text) {
        textLabel.setText(text);
        textLabel.pack();
        textLabel.setPosition(getWidth() / 2f - textLabel.getWidth() / 2f,
                getHeight() / 2f - textLabel.getHeight() / 2f);
    }

    @Override
    protected void sizeChanged() {
        super.sizeChanged();
        if (textLabel != null)
            textLabel.setPosition(getWidth() / 2f - textLabel.getWidth() / 2f,
                    getHeight() / 2f - textLabel.getHeight() / 2f);
    }
}
