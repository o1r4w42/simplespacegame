package orw.ss.screen.ui.widget;

import orw.ss.assets.SSSkin;
import orw.ss.screen.ui.SSUITable;

public abstract class SSWidget extends SSUITable {

    protected final SSSkin skin;

    public SSWidget(SSSkin skin, String name, float width, float height) {
        super(name);
        this.skin = skin;
        create(width, height);
    }

    protected abstract void create(float width, float height);
}
