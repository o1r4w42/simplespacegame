package orw.ss.screen.ui.widget;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import javax.inject.Inject;

import orw.ss.assets.SSSkin;
import orw.ss.injection.SSInjectionComponent;

public class SSButtonImage extends SSButton {

    private final Image image;

    public SSButtonImage(SSSkin skin, String name, ButtonStyle style) {
        super(skin, style);
        image = new Image();
        addActor(image);
        if (name != null && !name.isEmpty()) setImage(skin.getDrawable(name));
    }

    public void setImage(Drawable drawable) {
        setImage(drawable, 1f, 1f);
    }

    public void setImage(Drawable drawable, float scaleX, float scaleY) {
        if (drawable == null) {
            image.setVisible(false);
            return;
        }
        image.setDrawable(drawable);
        image.pack();
        image.setScale(scaleX, scaleY);
        image.setPosition(getWidth() / 2f - image.getWidth() / 2f * scaleX,
                        getHeight() / 2f - image.getHeight() / 2f * scaleY);
        image.setVisible(true);
    }

    @Override
    public void setPosition(float x, float y) {
        super.setPosition(x - getWidth() / 2f, y - getHeight() / 2f);
    }
}
