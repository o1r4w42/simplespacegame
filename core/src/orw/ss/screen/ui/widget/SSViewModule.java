package orw.ss.screen.ui.widget;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.util.ArrayList;
import java.util.List;

import orw.ss.SSUtil;
import orw.ss.SSVector;
import orw.ss.assets.SSSkin;
import orw.ss.entity.equip.parts.SSPartSlots;
import orw.ss.entity.equip.modules.SSShipModule;
import orw.ss.entity.logic.SSPlayerLogic;
import orw.ss.enums.SSSlotType;
import orw.ss.screen.ui.dialog.SSShipDialog;

public class SSViewModule extends SSWidget {

    private final SSShipDialog shipDialog;
    private Image moduleImage;
    private Image frameImage;
    private List<SSButtonSlot> slotButtons;
    private float scale;
    private Table buttonTable;
    private SSShipModule module;
    private int index;

    public SSViewModule(SSSkin skin, float width, float height, SSShipDialog shipDialog) {
        super(skin, "SSViewModule", width, height);
        this.shipDialog = shipDialog;
        slotButtons = new ArrayList<>();
    }

    @Override
    public void create(float width, float height) {
        frameImage = new Image();
        frameImage.setSize(height, height);
        frameImage.setPosition(height / 2f, 0);
        addActor(frameImage);

        moduleImage = new Image();
        moduleImage.setSize(height, height);
        moduleImage.setPosition(height / 2f, 0);
        addActor(moduleImage);

        scale = 1f;

        buttonTable = new Table();
        buttonTable.setSize(height, height);
        buttonTable.setPosition(height / 2f, 0);
        addActor(buttonTable);
    }

    public void update(int index, SSPlayerLogic logic) {
        this.index = index;
        module = logic.getModule(index);
        float baseSize = moduleImage.getWidth() * scale;
        updateImage(baseSize);
        updateButtons(logic, baseSize);
    }

    private void updateImage(float baseSize) {
        moduleImage.setDrawable(skin.getDrawable(module.getSpriteSource()));
        moduleImage.pack();
        scale = baseSize / moduleImage.getWidth();
        moduleImage.setScale(scale);

        frameImage.setDrawable(skin.getDrawable(module.getFrameSource()));
        frameImage.pack();
        frameImage.setScale(scale);
    }

    private void updateButtons(SSPlayerLogic logic, float baseSize) {
        buttonTable.clear();
        SSPartSlots slots = module.getSlots();
        int counter = 0;
        for (int i = 0; i < slots.getSize(); i++) {
            int size = slots.getSlotSize(i);
            SSSlotType type = slots.getSlotType(i);

            if (!module.getModuleType().checkType(type)) continue;

            for (int j = 0; j < slots.getSize(i); j++) {
                SSButtonSlot slotButton;
                if (counter < slotButtons.size()) {
                    slotButton = slotButtons.get(counter);
                    slotButton.set(index, i, type, size);
                } else {
                    slotButton = SSUtil.createSlotButton(skin, index, i, type, size, shipDialog);
                    slotButtons.add(slotButton);
                }
                slotButton.update(logic);
                buttonTable.addActor(slotButton);

                SSVector pos = slots.getPosition(i, j);
                float x = baseSize / 2f
                        + pos.getX() * SSUtil.SSFloats.BODY_TO_WORLD * scale;
                float y = baseSize / 2f
                        + pos.getY() * SSUtil.SSFloats.BODY_TO_WORLD * scale;
                slotButton.setPosition(x, y);
                counter++;
            }
        }

    }

}
