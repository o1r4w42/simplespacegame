package orw.ss.screen.ui.widget;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import orw.ss.assets.SSSkin;
import orw.ss.entity.equip.modules.SSShipModule;
import orw.ss.entity.logic.SSShipLogic;
import orw.ss.enums.SSModuleType;
import orw.ss.screen.ui.dialog.SSShipDialog;

public class SSButtonModule extends SSButtonImage {

    private final SSShipDialog shipDialog;
    private int index;
    private SSModuleType type;
    private SSShipModule module;

    public SSButtonModule(SSSkin skin, ButtonStyle style, int index,
                          SSModuleType type, SSShipDialog shipDialog) {
        super(skin, null, style);
        this.shipDialog = shipDialog;
        set(index, type);
        addListener();
        setColor(Color.GREEN);
    }

    public void set(int index, SSModuleType type) {
        this.index = index;
        this.type = type;
    }

    private void addListener() {
        ClickListener listener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (module == null) {
                    x = event.getStageX();
                    y = event.getStageY();
                    shipDialog.showAddModules(index, type, x, y);
                } else {
                    shipDialog.showModule(index);
                }
            }
        };
        addListener(listener);
    }

    public void update(SSShipLogic logic, float scale, boolean flipY) {
        module = logic.getModule(index);
        if (module == null) {
            setImage(null);
            setTouchable(Touchable.enabled);
            setColor(Color.GREEN);
            return;
        }
        setTouchable(Touchable.enabled);
        setColor(Color.BLUE);
        float scaleY = scale;
        if (flipY) scaleY *= -1;
        setImage(skin.getDrawable(module.getSpriteSource()), scale, scaleY);
    }

}
