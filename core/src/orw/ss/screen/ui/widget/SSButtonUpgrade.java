package orw.ss.screen.ui.widget;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import orw.ORWLogger;
import orw.ss.SSUtil;
import orw.ss.assets.SSSkin;
import orw.ss.entity.SSEntityDef;
import orw.ss.enums.SSEntityProp;
import orw.ss.enums.SSUpgradePartType;
import orw.ss.screen.ui.dialog.SSShipDialog;

public class SSButtonUpgrade extends SSButton {

    private Label[] partLabels;
    private int moduleIndex;
    private Image image;
    private SSEntityDef definition;
    private boolean installModule;
    private int slotIndex;
    private int slotSize;

    public SSButtonUpgrade(SSSkin skin, ButtonStyle style, SSShipDialog shipDialog) {
        super(skin, style);
        Table imgTable = new Table();
        image = new Image();
        imgTable.add(image);
        add(imgTable).size(64f);

        partLabels = new Label[SSUpgradePartType.values().length];
        for (SSUpgradePartType upt : SSUpgradePartType.values()) {
            partLabels[upt.getIndex()] = SSUtil.createLabel(skin);
            partLabels[upt.getIndex()].setAlignment(Align.center);
            add(partLabels[upt.getIndex()]).size(64f);
        }

        addListener(shipDialog);
    }

    private void addListener(final SSShipDialog shipDialog) {
        ClickListener listener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (installModule)
                    shipDialog.installModule(moduleIndex, definition);
                else shipDialog.installPart(moduleIndex, slotIndex, slotSize, definition);
            }
        };
        addListener(listener);
    }

    public void updateModules(SSEntityDef definition, int moduleIndex) {
        update(true, definition, moduleIndex, definition.getDefType().getName());
    }

    public void updateParts(SSEntityDef definition, int moduleIndex, int slotIndex, int slotSize) {
        this.slotIndex = slotIndex;
        this.slotSize = slotSize;
        update(false, definition, moduleIndex, definition.get(SSEntityProp.ICON,
                String.class, SSUtil.SSStrings.EMPTY));
    }

    private void update(boolean installModule, SSEntityDef definition, int moduleIndex, String imageName) {
        this.installModule = installModule;
        this.definition = definition;
        this.moduleIndex = moduleIndex;
        image.setDrawable(skin.getDrawable(imageName));
    }
}
