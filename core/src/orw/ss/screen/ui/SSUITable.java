package orw.ss.screen.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Table;

import orw.ss.SSUtil;

public abstract class SSUITable extends Table {

    protected final String name;

    public SSUITable(String name) {
        super();
        this.name = name;
        setName(name + SSUtil.SSStrings.UNDER_SCORE + SSUtil.SSStrings.rootTable);
    }

}
