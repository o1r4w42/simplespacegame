package orw.ss.screen;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.utils.Disposable;

public class SSScreenHandler implements Disposable {

    private List<SSScreen> screens;

    public SSScreenHandler() {
        screens = new ArrayList<SSScreen>();
    }

    @Override
    public void dispose() {
        for (SSScreen s : screens)
            s.dispose();
    }

    public void addScreen(SSScreen screen) {
        if (!screens.contains(screen)) screens.add(screen);
    }

    public SSScreen init() {
//        for (SSScreen s : screens)
//            s.init();
        if (screens.size() > 0) return screens.get(0);
        return null;
    }

}
