package orw.ss;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import orw.ORWLogger;
import orw.ss.assets.SSSkin;
import orw.ss.enums.SSCollType;
import orw.ss.enums.SSDirection;
import orw.ss.enums.SSModuleType;
import orw.ss.enums.SSSlotType;
import orw.ss.screen.ui.dialog.SSShipDialog;
import orw.ss.screen.ui.widget.SSButtonImage;
import orw.ss.screen.ui.widget.SSButtonModule;
import orw.ss.screen.ui.widget.SSButtonSlot;
import orw.ss.screen.ui.widget.SSButtonText;
import orw.ss.screen.ui.widget.SSButtonUpgrade;

public class SSUtil {

    public static float getAngleDiff(float a, float b) {
        float diff = a - b;
        if (diff < 0f) diff += 360f;
        else if (diff > 360f) diff -= 360f;
        if (diff > 180f) diff = 360f - diff;
        return diff;
    }

    public static float getAngleDiffRad(float a, float b) {
        float diff = a - b;
        if (diff < 0f) diff += MathUtils.PI * 2;
        else if (diff > MathUtils.PI * 2) diff -= MathUtils.PI * 2;
        if (diff > MathUtils.PI) diff = MathUtils.PI * 2 - diff;
        return Math.abs(diff);
    }

    public static SSDirection getAngleDir(float a, float b) {
        float diff;
        if (a > b) {
            diff = a - b;
            if (diff > 180f) return SSDirection.RIGHT;
            return SSDirection.LEFT;
        } else {
            diff = b - a;
            if (diff > 180f) return SSDirection.LEFT;
            return SSDirection.RIGHT;
        }
    }

    public static float modAngle(float angle, float mod) {
        angle += mod;
        if (angle < 0f) angle += 360f;
        else if (angle > 360f) angle -= 360f;
        return angle;
    }

    public static float modAngleRad(float angle, float mod) {
        angle += mod;
        if (angle < 0f) angle += MathUtils.PI * 2;
            //        return angle % MathUtils.PI * 2;
        else if (angle > MathUtils.PI * 2) angle -= MathUtils.PI * 2;
        return angle;
    }

    public static String[] splitSource(String source, String end) {
        return splitSource(source, end, SSStrings.SLASH, SSStrings.UNDER_SCORE);
    }

    public static String[] splitSource(String source, String end,
                                       String pathSep, String indSep) {
        String[] sourceSplit = new String[4];
        String[] split = source.split(end);
        split = split[0].split(pathSep);
        sourceSplit[0] = split[split.length - 2] + pathSep;
        sourceSplit[1] = split[split.length - 1];
        split = split[split.length - 1].split(indSep);
        sourceSplit[2] = split[0];
        sourceSplit[3] = split[1];
        return sourceSplit;
    }

    public static Label createLabel(SSSkin skin) {
        return createLabel(skin, SSUtil.SSStrings.EMPTY);
    }

    public static Label createLabel(SSSkin skin, String text) {
        return createLabel(skin, text, SSUtil.SSStrings.LABEL_STYLE_DEFAULT);
    }

    public static Label createLabel(SSSkin skin, String text, String styleName) {
        Label label = new Label(text, skin.getLabelStyle(styleName));
        return label;
    }

    public static Button createButton(SSSkin skin) {
        return createButton(skin, null);
    }

    public static Button createButton(SSSkin skin, EventListener listener) {
        return createButton(skin, listener, SSUtil.SSStrings.BUTTON_STYLE_DEFAULT);
    }

    public static Button createButton(SSSkin skin, EventListener listener, String styleName) {
        Button b = new Button(skin.getButtonStyle(styleName));
        if (listener != null) b.addListener(listener);
        return b;
    }

    public static SSButtonText createTextButton(SSSkin skin, String text) {
        return createTextButton(skin, text, Color.WHITE);
    }

    public static SSButtonText createTextButton(SSSkin skin, String text, Color color) {
        return createTextButton(skin, text, null, color);
    }

    public static SSButtonText createTextButton(SSSkin skin, String text, EventListener listener, Color color) {
        return createTextButton(skin, text, listener, SSUtil.SSStrings.BUTTON_STYLE_DEFAULT, color);
    }

    public static SSButtonText createTextButton(SSSkin skin, String text, EventListener listener,
                                                String styleName, Color color) {
        SSButtonText b = new SSButtonText(skin, text, skin.getButtonStyle(styleName), color);
        if (listener != null) b.addListener(listener);
        return b;
    }

    public static SSButtonImage createImageButton(SSSkin skin, String name, EventListener listener) {
        return createImageButton(skin, name, listener, SSUtil.SSStrings.BUTTON_STYLE_DEFAULT);
    }

    public static SSButtonImage createImageButton(SSSkin skin, String name, EventListener listener,
                                                  String styleName) {
        SSButtonImage b = new SSButtonImage(skin, name, skin.getButtonStyle(styleName));
        if (listener != null) b.addListener(listener);
        return b;
    }

    public static SSButtonSlot createSlotButton(SSSkin skin, int ind, int moduleInd,
                                                SSSlotType type, int size, SSShipDialog shipDialog) {
        return new SSButtonSlot(skin, skin.getButtonStyle(SSStrings.BUTTON_STYLE_SQR_64), ind,
                moduleInd, type, size, shipDialog);
    }

    public static SSButtonModule createModuleButton(SSSkin skin, int ind, SSModuleType type, SSShipDialog shipDialog) {
        return new SSButtonModule(skin, skin.getButtonStyle(SSStrings.BUTTON_STYLE_SQR_64), ind,
                type, shipDialog);
    }

    public static SSButtonUpgrade createUpgradeButton(SSSkin skin, SSShipDialog shipDialog) {
        return new SSButtonUpgrade(skin, skin.getButtonStyle(SSStrings.BUTTON_STYLE_SQR_64), shipDialog);
    }

    public static int[][] createShapePoints(float x, float y, float baseSize) {
        int[][] points;
        if (y != 0f) {
            points = new int[4][3];
            if (x > 0) {
                points[0][0] = 3;
                points[0][1] = Math.round((x + 20) * baseSize / 2f);
                points[0][2] = Math.round((20) * baseSize);

                points[1][0] = 2;
                points[1][1] = Math.round((x) * baseSize / 2f);
                points[1][2] = Math.round((y) * baseSize / 2f);

                points[2][0] = 5;
                points[2][1] = Math.round((x) * baseSize / 2f);
                points[2][2] = Math.round((y) * -baseSize / 2f);

                points[3][0] = 4;
                points[3][1] = Math.round((x + 20) * baseSize / 2f);
                points[3][2] = Math.round((20) * -baseSize);
            } else {
                points[0][0] = 0;
                points[0][1] = Math.round((x - 20) * baseSize / 2f);
                points[0][2] = Math.round((20) * baseSize);

                points[1][0] = 1;
                points[1][1] = Math.round((x) * baseSize / 2f);
                points[1][2] = Math.round((y) * baseSize / 2f);

                points[2][0] = 6;
                points[2][1] = Math.round((x) * baseSize / 2f);
                points[2][2] = Math.round((y) * -baseSize / 2f);

                points[3][0] = 7;
                points[3][1] = Math.round((x - 20) * baseSize / 2f);
                points[3][2] = Math.round((20) * -baseSize);
            }
        } else {
            points = new int[2][3];
            if (x > 0) {
                points[0][0] = 3;
                points[0][1] = Math.round((x + 20) * baseSize / 2f);
                points[0][2] = Math.round((20) * baseSize);

                points[1][0] = 4;
                points[1][1] = Math.round((x + 20) * baseSize / 2f);
                points[1][2] = Math.round((20) * -baseSize);
            } else {
                points[0][0] = 0;
                points[0][1] = Math.round((x - 20) * baseSize / 2f);
                points[0][2] = Math.round((20) * baseSize);

                points[1][0] = 7;
                points[1][1] = Math.round((x - 20) * baseSize / 2f);
                points[1][2] = Math.round((20) * -baseSize);
            }
        }
        return points;
    }

    public static class SSStrings {

        public static final String DMG_SHIELD = "dmg:shield";
        public static final String DMG_ARMOR = "dmg:armor";
        public static final String DMG = "dmg";
        public static final String LASER = "laser";
        public static final String PART = "part";
        public static final String DEFAULT = "default";
        public static final String EXPLOSION = "explosion";
        public static final String DAMAGE = "damage";
        public static final String SP_MAX = "sp:max";
        public static final String SP_REG = "sp:reg";
        public static final String SP = "sp";
        public static final String HP = "hp";
        public static final String HP_MAX = "hp:max";
        public static final String HP_REG = "hp:reg";
        public static final String VELOCITY_MAX = "velocity:max";
        public static final String ROTATE_MAX = "rotate:max";
        public static final String ROTATE_MOD = "rotate:mod";
        public static final String ROTATE_SPEED = "rotate:speed";
        public static final String ROTATE_STEP = "rotate:step";
        public static final String ACCELERATION_SPEED = "acceleration:speed";
        public static final String ACCELERATION_MAX = "acceleration:max";
        public static final String ACCELERATION_MOD = "acceleration:mod";
        public static final String ACCELERATION_STEP = "acceleration:step";
        public static final String SIZE = "size";
        public static final String MID = "mid";
        public static final String POWER = "power";
        public static final String WEAPON = "weapon";
        public static final String ENGINE = "engine";
        public static final String ARMOR = "armor";
        public static final String ICON = "icon";
        public static final String GUN = "gun";
        public static final String METEOR = "meteor";
        public static final String THRUST = "thrust";
        public static final String SIDE = "side";
        public static final String SHIP_PART_TYPE = "shipPartType";
        public static final String DRIVE = "drive";
        public static final String EFFECT = "effect";
        public static final String FRONT = "front";
        public static final String BACK = "back";
        public static final String SIDE_BACK = "side_back";
        public static final String INDEX = "index";
        public static final String X = "x";
        public static final String Y = "y";
        public static final String ID = "id";
        public static final String WIDTH = "width";
        public static final String HEIGHT = "height";
        public static final String BODY = "body";
        public static final String SLOT = "slot";
        public static final String MODULE = "module";
        public static final String VALUE = "value";
        public static final String EMPTY = "";
        public static final String UNDER_SCORE = "_";
        public static final String DOUBLE_DOT = ":";
        public static final String SLASH = "/";

        public static final String GameMenu = "GameMenu";
        public static final String rootTable = "rootTable";

        public static final String WHITE_PIXEL = "WHITE_PIXEL";
        public static final String LABEL_STYLE_DEFAULT = "lblStlDef";
        public static final String BUTTON_STYLE_DEFAULT = "btnStlDef";
        public static final String BUTTON_STYLE_RECT_320 = "btnStlRect320";
        public static final String BUTTON_STYLE_RECT_64 = "btnStlRect64";
        public static final String BUTTON_STYLE_SQR_64 = "btnStlSqr64";
        public static final String BUTTON_STYLE_SQR_280 = "btnStlSqr280";
        public static final String FILE_FONT_DEF = "font/Capture_it.ttf";
        public static final String FILE_SPRITE_ATLAS = "textures.atlas";
        public static final String FILE_SHIP_DEFS = "ships.tsx";
        public static final String FILE_MODULE_DEFS = "modules.tsx";
        public static final String FILE_FRAME_DEFS = "frames.tsx";
        public static final String FILE_ITEM_DEFS = "items.tsx";
        public static final String FILE_PART_DEFS = "parts.tsx";
        public static final String FILE_BG_DEFS = "background.tsx";
        public static final String FILE_MET_DEFS = "meteors.tsx";
        public static final String FILE_PNG = ".png";
        public static final String PRE_PART = "particles/";
        public static final String PRE_DEF = "defs/";
        public static final String PRE_SHIP = "ships/";
        public static final String PRE_ITEM = "items/";
        public static final String TEXTURE_SHIP_SMALL = "shipS";
        public static final String TEXTURE_ITEM_STAR = "star";
        public static final String EFFECT_DEFAULT = "default.p";
        public static final String EFFECT_GUN = "gun.p";
        public static final String EFFECT_LASER = "laser.p";
        public static final String EFFECT_DAMAGE = "damage.p";
        public static final String EFFECT_EXPLOSION = "explosion.p";
        public static final String EFFECT_SHIELD = "shield.p";

        public static final String SHIP_S_0 = "ships/s_0";
        public static final String SHIP_S_1 = "ships/s_1";
        public static final String SHIP_S_2 = "ships/s_2";
        public static final String FRAME_XS_0 = "frames/xs_0";
        public static final String FRAME_S_0 = "frames/s_0";
        public static final String FRAME_M_0 = "frames/m_0";
        public static final String MODULE_BASE_S_0 = "modules/baseS_0";
        public static final String MODULE_S_0 = "modules/s_0";
        public static final String MODULE_S_1 = "modules/s_1";
        public static final String MODULE_M_START_0 = "modules/mStart_0";
        public static final String MODULE_M_END_0 = "modules/mEnd_0";
        public static final String MET_S_0 = "meteors/s_0";
        public static final String MET_M_0 = "meteors/m_0";
        public static final String MET_L_0 = "meteors/l_0";
        public static final String MET_XL_0 = "meteors/xl_0";
        public static final String BG_0 = "bg/bg_0";
        public static final String ITEM_WEAPON = "items/weapon_0";
        public static final String ITEM_POWER = "items/power_0";
        public static final String ITEM_ENGINE = "items/engine_0";
        public static final String ITEM_ARMOR = "items/armor_0";
        public static final String PART_DRIVE_0 = "parts/drive_0";
        public static final String PART_THRUST_0 = "parts/thrust_0";
        public static final String PART_GUN_0 = "parts/gun_0";
        public static final String PART_LASER_0 = "parts/laser_0";
        public static final String PART_ARMOR_0 = "parts/armor_0";
        public static final String PART_SHIELD_0 = "parts/shield_0";

        public static final String ITEM = "item";
        public static final String SHIP = "ship";
        public static final String PLAYER = "player";
        public static final String ENEMY = "enemy";
        public static final String OBJECT = "object";

        public static final String ROTATION = "rotation";
        public static final String ACCELERATION = "acceleration";
        public static final String VELOCITY = "velocity";

        public static final String TYPE = "type";

        public static final String TILE = "tile";
        public static final String SOURCE = "source";
        public static final String DOT = ".";
        public static final String IMAGE = "image";
        public static final String PROPERTIES = "properties";
        public static final String PROPERTY = "property";
        public static final String OBJECT_GROUP = "objectgroup";
        public static final String DEF_TYPE = "defType";
        public static final String ENT_TYPE = "entType";

        public static final String VIEW_NAME = "viewName";
        public static final String VIEW_IND = "viewInd";
        public static final String VIEW_PATH = "viewPath";
        public static final String RADIUS = "radius";
        public static final String SENSOR = "sensor";
        public static final String NAME = "name";
        public static final String SLOTS = "slots";
        public static final String SHIELD = "shield";
        public static final String DEF_EMITTER = "Untitled";
        public static final String COMMA = ",";
        public static final String ENDING_COLOR = "_c";
        public static final String BASE = "base";
        public static final String ADD = "add";
        public static final String POINTS = "points";
        public static final String NL = "\n";
        public static final String DEF_FRAME = "frames/xs";


        public static final String DENSITY = "density";
        public static final String MASS = "mass";
//        public static final String PRE_MODULES = "preModules";
    }

    public static class SSShorts {

        public static final short collMaskPlayer = (short) (SSCollType.ITEM
                .getId() | SSCollType.ENEMY.getId()
                | SSCollType.METEOR.getId() | SSCollType.WEAPON.getId());

        public static final short collMaskItem = SSCollType.PLAYER.getId();

        public static final short collMaskEnemy = (short) (SSCollType.PLAYER
                .getId() | SSCollType.ENEMY.getId()
                | SSCollType.METEOR.getId() | SSCollType.WEAPON.getId());

        public static final short collMaskMeteor = (short) (SSCollType.PLAYER
                .getId() | SSCollType.ENEMY.getId()
                | SSCollType.METEOR.getId() | SSCollType.WEAPON.getId());

        public static final short collMaskWeapon = (short) (SSCollType.PLAYER
                .getId() | SSCollType.ENEMY.getId()
                | SSCollType.METEOR.getId());

    }


    public static class SSIntegers {

        public static final int SCREEN_WIDTH = 1280;
        public static final int SCREEN_HEIGHT = 720;

        public static int SSUpgradePartTypeInd = 0;
        public static int SSEntityStatInd = 0;
        public static int SSMenuDefInd = 0;
        public static int SSDialogDefInd = 0;
        public static int SSGroupTypeInd = 0;
    }

    public static class SSFloats {
        public static final float WORLD_TO_BODY = 0.01f;
        public static final float BODY_TO_WORLD = 100f;

    }
}
