package orw;

public class ORWLogger {

    private static final String logInit = "#";
    private static final String logStart = " (";
    private static final String logMid = ":";
    private static final String logEnd = "): ";

    public static class LogCategory {
        public static final int ALL = 0x0fffffff;
        public static final int NONE = 0x0;
//        public static final int LOAD = 0x1;
//        public static final int FLOW = 0x2;
//        public static final int STAGE = 0x4;
//        public static final int EXEC = 0x8;
//        public static final int DRAW = 0x10;
//        public static final int INIT = 0x20;
//        public static final int ANIM = 0x40;
//        public static final int LEVEL = 0x80;
//        public static final int GAME = 0x100;
//        public static final int COLLISION = 0x200;
//        public static final int STATUS = 0x400;
//        public static final int SFX = 0x800;
//        public static final int INPUT = 0x1000;
//        public static final int UI = 0x2000;

        public static final int WARNING = 0x10000000;
        public static final int ERROR = 0x20000000;
        public static final int INFO = 0x40000000;
        public static final int DETAIL = 0x80000000;
    }
    
    private static int intendation = 0;
    private static int logCategory =
                    LogCategory.ALL
    ;
    private static int filterCategory =
                    LogCategory.NONE
    ;
    private static String[] filter = {};
    private static String[] filterNegative = {};
    private static boolean printErrors = true;
    private static boolean printWarnings = true;
    private static boolean printDetails = false;
    public static int gameFrameNr = 0;

    public static void log(String msg) {
        log(LogCategory.ALL, msg, 0);
    }

    public static void log(String msg, int depth) {
        log(LogCategory.ALL, msg, depth);
    }

    public static void log(int logCategories, String msg, boolean condition) {
        if (condition) log(logCategories, msg, 0);
    }

    public static void log(int logCategories, String msg, int lvl,
                    boolean condition) {
        if (condition) log(logCategories, msg, lvl);
    }

    public static void log(int logCategories, String msg) {
        log(logCategories, msg, 0);
    }

    public static void log(int logCategories, String msg, int depth) {
        if (!checkPrint(logCategories, msg)) return;

        StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        int mID = 1;
        for (int i = 1; i < trace.length - 1; i++) {
            if (trace[i].getMethodName().equals("log")
                            || trace[i].getMethodName()
                                            .equals("printMapProperties")
                            || trace[i].getMethodName()
                                            .equals("printVJXProperties")) {
                continue;
            } else {
                mID = i + depth;
                break;
            }
        }

        String src = logInit + gameFrameNr + logStart
                        + trace[mID].getFileName() + logMid
                        + trace[mID].getLineNumber() + logEnd;
        src = String.format("%1$-40s", src);
        String m = src;

        if (logCategories == LogCategory.WARNING) m += "WARNING ---| ";
        else if (logCategories == LogCategory.ERROR) m += "ERROR ---| ";

        String intendationStr = "";
        for (int i = 0; i < intendation; i++) {
            intendationStr += "|   ";
        }
        m += intendationStr + msg;
        m = String.format("%1$-175s", m) + "|";
        System.out.println(m);
        return;
    }

    private static boolean checkPrint(int logCategories, String msg) {
        if (msg == null || logCategories != LogCategory.ALL
                        && (logCategories & filterCategory) > 0)
            return false;
        for (String string : filterNegative)
            if (msg.contains(string)) return false;

        boolean print = true;
        if (filter.length > 0) {
            print = false;
            for (String string : filter)
                if (msg.contains(string)) print = true;
        }

        if (!print) return false;

        if (logCategories == LogCategory.ALL
                        || (logCategories & logCategory) > 0) {
            if ((logCategories & LogCategory.DETAIL) > 0 && !printDetails)
                return false;

            return true;
        }

        if ((logCategories & LogCategory.ERROR) > 0 && printErrors
                        || (logCategories & LogCategory.WARNING) > 0
                                        && printWarnings)
            return true;
        return false;
    }
}
